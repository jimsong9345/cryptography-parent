package com.study.crypto.certificate.server.bytebuddy;

/**
 * @author Songjin
 * @since 2023-06-19 10:49
 */
public class Bar {

    public static String sayHelloBar() {
        return "Holla in Bar!";
    }
}
