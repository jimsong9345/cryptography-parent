package com.study.crypto.certificate.server.mybatis;

import org.junit.jupiter.api.Test;
import org.mybatis.generator.api.CommentGenerator;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 描述：使用Java方式运行MBG
 */
class MyBatisGeneratorTest {
	
	@Test
	void test() throws Exception{
		System.setProperty("user.name", "Songjin");
		List<String> warnings = new ArrayList<>();
		boolean overwrite = true;
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream is = classloader.getResourceAsStream("mybatis/generatorConfig.xml");
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = cp.parseConfiguration(is);
		CommentGenerator commentGenerator = config.getContext("Mysql").getCommentGenerator();
		System.out.println(commentGenerator);
		DefaultShellCallback callback = new DefaultShellCallback(overwrite);
		MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
		myBatisGenerator.generate(null);
	}
	
}