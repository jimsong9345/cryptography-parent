package com.study.crypto.certificate.server;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.junit.jupiter.api.Test;

import java.io.StringReader;
import java.security.Security;

/**
 * @author Songjin
 * @since 2023-02-01 15:53
 */
class Pkcs10Test {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void testPkcs10SignatureValid() throws Exception{
        String pemText = "MIIBGzCBwwIBADBjMQwwCgYDVQQDDANaQlgxDDAKBgNVBAwMA1pCWDEUMBIGA1UECwwLc2lnbi12ZXJpZnkxFDASBgNVBAsMC3NpZ24tdmVyaWZ5MQwwCgYDVQQKDANaQlgxCzAJBgNVBAYTAkNOMFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEZt1sSLl9odkeMcLpwzJS9pAGvUl32+n1SL+yxiVDG7vF2PCvZ9i2tLMN+b+fyJojvJ85L1Qpco0x2AGDh6y9pDAKBggqgRzPVQGDdQNHADBEAiB6jcEm7oQcBctteE2gzcRn7ySEtdOBchxVQ+Mc/P7iTwIgsT3HPMtr0if45PVfcGbvNnmzKyZ8SDyZsrBFn4lj2aQ=";

        // PemReader pemReader = new PemReader(new StringReader(pemText));
        // PemObject obj = pemReader.readPemObject();
        // pemReader.close();
        // byte[] pembytes = obj.getContent();
        PKCS10CertificationRequest csr = new PKCS10CertificationRequest(Base64.decodeBase64(pemText));
        JcaContentVerifierProviderBuilder providerBuilder = new JcaContentVerifierProviderBuilder();
        ContentVerifierProvider verifierProvider = providerBuilder.build(csr.getSubjectPublicKeyInfo());
        boolean signatureValid = csr.isSignatureValid(verifierProvider);
        System.out.println(signatureValid);
    }

}
