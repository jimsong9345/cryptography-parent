package com.study.crypto.certificate.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.study.crypto.dto.RequestRandomDto;
import com.study.crypto.dto.ResponseRandomDto;
import com.study.crypto.dto.SignInfoDto;
import com.study.crypto.dto.ca.*;
import com.study.crypto.dto.ca.RequestPkcs10Dto.RequestPkcs10DtoData;
import com.study.crypto.dto.ca.RequestSealCertDistinctNameDto.RequestSealCertDistinctNameDtoData;
import com.study.crypto.dto.ca.RequestSealCertDto.RequestSealCertDtoData;
import com.study.crypto.dto.ca.ResponseCheckResultSealCertDto.CheckResultSealCertDtoPackage;
import com.study.crypto.certificate.server.entity.CertificateApply;
import com.study.crypto.certificate.server.entity.Certification;
import com.study.crypto.certificate.server.entity.KeyStorage;
import com.study.crypto.certificate.server.service.CertificateApplyService;
import com.study.crypto.certificate.server.service.CertificationService;
import com.study.crypto.certificate.server.service.KeyStorageService;
import com.study.crypto.certificate.server.service.SealService;
import com.study.crypto.signer.Signer;
import com.study.crypto.signer.SignerFactory;
import com.study.crypto.utils.CertUtils;
import com.study.crypto.utils.EssPdfUtil;
import com.study.crypto.utils.HttpUtils;
import com.study.crypto.utils.KeyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.sql.DataSource;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

/**
 * @author Songjin
 * @since 2021-01-11 15:40
 */
@Slf4j
@ActiveProfiles({"test"})
@SpringBootTest
class CertificateClientTest {
    
    @Autowired
    private KeyStorageService keyStorageService;
    @Autowired
    private CertificateApplyService certApplyService;
    @Autowired
    private CertificationService certService;
    private final String serverUrl = "http://127.0.0.1:8080";
    private final String dir = "D:/java/JetBrains/workspace03/anysign-gb/anysign-gb-server/target";
    
    private PrivateKey clientPrivateKey;
    private Signer signer;
    
    @BeforeEach
    void before() throws Exception {
        // 获取客户端服务器私钥
        URL keystoreResource = ClassLoader.getSystemResource("client-certs/sm2-client-key.keystore");
        String pemString = FileUtils.readFileToString(new File(keystoreResource.getFile()), StandardCharsets.UTF_8);
        clientPrivateKey = KeyUtils.privateKey(pemString);
        signer = SignerFactory.produce(GMObjectIdentifiers.sm2sign_with_sm3);
    }
    
    @Test
    void testX509Certificate() throws Exception{
        Security.addProvider(new BouncyCastleProvider());
        byte[] bytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        CertificateFactory instance = CertificateFactory.getInstance("X.509", "BC");
        X509Certificate certificate = (X509Certificate) instance.generateCertificate(new ByteArrayInputStream(bytes));
        boolean[] keyUsages = certificate.getKeyUsage();
        for (boolean keyUsage : keyUsages) {
            System.out.print(keyUsage + ", ");
        }
        System.out.println();
    }

    /**
     * 使用 pkcs10 申请证书，签发双证
     */
    @Test
    void testApplyCertByPkcs10() throws Exception{
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String notBefore = formatter.format(now);
        String notAfter = formatter.format(now.plusYears(1));
        String pkcs10 = "MIIBTDCB8wIBADCBkDELMAkGA1UEBhMCQ04xDTALBgNVBAoMBE5IU0ExCzAJBgNVBAgMAjEwMQswCQYDVQQHDAI3MTELMAkGA1UEBwwCMDAxCzAJBgNVBAsMAjAxMRswGQYDVQQMDBI5MTExMDEwODcyMjYxOTQxMUExITAfBgNVBAMMGOWMl+S6rOaVsOWtl+iupOivgea1i+ivlTBZMBMGByqGSM49AgEGCCqBHM9VAYItA0IABG6Wc7Dxc1z3zB9vhyPjhywFBtBbN+laE4y3ZuzL+ew1ndYRflDZQfSesMp5PH5U57ocDROP/E8UZjNWbbd8UFygADAKBggqgRzPVQGDdQNIADBFAiBQH5K9dp64JLy8NlQM4i2cFD+ii/76rFiet6LKJKXxAAIhALRaCsHLpIiRpA0BboBfHIwWSapoVVfEp9iZLVIw83K8";

        RequestPkcs10DtoData pkc10Data = new RequestPkcs10DtoData();
        pkc10Data.setRequestId(EssPdfUtil.genRandomUuid());
        pkc10Data.setPkcs10(pkcs10);
        pkc10Data.setNotBefore(notBefore);
        pkc10Data.setNotAfter(notAfter);
        pkc10Data.setAlgorithm(CaConstants.SIGN_ALGORITHM);

        // 获取随机数 + 申请证书
        String tokenInfo = this.applyServiceRandom();
        RequestPkcs10Dto request = new RequestPkcs10Dto();
        request.setVersion(CaConstants.VERSION);
        request.setTaskCode(CaConstants.TASK_CODE_APPLY_SEAL_CERT);
        request.setTokenInfo(tokenInfo);
        request.getData().add(pkc10Data);
        log.info("证书申请请求，签名原文: {}", JSON.toJSONString(request));
        byte[] signature = signer.sign(JSON.toJSONBytes(request), clientPrivateKey);
        SignInfoDto signInfo = new SignInfoDto();
        signInfo.setSignAlgorithm(GMObjectIdentifiers.sm2sign_with_sm3.getId());
        signInfo.setSignValue(Base64.toBase64String(signature));
        request.setSignInfo(signInfo);
        String requestUrl = serverUrl + "/issue/single/applyCertByPkcs10";
        JSONObject response_ = HttpUtils.doPost(requestUrl, JSON.toJSONString(request));
        log.info("响应数据: {}", response_);

        ResponseCheckResultSealCertDto response = JSON.toJavaObject(response_, ResponseCheckResultSealCertDto.class);
        boolean success = CaConstants.SUCCESS.equals(response.getResultCode());
        boolean innerSuccess = CaConstants.SUCCESS.equals(response.getData().get(0).getErrorCode());
        // 单元测试断言
        Assertions.assertTrue(success && innerSuccess, "证书申请请求异常消息: " + response.getResultCodeMsg());

        ResponseCheckResultSealCertDto.CheckResultSealCertDtoData dtoData = response.getData().get(0);
        CheckResultSealCertDtoPackage package_ = dtoData.getPackage_();
        String signcert = package_.getSignCert();
        String enccert = package_.getEncCert();
        String enckey = package_.getEncKeyProtection();
        File file1 = new File("./target", "sign.cer");
        File file2 = new File("./target", "enc.cer");
        File file3 = new File("./target", "enc_pri.key");
        FileUtils.writeStringToFile(file1, signcert, StandardCharsets.UTF_8);
        FileUtils.writeStringToFile(file2, enccert, StandardCharsets.UTF_8);
        FileUtils.writeStringToFile(file3, enckey, StandardCharsets.UTF_8);
    }

    /**
     * 申请单证，仅签名证书，无加密证书
     */
    @Test
    void testApplySingleCert() throws Exception{
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String notBefore = formatter.format(now);
        String notAfter = formatter.format(now.plusYears(1));
        RequestSealCertDistinctNameDtoData certReqData = new RequestSealCertDistinctNameDtoData();
        certReqData.setRequestId(EssPdfUtil.genRandomUuid());
        certReqData.setCertType(CaConstants.CERT_TYPE_CORPORATE);
        String c = "CN";
        String o = "测试制作系统签名证书测试";
        String cn = "测试制作系统签名证书测试";
        X500Name x500Name = CertUtils.buildDistinctName(c, o, cn);
        certReqData.setDn(x500Name.toString());
        certReqData.setNotBefore(notBefore);
        certReqData.setNotAfter(notAfter);
        certReqData.setAlgorithm(CaConstants.SIGN_ALGORITHM);
        // 产生密钥对并保存到数据库
        String keyId = EssPdfUtil.genRandomUuid();
        byte[] publicKeyBytes = this.produceKeyPair(keyId, "制作系统密钥" + "_" + System.currentTimeMillis());
        certReqData.setSubjectPublicKeyInfo(Base64.toBase64String(publicKeyBytes));

        RequestSealCertDistinctNameDto request = new RequestSealCertDistinctNameDto();
        request.setVersion(CaConstants.VERSION);
        request.setTaskCode(CaConstants.TASK_CODE_APPLY_SEAL_CERT);
        request.setTokenInfo(EssPdfUtil.genRandomUuid());
        request.getData().add(certReqData);
        log.info("证书申请请求，签名原文: {}", JSON.toJSONString(request));
        byte[] signature = signer.sign(JSON.toJSONBytes(request), clientPrivateKey);
        SignInfoDto signInfo = new SignInfoDto();
        signInfo.setSignAlgorithm(GMObjectIdentifiers.sm2sign_with_sm3.getId());
        signInfo.setSignValue(Base64.toBase64String(signature));
        request.setSignInfo(signInfo);
        String requestUrl = serverUrl + "/issue/single/applySealCert";
        JSONObject response_ = HttpUtils.doPost(requestUrl, JSON.toJSONString(request));
        log.info("响应数据: {}", response_);
        ResponseCheckResultSealCertDto response = JSON.toJavaObject(response_, ResponseCheckResultSealCertDto.class);
        boolean success = CaConstants.SUCCESS.equals(response.getResultCode());
        boolean innerSuccess = CaConstants.SUCCESS.equals(response.getData().get(0).getErrorCode());
        // 单元测试断言
        Assertions.assertTrue(success && innerSuccess, "证书申请请求异常消息: " + response.getResultCodeMsg());
        
        CheckResultSealCertDtoPackage package_ = response.getData().get(0).getPackage_();
        byte[] signcertBase64 = Base64.decode(package_.getSignCert());
        int signcertUsage = CertUtils.certificateUsage(signcertBase64);
        X509Certificate signcert = CertUtils.rebuildX509Certificate(signcertBase64);
        Certification signCert_ = new Certification();
        signCert_.setSerialNumber(signcert.getSerialNumber().toString(16).toUpperCase());
        signCert_.setCertDn(signcert.getSubjectDN().getName());
        signCert_.setBeginTime(EssPdfUtil.of(signcert.getNotBefore()));
        signCert_.setEndTime(EssPdfUtil.of(signcert.getNotAfter()));
        signCert_.setKeyId(keyId);
        signCert_.setCertType(Certification.CERT_TYPE_SM2);
        signCert_.setCertificate(signcertBase64);
        signCert_.setKeyUsage(signcertUsage);
        signCert_.setId(EssPdfUtil.genRandomUuid());
        signCert_.setCreateTime(LocalDateTime.now());
        signCert_.setUpdateTime(LocalDateTime.now());
        certService.insert(signCert_);
    }
    
    /**
     * 使用 taskId 获取证书
     */
    @Test
    void testCheckResult() throws Exception{
        RequestCheckResultDto request = new RequestCheckResultDto();
        request.setVersion(CaConstants.VERSION);
        request.setTaskCode(CaConstants.CHECK_RESULT);
        String taskId = readFileToString("task-id");
        request.getData().setTaskId(taskId);
        byte[] signature = signer.sign(JSON.toJSONBytes(request), clientPrivateKey);
        SignInfoDto signInfo = new SignInfoDto();
        signInfo.setSignValue(Base64.toBase64String(signature));
        signInfo.setSignAlgorithm(GMObjectIdentifiers.sm2sign_with_sm3.getId());
        request.setSignInfo(signInfo);
        String requestUrl = serverUrl + "/issue/double/checkResult";
        JSONObject response_ = HttpUtils.doPost(requestUrl, JSON.toJSONString(request));
        ResponseCheckResultSealCertDto response = JSON.toJavaObject(response_, ResponseCheckResultSealCertDto.class);
        boolean success = CaConstants.SUCCESS.equals(response.getResultCode());
        boolean innerSuccess  = CaConstants.SUCCESS.equals(response.getData().get(0).getErrorCode());
        // 单元测试断言
        Assertions.assertTrue(success && innerSuccess, "服务器响应错误");
        
        String keyId = readFileToString("key-id");
        String applyId = readFileToString("apply-id");
        CheckResultSealCertDtoPackage package_ = response.getData().get(0).getPackage_();
        byte[] signcertBytes = Base64.decode(package_.getSignCert());
        byte[] enccertBytes = Base64.decode(package_.getEncCert());
        byte[] envelopedKey = Base64.decode(package_.getEncKeyProtection());
        int signcertUsage = CertUtils.certificateUsage(signcertBytes);
        int enccertUsage = CertUtils.certificateUsage(enccertBytes);
        X509Certificate signcert = CertUtils.rebuildX509Certificate(signcertBytes);
        X509Certificate enccert = CertUtils.rebuildX509Certificate(enccertBytes);
        Certification signCert_ = new Certification();
        signCert_.setId(EssPdfUtil.genRandomUuid());
        signCert_.setSerialNumber(signcert.getSerialNumber().toString(16).toUpperCase());
        signCert_.setCertDn(signcert.getSubjectDN().getName());
        signCert_.setBeginTime(EssPdfUtil.of(signcert.getNotBefore()));
        signCert_.setEndTime(EssPdfUtil.of(signcert.getNotAfter()));
        signCert_.setKeyId(keyId);
        signCert_.setApplyId(applyId);
        signCert_.setCertType(Certification.CERT_TYPE_SM2);
        signCert_.setCertificate(signcertBytes);
        signCert_.setKeyUsage(signcertUsage);
    
        Certification encCert_ = new Certification();
        encCert_.setId(EssPdfUtil.genRandomUuid());
        encCert_.setSerialNumber(enccert.getSerialNumber().toString(16).toUpperCase());
        encCert_.setCertDn(enccert.getSubjectDN().getName());
        encCert_.setBeginTime(EssPdfUtil.of(enccert.getNotBefore()));
        encCert_.setEndTime(EssPdfUtil.of(enccert.getNotAfter()));
        encCert_.setKeyId(keyId);
        encCert_.setApplyId(applyId);
        encCert_.setCertType(Certification.CERT_TYPE_SM2);
        encCert_.setCertificate(enccertBytes);
        encCert_.setKeyUsage(enccertUsage);
        encCert_.setEnvelopedKey(envelopedKey);
        certService.insertList(Arrays.asList(signCert_, encCert_));
    
        // 将密钥对保护结构写入指定目录，后续测试
        String envelopedKeyHex = Hex.toHexString(envelopedKey);
        File file1 = new File(dir, "envelopedKeyHex.txt");
        File file2 = new File(dir, "signCert.txt");
        File file3 = new File(dir, "encCert.txt");
        FileUtils.writeStringToFile(file1, envelopedKeyHex, StandardCharsets.UTF_8);
        FileUtils.writeStringToFile(file2, package_.getSignCert(), StandardCharsets.UTF_8);
        FileUtils.writeStringToFile(file3, package_.getEncCert(), StandardCharsets.UTF_8);
    }
    
    /**
     * 申请证书
     */
    @Test
    void testApplySealCert() throws Exception{
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String notBefore = formatter.format(now);
        String notAfter = formatter.format(now.plusYears(1));
        RequestSealCertDtoData certReqData = new RequestSealCertDtoData();
        certReqData.setRequestId(EssPdfUtil.genRandomUuid());
        certReqData.setCertType(CaConstants.CERT_TYPE_CORPORATE);
        certReqData.setCountryName("CN");
        // certReqData.setOrganizationName("11110000000021039C北京市民政局");
        certReqData.setOrganizationName("911100001011016602京东1");
        certReqData.setCommonName("11010800017635");
        certReqData.setNotBefore(notBefore);
        certReqData.setNotAfter(notAfter);
        certReqData.setAlgorithm(CaConstants.SIGN_ALGORITHM);
        // 产生密钥对并保存到数据库
        String keyName = certReqData.getCommonName() + "_" + System.currentTimeMillis();
        String keyId = EssPdfUtil.genRandomUuid();
        // 将产生的密钥数据 keyId 写入指定文件，方便测试
        writeStringToFile(keyId, "key-id");
        byte[] publicKeyBytes = this.produceKeyPair(keyId, keyName);
        certReqData.setSubjectPublicKeyInfo(Base64.toBase64String(publicKeyBytes));
    
        // 将产生的密钥字节流写入指定文件，方便测试
        KeyStorage keyStorage = keyStorageService.selectByPrimaryKey(keyId);
        String keyBytesHex = Hex.toHexString(keyStorage.getKeyBytes());
        File file1 = new File(dir, "keyStoreHex.txt");
        FileUtils.writeStringToFile(file1, keyBytesHex, StandardCharsets.UTF_8);
    
        // 获取随机数 + 申请证书
        String tokenInfo = this.applyServiceRandom();
        RequestSealCertDto request = new RequestSealCertDto();
        request.setVersion(CaConstants.VERSION);
        request.setTaskCode(CaConstants.TASK_CODE_APPLY_SEAL_CERT);
        request.setTokenInfo(tokenInfo);
        request.getData().add(certReqData);
        log.info("证书申请请求——获取taskId，签名原文: {}", JSON.toJSONString(request));
        byte[] signature = signer.sign(JSON.toJSONBytes(request), clientPrivateKey);
        SignInfoDto signInfo = new SignInfoDto(GMObjectIdentifiers.sm2sign_with_sm3.getId(), signature);
        request.setSignInfo(signInfo);
        String requestUrl = serverUrl + "/issue/double/applySealCert";
        JSONObject response_ = HttpUtils.doPost(requestUrl, JSON.toJSONString(request));
        ResponseSealCertDto response = JSON.toJavaObject(response_, ResponseSealCertDto.class);
        boolean success = CaConstants.SUCCESS.equals(response.getResultCode());
        // 单元测试断言
        Assertions.assertTrue(success, "证书申请请求——获取taskId，异常消息: " + response.getResultCodeMsg());
        
        List<CertificateApply> applies = CertificateApply.getInstances(request);
        applies.forEach(e -> {
            e.setKeystoreId(keyId);
            e.setTaskId(response.getData().getTaskId());
    
            try {
                writeStringToFile(e.getTaskId(), "task-id");
                writeStringToFile(e.getId(), "apply-id");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            certApplyService.insert(e);
        });
    }
    
    /**
     * 产生签名密钥对
     * @param keyName 密钥名称
     * @throws Exception 异常
     */
    private byte[] produceKeyPair(String keyId, String keyName) throws Exception {
        KeyPair keyPair = KeyUtils.generateKeyPair(KeyUtils.ALGO_SM2);
        BCECPrivateKey privateKey = (BCECPrivateKey) keyPair.getPrivate();
        BCECPublicKey publicKey = (BCECPublicKey) keyPair.getPublic();
        byte[] keyPairBytes = KeyUtils.packageKeyPairSM2(publicKey, privateKey);
        log.debug("公钥hex: {}", Hex.toHexString(publicKey.getQ().getEncoded(false)));
        log.debug("私钥hex: {}", Hex.toHexString(privateKey.getD().toByteArray()));
        KeyStorage keyStorage = new KeyStorage();
        keyStorage.setId(keyId);
        keyStorage.setKeyName(keyName);
        keyStorage.setAlias("ESS-PDF");
        keyStorage.setKeySize((short) 256);
        keyStorage.setKeyAlg(KeyUtils.ALGO_SM2);
        keyStorage.setKeyType(KeyStorage.KEY_TYPE_SIGNATURE);
        keyStorage.setKeyBytes(keyPairBytes);
        keyStorage.setCreateTime(LocalDateTime.now());
        keyStorageService.insert(keyStorage);
        // 获取公钥字节数据，包含第一字节前缀 04
        byte[] publicKeyBytes = ByteUtils.subArray(publicKey.getQ().getEncoded(false), 0);
        return BigIntegers.asUnsignedByteArray(new BigInteger(publicKeyBytes));
    }
    
    private void writeStringToFile(String content, String fileName) throws Exception {
        File file = new File("./target", fileName + ".txt");
        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
    }
    
    private String readFileToString(String fileName) throws Exception {
        File file = new File("./target", fileName + ".txt");
        return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    }
    
    /**
     * 生成密钥对，将密钥对写入文件
     */
    @Test
    void testGenerateKeyPair() throws Exception {
	    KeyPair keyPair = KeyUtils.generateKeyPair(KeyUtils.ALGO_SM2);
        BCECPrivateKey privateKey = (BCECPrivateKey) keyPair.getPrivate();
        BCECPublicKey  publicKey  = (BCECPublicKey) keyPair.getPublic();
        String privateKeyHex = Hex.toHexString(privateKey.getD().toByteArray());
        String publicKeyHex = Hex.toHexString(publicKey.getQ().getEncoded(false));
        System.out.println("privateKeyHex: " + privateKeyHex);
        System.out.println("publicKeyHex: " + publicKeyHex);
        System.out.println("publicKey X: " + Hex.toHexString(publicKey.getQ().getXCoord().getEncoded()));
        System.out.println("publicKey Y: " + Hex.toHexString(publicKey.getQ().getYCoord().getEncoded()));
        FileUtils.writeByteArrayToFile(new File("target/privateKey.dat"), privateKey.getD().toByteArray());
        FileUtils.writeByteArrayToFile(new File("target/publicKey.dat"), ByteUtils.subArray(publicKey.getQ().getEncoded(false), 1));
    }
    
    /**
     * 签名验签测试
     */
    @Test
    void testSignAndVerify() throws Exception{
        byte[]     privateKeyBytes = FileUtils.readFileToByteArray(new File("target/privateKey.dat"));
        byte[]     publicKeyBytes  = FileUtils.readFileToByteArray(new File("target/publicKey.dat"));
        PrivateKey privateKey      = KeyUtils.convertPrivateKey(privateKeyBytes);
        PublicKey  publicKey       = KeyUtils.convertPublicKey(publicKeyBytes);
        Signer signer = SignerFactory.produce(GMObjectIdentifiers.sm2sign_with_sm3);
        byte[] inData = "456".getBytes();
        byte[] signedData = signer.sign(inData, privateKey);
        System.out.println(Hex.toHexString(signedData));
        boolean verify = signer.verify(inData, signedData, publicKey);
        System.out.println(verify);
    }
    
    /**
     * 证书申请接口参数校验
     */
    @Test
    void testApplySealCertValidation() throws Exception{
        String tokenInfo = this.applyServiceRandom();
        String requestUrl = serverUrl + "/issue/double/applySealCert";
        RequestSealCertDtoData certReqData = new RequestSealCertDtoData();
        RequestSealCertDto request = new RequestSealCertDto();
        request.setTokenInfo(tokenInfo);
        request.getData().add(certReqData);
        request.setSignInfo(new SignInfoDto());
        JSONObject response = HttpUtils.doPost(requestUrl, JSON.toJSONString(request));
        System.out.println(response);
    }
    
    /**
     * 获取随机数
     */
    @Test
    void testApplyServiceRandom() throws Exception{
        String tokenInfo = this.applyServiceRandom();
        System.out.println(tokenInfo);
    }
    
    private String applyServiceRandom() throws Exception {
        URL systemResource = ClassLoader.getSystemResource("certs/sm2-server-cert.cer");
        byte[] cert = FileUtils.readFileToByteArray(new File(systemResource.getFile()));
        String certBase64 = Base64.toBase64String(cert);
        RequestRandomDto request = RequestRandomDto.getRequestRandomDtoCa();
        request.setRandomA(EssPdfUtil.genRandomUuid().substring(0, 16));
        request.setApplicantCert(certBase64);
        String requestUrl = serverUrl + "/issue/applyServiceRandom";
        JSONObject response_ = HttpUtils.doPost(requestUrl, JSONObject.toJSONString(request));
        ResponseRandomDto response = JSON.toJavaObject(response_, ResponseRandomDto.class);
        return request.getRandomA().concat(response.getRandomB());
    }
    
    @Autowired
    private DataSource dataSource;
    @Autowired
    private SealService sealService;

    @Test
    void testDataSource() throws Exception{
        System.out.println(dataSource);
        System.out.println(sealService);
    }
    
}
