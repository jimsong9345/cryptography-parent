package com.study.crypto.certificate.server.bytebuddy;

/**
 * @author Songjin
 * @since 2023-06-19 10:50
 */
public class Foo {

    public String sayHelloFoo() {
        return "Hello in Foo!";
    }
}
