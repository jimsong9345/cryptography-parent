package com.study.crypto.certificate.server;

import com.github.pagehelper.PageHelper;
import com.study.crypto.certificate.server.config.RedisComponent;
import com.study.crypto.certificate.server.entity.BusinessLog;
import com.study.crypto.certificate.server.entity.Seal;
import com.study.crypto.certificate.server.entity.SysAttr;
import com.study.crypto.certificate.server.mapper.SealMapper;
import com.study.crypto.certificate.server.mapper.SysAttrMapper;
import com.study.crypto.certificate.server.service.BusinessLogService;
import com.study.crypto.certificate.server.service.SealService;
import com.study.crypto.certificate.server.service.SysAttrService;
import com.study.crypto.utils.EssPdfUtil;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author Songjin
 * @since 2021-01-03 0:33
 */
@SpringBootTest
class CertificateServerApplicationTests {
    
    @Autowired
    private DataSource dataSource;
    @Autowired
    private SealService sealService;
    @Autowired
    private SysAttrService sysAttrService;
    @Autowired
    private BusinessLogService businessLogService;
    @Autowired
    private RedisProperties redisProperties;
    @Autowired
    private RedisComponent redisComponent;
    @Autowired
    private SysAttrMapper sysAttrMapper;
    @Autowired
    private SealMapper sealMapper;
    
    @Test
    void testRedis() throws Exception{
        Object username = redisComponent.get("username");
        System.out.println(username);
        redisComponent.set("username", "root");
    
        SysAttr sysAttr = sysAttrService.selectByPrimaryKey("SECRET_REFRESH_URL");
        redisComponent.set("SECRET_REFRESH_URL", sysAttr);
        System.out.println(sysAttr);
    }
    
    @Test
    void testSplitBusinessLog() throws Exception{
        List<BusinessLog> logs = businessLogService.selectAll();
        List<BusinessLog> newLogs = new ArrayList<>();
        logs.forEach(log ->{
            businessLogService.deleteByPrimaryKey(log.getId());
            String[] sealnums = log.getSealNum().split(",");
            for (String sealnum : sealnums) {
                BusinessLog clone = new BusinessLog();
                BeanUtils.copyProperties(log, clone);
                clone.setSealNum(sealnum);
                clone.setId(EssPdfUtil.genRandomUuid());
                newLogs.add(clone);
            }
        });
        newLogs.forEach(log -> {
            businessLogService.insert(log);
        });
    }
    
    @Test
    void testRedisProperties() throws Exception{
        System.out.println(redisProperties);
    }
    
    @Test
    void testDataSource() throws Exception{
        System.out.println(dataSource);
    }
    
    @Test
    void test01() throws Exception{
        SysAttr attr = new SysAttr();
        attr.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        attr.setAttrId("test2");
        attr.setAttrValue("测试属性");
        sysAttrService.insert(attr);
    }
    
    @Test
    void testInsertList() throws Exception{
        SysAttr sysAttr = new SysAttr();
        sysAttr.setId(EssPdfUtil.genRandomUuid());
        sysAttr.setAttrId("username");
        sysAttr.setAttrValue("root");
        byte[] serialize = SerializationUtils.serialize(sysAttr);
        SysAttr sysAttr2 = SerializationUtils.deserialize(serialize);
        sysAttr2.setId(EssPdfUtil.genRandomUuid());
        sysAttr2.setAttrId("password");
        sysAttr2.setAttrValue("123456");
        List<SysAttr> list = Arrays.asList(sysAttr, sysAttr2);
        int i = sysAttrService.insertList(list);
        System.out.println("行数: " + i);
    }
    
    @Test
    void testService() throws Exception {
        String id   = "123";
        Seal   seal = sealService.selectByPrimaryKey(id);
        System.out.println(seal);
    
        SysAttr sysAttr = sysAttrService.selectByPrimaryKey("SECRET_REFRESH_URL");
        System.out.println(sysAttr);
    
        PageHelper.startPage(1, 10);
        List<SysAttr> sysAttrs = sysAttrService.selectAll();
        sysAttrs.forEach(System.out::println);
    
        PageHelper.startPage(2, 10);
        SysAttr sysAttr1 = new SysAttr();
        sysAttr1.setAttrId("CRYPTO_SERVICE_ADDRESS");
        List<SysAttr> select = sysAttrService.select(sysAttr1);
        select.forEach(System.out::println);
    }
    
    @Test
    void testMapper() throws Exception{
        Seal seal = sealMapper.selectByPrimaryKey("123");
        System.out.println(seal);

        SysAttr sysAttr = sysAttrMapper.selectByPrimaryKey("SECRET_REFRESH_URL");
        System.out.println(sysAttr);
    }

    @Test
    void contextLoads() {
        String property = System.getProperty("user.dir");
        System.out.println(property);
        URL systemResource = ClassLoader.getSystemResource("gmssljni.dll");
        System.out.println(systemResource.getFile());
        System.out.println(System.getProperty("java.library.path"));
    }

}
