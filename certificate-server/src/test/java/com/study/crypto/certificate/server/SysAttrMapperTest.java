package com.study.crypto.certificate.server;

import com.study.crypto.certificate.server.entity.SysAttr;
import com.study.crypto.certificate.server.mapper.SysAttrMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Songjin
 * @since 2023-01-04 13:44
 */
@Slf4j
@SpringBootTest
class SysAttrMapperTest {

    @Autowired
    private SysAttrMapper sysAttrMapper;

    @Test
    void testInsert() throws Exception{
        SysAttr sysAttr = SysAttr.builder()
                                 .attrId("username")
                                 .attrValue("admin")
                                 .attrRemake("备注")
                                 .build();
        sysAttrMapper.insert(sysAttr);
    }

}
