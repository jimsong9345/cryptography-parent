package com.study.crypto.certificate.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.study.crypto.asn1.sm2.SM2EnvelopedKey;
import com.study.crypto.certificate.server.utils.CertUtils_;
import com.study.crypto.dto.RequestRandomDto;
import com.study.crypto.utils.*;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.bc.BcX509ExtensionUtils;
import org.bouncycastle.crypto.engines.SM2Engine.Mode;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Songjin
 * @since 2021-01-03 17:40
 */
class CertificateIssuerTest {
    
    @Test
    void testReadClassPathFile() throws Exception{
        File   clientCertFile  = ResourceUtils.getFile("classpath:client-certs/sm2-client-cert.cer");
        byte[] clientCertBytes = FileUtils.readFileToByteArray(clientCertFile);
        System.out.println(clientCertBytes);
    }
    
    /**
     * 测试参数校验
     */
    @Test
    void testValidation() throws Exception{
        String requestUrl = "http://127.0.0.1/issue/applyServiceRandom";
        RequestRandomDto request = RequestRandomDto.getRequestRandomDtoCa();
        JSONObject response = HttpUtils.doPost(requestUrl, JSON.toJSONString(request));
        System.out.println(response);
    }
    
    @Test
    void testLongMax() throws Exception{
        System.out.println(Long.MAX_VALUE);
        System.out.println(KeyUsage.decipherOnly);
    }

    @Test
    void testX500Name() throws Exception{
        X500NameBuilder subjectBuilder = new X500NameBuilder(BCStyle.INSTANCE);
        X500Name subject = subjectBuilder.addRDN(BCStyle.C, "CN")
                                         .addRDN(BCStyle.O, "测试机构")
                                         .addRDN(BCStyle.CN, ".+/")
                                         .build();
        System.out.println(subject);
        // X500Name x500Name = new X500Name("C=CN,O=测试机构,CN=.+/");
        // System.out.println(x500Name);

        String hexStr = "00c1b8ed034ce9b013";
        byte[] decode = Hex.decode(hexStr);
        System.out.println(Hex.toHexString(decode));

        String str = "MIIBvzCCAWOgAwIBAgIJAMG47QNM6bATMAwGCCqBHM9VAYN1BQAwJDELMAkGA1UEBhMCQ04xFTATBgNVBAMMDENDSVQgU00yIE9DQTAeFw0yNDA0MDkwMjQxMjVaFw0yNjA0MDkwMjQxMjVaMEMxCzAJBgNVBAYTAkNOMTQwMgYDVQQDDCt3SElTUGFCTllRMVBjU2hJai9HdFU2M25pRkNnUjdvZjBHUWJWVXBHNGJrMFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEUZ3ld4CzO+dtztSeicojd9Yy9dJj035cbzjnLd0gdMhKVA8GlqWutCvP36Usa068zb12fmlPG6WUfqUhJy1uD6NdMFswHwYDVR0jBBgwFoAUJd5s3qD1d79lKi2yX18F4+2xywMwDgYDVR0PAQH/BAQDAgbAMB0GA1UdDgQWBBQX/Ci3ho7LbKOBJtHqozoeRA0MTTAJBgNVHRMEAjAAMAwGCCqBHM9VAYN1BQADSAAwRQIhAOFHSv/pxRpX2MQU+8/rw/dbRJspioPNXBbUlCdd6cZ4AiAABgttumUhb8Px/NhKJHw4//yf4g8X25fxwh4qAHwEkw==";
        byte[] decode1 = Base64.decode(str);
        Certificate serverCert = Certificate.getInstance(decode1);
        byte[] bytes = serverCert.getSerialNumber().getPositiveValue().toByteArray();
        System.out.println(Hex.toHexString(bytes));
    }

    
    @Test
    void testIssueCertificate2() throws Exception{
        String pubkeyBase64 = "BAzFUBzY3/emW34WVtQUypFDRluRDeAKGEAUldbgKtKje/AqRJ69lTe6eSVCmcUaZirSlxaxpakhbWgHMP5OHOs=";
        X500NameBuilder subjectBuilder = new X500NameBuilder(BCStyle.INSTANCE);
        X500Name subject = subjectBuilder.addRDN(BCStyle.C, "CN")
                                         .addRDN(BCStyle.O, "11110000000021039C北京市民政局")
                                         .addRDN(BCStyle.CN, "11011500009158").build();
        File          serverKeyFile = ResourceUtils.getFile("classpath:certs/sm2-server-key.keystore");
        String        pemString     = FileUtils.readFileToString(serverKeyFile, StandardCharsets.UTF_8);
        PrivateKey    privateKey    = KeyUtils.privateKey(pemString);
        ContentSigner signer        = new JcaContentSignerBuilder("SM3WithSM2").setProvider("BC").build(privateKey);
    
        File        serverCertFile  = ResourceUtils.getFile("classpath:certs/sm2-server-cert.cer");
        byte[]      serverCertBytes = FileUtils.readFileToByteArray(serverCertFile);
        Certificate serverCert      = Certificate.getInstance(serverCertBytes);
    
        LocalDateTime notBefore = LocalDateTime.now();
        LocalDateTime notAfter  = notBefore.plusYears(1);
    
        BigInteger serial = new BigInteger("12382358324098");
        Certificate certificate = CertUtils.generateUserCertificate(pubkeyBase64, subject, signer, serverCert, notBefore, notAfter, serial);
        FileUtils.writeByteArrayToFile(new File("D:/sm2-user2.cer"), certificate.getEncoded());
    }
    
    /**
     * 使用服务器证书签发证书
     */
    @Test
    void testIssueCertificate() throws Exception{
        BigInteger    serial     = new BigInteger("12382358324098");
        LocalDateTime notBefore_ = LocalDateTime.now();
        LocalDateTime notAfter_  = notBefore_.plusYears(1);
        Date          notBefore  = EssPdfUtil.of(notBefore_);
        Date          notAfter   = EssPdfUtil.of(notAfter_);
        X500NameBuilder subjectBuilder = new X500NameBuilder(BCStyle.INSTANCE);
        X500Name subject = subjectBuilder.addRDN(BCStyle.C, "CN")
                                         .addRDN(BCStyle.O, "11110000000021039C北京市民政局")
                                         .addRDN(BCStyle.CN, "11011500009158").build();
        String                   pubkeyBase64    = "BAzFUBzY3/emW34WVtQUypFDRluRDeAKGEAUldbgKtKje/AqRJ69lTe6eSVCmcUaZirSlxaxpakhbWgHMP5OHOs=";
        SubjectPublicKeyInfo     subjectKeyInfo  = KeyUtils.obtainSubjectPublicKeyInfoBase64(pubkeyBase64);
        File                     serverKeyFile   = ResourceUtils.getFile("classpath:certs/sm2-server-key.keystore");
        String                   pemString       = FileUtils.readFileToString(serverKeyFile, StandardCharsets.UTF_8);
        PrivateKey               privateKey      = KeyUtils.privateKey(pemString);
        File                     serverCertFile  = ResourceUtils.getFile("classpath:certs/sm2-server-cert.cer");
        byte[]                   serverCertBytes = FileUtils.readFileToByteArray(serverCertFile);
        Certificate              serverCert      = Certificate.getInstance(serverCertBytes);
        ContentSigner            signer          = new JcaContentSignerBuilder("SM3WithSM2").setProvider("BC").build(privateKey);
        BcX509ExtensionUtils     extUtils        = new BcX509ExtensionUtils();
        X509v3CertificateBuilder builder         = new X509v3CertificateBuilder(serverCert.getSubject(), serial, notBefore, notAfter, subject, subjectKeyInfo);
        builder.addExtension(Extension.basicConstraints, true, new BasicConstraints(false));
        builder.addExtension(Extension.keyUsage, true, new KeyUsage(KeyUsage.digitalSignature | KeyUsage.nonRepudiation));
        // 授权密钥标识
        builder.addExtension(Extension.authorityKeyIdentifier, false, extUtils.createAuthorityKeyIdentifier(serverCert.getSubjectPublicKeyInfo()));
        // 使用者密钥标识
        builder.addExtension(Extension.subjectKeyIdentifier, false, extUtils.createSubjectKeyIdentifier(subjectKeyInfo));
        X509CertificateHolder certificateHolder = builder.build(signer);
        Certificate certificate = certificateHolder.toASN1Structure();
        FileUtils.writeByteArrayToFile(new File("D:/sm2-user-signcert.cer"), certificate.getEncoded());
    }
    
    /**
     * 签发加密证书，同时生成密钥对保护结构
     */
    @Test
    void testIssueEncCertificate() throws Exception{
        BigInteger serial = new BigInteger("12382358324098");
        LocalDateTime notBefore_ = LocalDateTime.now();
        LocalDateTime notAfter_ = notBefore_.plusYears(1);
        Date notBefore = EssPdfUtil.of(notBefore_);
        Date notAfter = EssPdfUtil.of(notAfter_);
        X500NameBuilder subjectBuilder = new X500NameBuilder(BCStyle.INSTANCE);
        X500Name subject = subjectBuilder.addRDN(BCStyle.C, "CN")
                                         .addRDN(BCStyle.O, "11110000000021039C北京市民政局")
                                         .addRDN(BCStyle.CN, "11011500009158").build();
        String pubkeyBase64 = "BAzFUBzY3/emW34WVtQUypFDRluRDeAKGEAUldbgKtKje/AqRJ69lTe6eSVCmcUaZirSlxaxpakhbWgHMP5OHOs=";
        SubjectPublicKeyInfo subjectKeyInfo = KeyUtils.obtainSubjectPublicKeyInfoBase64(pubkeyBase64);
        File serverKeyFile = ResourceUtils.getFile("classpath:certs/sm2-server-key.keystore");
        String pemString = FileUtils.readFileToString(serverKeyFile, StandardCharsets.UTF_8);
        PrivateKey privateKey = KeyUtils.privateKey(pemString);
        File serverCertFile = ResourceUtils.getFile("classpath:certs/sm2-server-cert.cer");
        byte[] serverCertBytes = FileUtils.readFileToByteArray(serverCertFile);
        Certificate serverCert = Certificate.getInstance(serverCertBytes);
        ContentSigner signer = new JcaContentSignerBuilder("SM3WithSM2").setProvider("BC").build(privateKey);
        BcX509ExtensionUtils extUtils = new BcX509ExtensionUtils();
        X509v3CertificateBuilder builder = new X509v3CertificateBuilder(serverCert.getSubject(), serial, notBefore, notAfter, subject, subjectKeyInfo);
        builder.addExtension(Extension.basicConstraints, true, new BasicConstraints(false));
        builder.addExtension(Extension.keyUsage, true, new KeyUsage(KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.keyAgreement));
        // 授权密钥标识
        builder.addExtension(Extension.authorityKeyIdentifier, false, extUtils.createAuthorityKeyIdentifier(serverCert.getSubjectPublicKeyInfo()));
        // 使用者密钥标识
        builder.addExtension(Extension.subjectKeyIdentifier, false, extUtils.createSubjectKeyIdentifier(subjectKeyInfo));
        X509CertificateHolder certificateHolder = builder.build(signer);
        Certificate certificate = certificateHolder.toASN1Structure();
        FileUtils.writeByteArrayToFile(new File("D:/sm2-user-enccert.cer"), certificate.getEncoded());
    }
    
    /**
     * 测试sm2密钥对保护数据格式
     */
    @Test
    void testEnvelopedKey() throws Exception{
        KeyPair        keyPair_    = KeyUtils.generateKeyPair(KeyUtils.ALGO_SM2);
        BCECPrivateKey privateKey_ = (BCECPrivateKey) keyPair_.getPrivate();
        BCECPublicKey  publicKey_  = (BCECPublicKey) keyPair_.getPublic();
        byte[] symmetric = SM4Utils.generateKey();
        KeyPair        keyPair    = KeyUtils.generateKeyPair(KeyUtils.ALGO_SM2);
        BCECPrivateKey privateKey = (BCECPrivateKey) keyPair.getPrivate();
        BCECPublicKey  publicKey  = (BCECPublicKey) keyPair.getPublic();
    
        // 1.对称密码算法标识
        AlgorithmIdentifier symAlgID = new AlgorithmIdentifier(GMObjectIdentifiers.sms4_ecb, DERNull.INSTANCE);
    
        // 2.对称密钥的 sm2 密文结构: SM2Cipher
        byte[] encrypt = SM2Utils.encrypt(publicKey_, symmetric, Mode.C1C3C2);
        ASN1Sequence cipher = ASN1Sequence.getInstance(encrypt);
    
        // 3.公钥 sm2PublicKey
        DERBitString sm2PublicKey = new DERBitString(publicKey.getQ().getEncoded(false));
        
        // 4.私钥密文 sm2EncryptedPrivateKey
        byte[] privateKeyBytes = BigIntegers.asUnsignedByteArray(privateKey.getD());
        byte[] encryptedPrivateKey = SM4Utils.encrypt_ecb_nopadding(symmetric, privateKeyBytes);
        DERBitString sm2EncryptedPrivateKey = new DERBitString(encryptedPrivateKey);
    
        SM2EnvelopedKey envelopedKey = new SM2EnvelopedKey(symAlgID, cipher, sm2PublicKey, sm2EncryptedPrivateKey);
        FileUtils.writeByteArrayToFile(new File("D:/envelopedKey.dat"), envelopedKey.getEncoded(ASN1Encoding.DER));
        
        // dec-1.使用签名私钥解对称密钥
        byte[] symmetric_ = SM2Utils.decrypt(privateKey_, encrypt, Mode.C1C3C2);
        byte[] encryptedPrivateKey_ = SM4Utils.decrypt_ecb_nopadding(symmetric_, encryptedPrivateKey);
        System.out.println("privateKeyBytes: " + Hex.toHexString(privateKeyBytes));
        System.out.println("encryptedPrivateKey_: " + Hex.toHexString(encryptedPrivateKey_));
        System.out.println("symmetric: " + Hex.toHexString(symmetric));
        System.out.println("symmetric_: " + Hex.toHexString(symmetric_));
    }
    
    @Test
    void testSM2Decrypt() throws Exception{
        String privateKeyBase64 = "2y7gAauzB8wYF4CsK17ymEgG2fFvWWBi6PDFCevKXl8=";
        String encryptedBase64 = "MHkCIBS8jBYfWSAKHfDxAkSVH2ldEMougR+n1+Eh4qWtH741AiEAm+gUgUrUjArcJg89UwtTmeZX6Cfl2kuUnVbIYR8tX8UEIJByph9fMR6zwGnP5aFWscbnOEVq2QbFMrA5GD85UBZRBBB3159qNnEH89F4O//gmhH+";
        byte[] privateKeyBytes = Base64.decode(privateKeyBase64);
        byte[] encryptedBytes = Base64.decode(encryptedBase64);
        PrivateKey privateKey = KeyUtils.convertPrivateKey(privateKeyBytes);
    
        byte[] decrypt = SM2Utils.decrypt(privateKey, encryptedBytes, Mode.C1C3C2);
        System.out.println(decrypt);
    }
    
    @Test
    void test_sm4() throws Exception{
        byte[] key = SM4Utils.generateKey();
        byte[] bytes = SM4Utils.encrypt_ecb_padding(key, "12345678901234".getBytes());
        byte[] bytes1 = SM4Utils.decrypt_ecb_padding(key, bytes);
        System.out.println(Base64.toBase64String(key));
        System.out.println(Base64.toBase64String(bytes));
        System.out.println(new String(bytes1));
    }
    
    @Test
    void testCacerts() throws Exception{
        // 加载JDK的cacerts keystore文件
        String          filename = System.getProperty("java.home") + "/lib/security/cacerts".replace('/', File.separatorChar);
        FileInputStream is       = new FileInputStream(filename);
        KeyStore        keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        String          password = "changeit";
        keystore.load(is, password.toCharArray());
    
        //此类用于从keystore遍历出受信的CA
        PKIXParameters params = new PKIXParameters(keystore);
    
        // 获取包含受信的CA证书的一组信任锚点
        for (TrustAnchor ta : params.getTrustAnchors()) {
            // 获取证书
            X509Certificate cert = ta.getTrustedCert();
            System.out.println(cert);
        }
    }
    
    
    /**
     * 使用根证书签发CA证书
     */
    @Test
    void testIssueCACertificate() throws Exception {
        // 生成密钥对
        KeyPair ecckey = KeyUtils.generateKeyPair(KeyUtils.ALGO_ECC);
        KeyPair sm2key = KeyUtils.generateKeyPair(KeyUtils.ALGO_SM2);
        KeyPair rsakey = KeyUtils.generateKeyPair(KeyUtils.ALGO_RSA);
        SubjectPublicKeyInfo eccSubjectKeyInfo = SubjectPublicKeyInfo.getInstance(ecckey.getPublic().getEncoded());
        SubjectPublicKeyInfo sm2SubjectKeyInfo = SubjectPublicKeyInfo.getInstance(sm2key.getPublic().getEncoded());
        SubjectPublicKeyInfo rsaSubjectKeyInfo = SubjectPublicKeyInfo.getInstance(rsakey.getPublic().getEncoded());
        // 根证书签发CA证书
        LocalDateTime notBefore_ = LocalDateTime.now();
        LocalDateTime notAfter_  = notBefore_.plusYears(10);
        
        Date notBefore = EssPdfUtil.of(notBefore_);
        Date notAfter  = EssPdfUtil.of(notAfter_);
        X500NameBuilder ecckeyBuilder = new X500NameBuilder(BCStyle.INSTANCE);
        X500NameBuilder sm2Builder = new X500NameBuilder(BCStyle.INSTANCE);
        X500NameBuilder rsaBuilder = new X500NameBuilder(BCStyle.INSTANCE);
        X500Name eccX500Name = ecckeyBuilder.addRDN(BCStyle.C, "CN").addRDN(BCStyle.O, "BJCA").addRDN(BCStyle.OU, "BJCA").addRDN(BCStyle.CN, "ECC Server CA").build();
        X500Name sm2X500Name = sm2Builder.addRDN(BCStyle.C, "CN").addRDN(BCStyle.O, "BJCA").addRDN(BCStyle.OU, "BJCA").addRDN(BCStyle.CN, "SM2 Server CA").build();
        X500Name rsaX500Name = rsaBuilder.addRDN(BCStyle.C, "CN").addRDN(BCStyle.O, "BJCA").addRDN(BCStyle.OU, "BJCA").addRDN(BCStyle.CN, "RSA Server CA").build();
        BigInteger eccSerial = new BigInteger("1C100000000000000002", 16);
        BigInteger sm2Serial = new BigInteger("1A100000000000000002", 16);
        BigInteger rsaSerial = new BigInteger("1B100000000000000002", 16);
        Certificate eccCertificate = this.issueCAcertificate("SHA256WITHECDSA", eccSerial, notBefore, notAfter, eccX500Name, eccSubjectKeyInfo);
        Certificate sm2Certificate = this.issueCAcertificate("SM3WITHSM2", sm2Serial, notBefore, notAfter, sm2X500Name, sm2SubjectKeyInfo);
        Certificate rsaCertificate = this.issueCAcertificate("SHA256WITHRSA", rsaSerial, notBefore, notAfter, rsaX500Name, rsaSubjectKeyInfo);
        // 写入密钥到文件
        String eccKey = KeyUtils.toPEMText(ecckey);
        String sm2Key = KeyUtils.toPEMText(sm2key);
        String rsaKey = KeyUtils.toPEMText(rsakey);
        FileUtils.writeStringToFile(new File("target/certs/ecc-server-key.keystore"), eccKey, StandardCharsets.UTF_8);
        FileUtils.writeStringToFile(new File("target/certs/sm2-server-key.keystore"), sm2Key, StandardCharsets.UTF_8);
        FileUtils.writeStringToFile(new File("target/certs/rsa-server-key.keystore"), rsaKey, StandardCharsets.UTF_8);
        // 写入证书DER到文件
        FileUtils.writeByteArrayToFile(new File("target/certs/ecc-server-cert.cer"), eccCertificate.getEncoded());
        FileUtils.writeByteArrayToFile(new File("target/certs/sm2-server-cert.cer"), sm2Certificate.getEncoded());
        FileUtils.writeByteArrayToFile(new File("target/certs/rsa-server-cert.cer"), rsaCertificate.getEncoded());
    }
    
    private Certificate issueCAcertificate(String algorithm, BigInteger serial, Date notBefore, Date notAfter, X500Name subject, SubjectPublicKeyInfo subjectKeyInfo) throws Exception {
        File rootCertFile;
        File rootKeyFile;
        if ("SM3WITHSM2".equals(algorithm)) {
            rootCertFile = ResourceUtils.getFile("classpath:certs/sm2-root-cert.cer");
            rootKeyFile  = ResourceUtils.getFile("classpath:certs/sm2-root-key.keystore");
        } else if ("SHA256WITHECDSA".equals(algorithm)) {
            rootCertFile = ResourceUtils.getFile("classpath:certs/ecc-root-cert.cer");
            rootKeyFile  = ResourceUtils.getFile("classpath:certs/ecc-root-key.keystore");
        } else {
            rootCertFile = ResourceUtils.getFile("classpath:certs/rsa-root-cert.cer");
            rootKeyFile  = ResourceUtils.getFile("classpath:certs/rsa-root-key.keystore");
        }
        BcX509ExtensionUtils extUtils = new BcX509ExtensionUtils();
        Certificate rootCert = Certificate.getInstance(FileUtils.readFileToByteArray(rootCertFile));
        String pemString = FileUtils.readFileToString(rootKeyFile, StandardCharsets.UTF_8);
        PrivateKey privateKey = KeyUtils.privateKey(pemString);
        ContentSigner signer = new JcaContentSignerBuilder(algorithm).setProvider("BC").build(privateKey);
        
        X509v3CertificateBuilder builder = new X509v3CertificateBuilder(rootCert.getSubject(), serial, notBefore, notAfter, subject, subjectKeyInfo);
        builder.addExtension(Extension.basicConstraints, true, new BasicConstraints(0));
        builder.addExtension(Extension.keyUsage, true, new KeyUsage(KeyUsage.digitalSignature | KeyUsage.nonRepudiation));
        // 授权密钥标识
        builder.addExtension(Extension.authorityKeyIdentifier, false, extUtils.createAuthorityKeyIdentifier(rootCert.getSubjectPublicKeyInfo()));
        // 使用者密钥标识
        builder.addExtension(Extension.subjectKeyIdentifier, false, extUtils.createSubjectKeyIdentifier(subjectKeyInfo));
        X509CertificateHolder certificateHolder = builder.build(signer);
        return certificateHolder.toASN1Structure();
    }
    
    /**
     * 产生自签名根证书
     */
    @Test
    void testIssueSelfSignedCertificate() throws Exception{
        // 生成密钥对
        KeyPair eccrootcakey = KeyUtils.generateKeyPair(KeyUtils.ALGO_ECC);
        KeyPair sm2rootcakey = KeyUtils.generateKeyPair(KeyUtils.ALGO_SM2);
        KeyPair rsarootcakey = KeyUtils.generateKeyPair(KeyUtils.ALGO_RSA);
        // 自签发根证书
        Date notBefore = new Date();
        Date notAfter  = new Date(notBefore.getTime() + 10 * 1000 * 60 * 60 * 24 * 365L);
        X500NameBuilder eccbuilder = new X500NameBuilder(BCStyle.INSTANCE);
        X500NameBuilder sm2builder = new X500NameBuilder(BCStyle.INSTANCE);
        X500NameBuilder rsabuilder = new X500NameBuilder(BCStyle.INSTANCE);
        X500Name eccSubject = eccbuilder.addRDN(BCStyle.C, "CN").addRDN(BCStyle.CN, "ECC Root CA").build();
        X500Name sm2Subject = sm2builder.addRDN(BCStyle.C, "CN").addRDN(BCStyle.CN, "SM2 Root CA").build();
        X500Name rsaSubject = rsabuilder.addRDN(BCStyle.C, "CN").addRDN(BCStyle.CN, "RSA Root CA").build();
        BigInteger sm2Serial = new BigInteger("1A100000000000000001", 16);
        BigInteger rsaSerial = new BigInteger("1B100000000000000001", 16);
        BigInteger eccSerial = new BigInteger("1C100000000000000001", 16);
        Certificate eccCaCert = CertUtils_.generateSelfSignedCertificate(eccSubject, eccrootcakey, notBefore, notAfter, eccSerial);
        Certificate sm2CaCert = CertUtils_.generateSelfSignedCertificate(sm2Subject, sm2rootcakey, notBefore, notAfter, sm2Serial);
        Certificate rsaCaCert = CertUtils_.generateSelfSignedCertificate(rsaSubject, rsarootcakey, notBefore, notAfter, rsaSerial);
        // 写入密钥到文件
        String eccKey = KeyUtils.toPEMText(eccrootcakey);
        String sm2Key = KeyUtils.toPEMText(sm2rootcakey);
        String rsaKey = KeyUtils.toPEMText(rsarootcakey);
        FileUtils.writeStringToFile(new File("target/certs/ecc-root-key.keystore"), eccKey, StandardCharsets.UTF_8);
        FileUtils.writeStringToFile(new File("target/certs/sm2-root-key.keystore"), sm2Key, StandardCharsets.UTF_8);
        FileUtils.writeStringToFile(new File("target/certs/rsa-root-key.keystore"), rsaKey, StandardCharsets.UTF_8);
        // 写入证书DER到文件
        FileUtils.writeByteArrayToFile(new File("target/certs/ecc-root-cert.cer"), eccCaCert.getEncoded());
        FileUtils.writeByteArrayToFile(new File("target/certs/sm2-root-cert.cer"), sm2CaCert.getEncoded());
        FileUtils.writeByteArrayToFile(new File("target/certs/rsa-root-cert.cer"), rsaCaCert.getEncoded());
    }
    
}
