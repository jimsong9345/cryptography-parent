package com.study.crypto.certificate.server.bytebuddy;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;
import org.junit.jupiter.api.Test;

/**
 * @author Songjin
 * @since 2023-06-19 10:48
 */
class ByteBuddyTest {

    @Test
    void test() throws Exception {
        ByteBuddy byteBuddy = new ByteBuddy();
        DynamicType.Unloaded<Foo> dynamicType = byteBuddy.subclass(Foo.class)
                                                         .method(ElementMatchers.named("sayHelloFoo"))
                                                         .intercept(MethodDelegation.to(Bar.class))
                                                         .make();
        Foo foo = dynamicType.load(getClass().getClassLoader())
                             .getLoaded()
                             .newInstance();
        String r = foo.sayHelloFoo();
        System.out.println(r);
    }
}
