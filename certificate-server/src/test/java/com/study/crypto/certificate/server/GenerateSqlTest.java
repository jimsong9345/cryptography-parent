package com.study.crypto.certificate.server;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.LocalDate;
import java.util.Arrays;

/**
 * @author Songjin
 * @since 2021-01-05 10:20
 */
class GenerateSqlTest {
    
    @Test
    void test() throws Exception{
        String format01 = "insert into bo_businesslog_20200500 select * from bo_businesslog_20200400 where opttime>='%s 00:00:00' and opttime<='%s 23:59:59';";
        String format02 = "delete from bo_businesslog_20200400 where opttime>='%s 00:00:00' and opttime<='%s 23:59:59';";
        LocalDate firstDate = LocalDate.of(2020, 5, 1);
        LocalDate lastDate = LocalDate.of(2020, 5, 31);
        File file = new File("D:/the-sql.sql");
        while (lastDate.isAfter(firstDate) || lastDate.isEqual(firstDate)) {
            String firstDateStr = firstDate.toString();
            String sql01 = String.format(format01, firstDateStr, firstDateStr);
            String sql02 = String.format(format02, firstDateStr, firstDateStr);
            firstDate = firstDate.plusDays(1);
            FileUtils.writeLines(file, Arrays.asList(sql01, sql02, "-- -----"), true);
        }
    }
    
}
