package org.study.gmssl;

import org.bouncycastle.util.encoders.Base64;
import org.gmssl.GmSSL;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author Songjin
 * @since 2020-12-06 21:30
 */
class GmsslTest {
    
    GmSSL gmssl = new GmSSL();
    
    
    @Test
    void testGenerateRandom() throws Exception {
        byte[] bytes = gmssl.generateRandom(16);
        String base64String = Base64.toBase64String(bytes);
        System.out.println(base64String);
    }
    
    @Test
    void testGetDeriveKeyAlgorithms() throws Exception {
        String[] deriveKeyAlgorithms = gmssl.getDeriveKeyAlgorithms();
        List<String> list = Arrays.asList(deriveKeyAlgorithms);
        list.forEach(System.out::println);
    }
    
    @Test
    void testGetPublicKeyEncryptions() throws Exception {
        String[]     publicKeyEncryptions = gmssl.getPublicKeyEncryptions();
        List<String> list                 = Arrays.asList(publicKeyEncryptions);
        list.forEach(System.out::println);
    }
    
    @Test
    void testGetSignAlgorithms() throws Exception {
        String[] signAlgorithms = gmssl.getSignAlgorithms();
        List<String> list = Arrays.asList(signAlgorithms);
        list.forEach(System.out::println);
    }
    
    @Test
    void testGetMacs() throws Exception {
        String[] macs = gmssl.getMacs();
        List<String> list = Arrays.asList(macs);
        list.forEach(System.out::println);
    }
    
    @Test
    void testGetDigests() throws Exception {
        String[] digests = gmssl.getDigests();
        List<String> list = Arrays.asList(digests);
        list.forEach(System.out::println);
    }
    
    @Test
    void testGetCiphers() throws Exception {
        String[] ciphers = gmssl.getCiphers();
        List<String> list = Arrays.asList(ciphers);
        list.forEach(System.out::println);
    }
    
    @Test
    void testGetVersions() throws Exception {
        String[] versions = gmssl.getVersions();
        List<String> list = Arrays.asList(versions);
        list.forEach(System.out::println);
    }
    
    @Test
    void testLibraryPath() throws Exception {
        String property = System.getProperty("java.library.path");
        System.out.println(property);
    }
    
}
