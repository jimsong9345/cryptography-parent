#!/bin/bash

#region 取当前目录
BASE_PATH=$(
  cd "$(dirname "$0")"
  pwd
)
HOME_PATH="$HOME"
# endregion
# region 使用到的基本变量
APP_BASE_PATH=$(
  cd "$(dirname "$0")"
  cd ..
  pwd
)

[ -f "$BASE_PATH/log.sh" ] && . $BASE_PATH/log.sh
[ -f "$BASE_PATH/common.sh" ] && . $BASE_PATH/common.sh
[ -f "$BASE_PATH/env.sh" ] && . $BASE_PATH/env.sh

PIDFILEDIR="$HOME_PATH/.back/${APP_NAME}"
mkdir -p $PIDFILEDIR
APP_PIDFILE="$PIDFILEDIR/pid"
## 用于寻找java程序的位置
if [ -z "$JAVA_HOME" ]; then
  log_failure "JAVA_HOME must be set"
  exit 500
fi

# endregion
#region 加载自定义的扩展配置

extraEnvFiles="conf conf.sh config config.sh"

## 支持 conf, config集中形式， 可以放在bin， conf 目录下
SERVICE_CONFIGS=""
for extraEnvFile in $extraEnvFiles; do
  [ -f "$BASE_PATH/$extraEnvFile" ] && SERVICE_CONFIGS="$SERVICE_CONFIGS $BASE_PATH/$extraEnvFile"
  [ -f "$APP_BASE_PATH/conf/$extraEnvFile" ] && SERVICE_CONFIGS="$SERVICE_CONFIGS $APP_BASE_PATH/conf/$extraEnvFile"
done

# load configuration
CONFIG_LOAD="no"
for SERVICE_CONFIG in $SERVICE_CONFIGS; do
  if [ -f "$SERVICE_CONFIG" ]; then
    (. $SERVICE_CONFIG >/dev/null 2>&1)
    if [ "$?" != "0" ]; then
      log_failure "There are syntax errors in '$SERVICE_CONFIG'"
      exit 1
    else
      log_info "Loading '$SERVICE_CONFIG'"
      . $SERVICE_CONFIG
      CONFIG_LOAD="yes"
      break
    fi
  fi
done
#endregion
#region 判断当前用户是否可以执行应用
if [ "$user" == "" ] || [ "$user" == "$(getCurrentUser)" ]; then
  log_info "can run $appName as $(getCurrentUser)"
else
  log_failure "can not run $appName as $(getCurrentUser)"
  exit 1
fi
#endregion

####### 系统的特殊配置，默认值，可以通过env等扩展配置覆盖
## 用来控制前台启动还是后台启动
runOnBackground=${runOnBackground:-true}
# 用来控制是否检查health
checkHealthy=${checkHealthy:-true}
# 用来检查是否ready（TODO）
checkReady=${checkReady:-true}

#定义变量:CLASS_PATH、DEFAULT_JAR、LOG_PATH、JAVA_OPTS
APP_PATH=${APP_PATH:-$(dirname "$BASE_PATH")}
CLASS_PATH=${CLASS_PATH:-$APP_PATH/config:$APP_PATH/lib/*}

DEFAULT_JAR=$(find $APP_PATH/lib/ -name *.jar)
LOG_PATH="${APP_PATH}/logs/${APP_NAME}"
if [ ! -d "$LOG_PATH" ]; then
  mkdir -p $LOG_PATH
fi
# region jvm 参数

JAVA_OPTS=${JAVA_OPTS:-$defaultJavaOpts}

JAVA_OPTS="${JAVA_OPTS} -Dlogpath.base=${LOG_PATH}"
JAVA_OPTS="${JAVA_OPTS} ${EXTRA_OPTS}"
JAVA_OPTS="${JAVA_OPTS} ${DEBUG_OPTS}"

TYPHON_PATH=${TYPHON_PATH:-$APP_PATH/config}
JAVA_OPTS="${JAVA_OPTS} -DtyphonPath=${TYPHON_PATH}"

# 默认使用config目录下的 logback 配置文件
if [[ $(echo $JAVA_OPTS | grep "Dlogging.config") == "" ]]; then
  JAVA_OPTS="${JAVA_OPTS} -Dlogging.config=${APP_PATH}/config/logback-spring.xml"
fi
#默认utf8
if [[ $(echo $JAVA_OPTS | grep "Dfile.encoding") == "" ]]; then
  JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=UTF-8"
fi

JAVA_OPTS="${JAVA_OPTS} -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${LOG_PATH}/${APP_NAME}.heap.bin -XX:+PrintGC -Xloggc:${LOG_PATH}/${APP_NAME}.gc.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+PrintTenuringDistribution -XX:+PrintGCApplicationStoppedTime -XX:+PrintHeapAtGC -XX:+PrintFlagsFinal"
# endregion

echo "=========env========"
echo "APP_PATH=$APP_PATH"
echo "BASE_PATH=$BASE_PATH"
echo "DEFAULT_JAR=$DEFAULT_JAR"
echo "CLASS_PATH=$CLASS_PATH"
echo "LOG_PATH=$LOG_PATH"
echo "JAVA_OPTS=$JAVA_OPTS"
echo "=========env========"

cd $APP_PATH
ulimit -HSn ${ULIMIT}
#
#
# 获取当前服务的pid
# 1、优先使用 APP_PIDFILE变量， 如果里面存储的pid不存在，使用步骤2
# 2、使用$APP_NAME进行查找，如果查找到多条，使用全路径查找, 否则直接用appName查询
# 3、如果查不到返回值为非0
#
getPID() {
  [ -f "$APP_PIDFILE" ] && appId=$(cat $APP_PIDFILE)
  if [ ! -z "$appId" ]; then
    ps -p $appId 1>/dev/null
    [ "$?" = "1" ] && appId=""
  fi
  if [ -z "$appId" ]; then
    count=$(pgrep -f $APP_NAME | grep java | wc -l)
    [ "$count" == "0" ] && return 1
    # if [ "$count" -gt "1" ]; then
    appId=$(ps aux | grep java | grep "$APP_BASE_PATH/" | grep -v grep | awk '{print $2}')
    # else
    # appId=$(pgrep -f "\b$APP_NAME\b"   -U $UID)
    # fi
  fi
  [ -z "$appId" ] && return 2
  echo $appId
  return 0
}

# 修改获取pid的方式
exist() {
  cid=$(getPID)
  if [ "$?" = "0" -a ! -z "$cid" ]; then
    return 0
  else
    return 1
  fi
}

start() {
  if exist; then
    log_success "$APP_NAME is already running."
    exit 1
  else
    cd $APP_PATH
    if [ "$runOnBackground" = "true" ]; then
      nohup java $JAVA_OPTS -cp $CLASS_PATH -jar $DEFAULT_JAR $APP_NAME >$LOG_PATH/console.log 2>&1 &
      # record pid to $APP_PIDFILE
      RET=$?
      APPID=$!
      log_warning ${APP_PIDFILE}
      echo $APPID >${APP_PIDFILE}
      log_success "$APP_NAME is started. pidfile created : ${APP_ID} (pid  $APPID) ; operate status $RET"
    else
      java $JAVA_OPTS -cp $CLASS_PATH -jar $DEFAULT_JAR $APP_NAME

    fi
  fi
}

stop() {
  runningPID=$(getPID)
  if [ "$?" = "0" -a "$runningPID" ]; then
    log_warning "$APP_NAME pid: $runningPID"
    count=0
    kwait=15
    log_warning "$APP_NAME is stopping, please wait..."
    kill -15 $runningPID
    until [[ $(ps --pid $runningPID 2>/dev/null | grep -c $runningPID 2>/dev/null) -eq '0' ]] || [ $count -gt $kwait ]; do
      sleep 1
      let count=$count+1
    done

    if [ $count -gt $kwait ]; then
      kill -9 $runningPID
    fi
    clear
    [ -f "$APP_PIDFILE" ] && rm $APP_PIDFILE
    log_success "$APP_NAME is stopped."
  else
    log_failure "$APP_NAME has not been started."
  fi
}

invoke_kill() {
  if (exist); then
    SERVICE_PID="$(cat ${APP_PIDFILE})"
    log_warning "INFO: sending SIGKILL to pid '$SERVICE_PID'"
    kill -KILL $SERVICE_PID
    RET="$?"
    rm -f "${APP_PIDFILE}"
    return $RET
  fi
  log_failure "INFO: not running, nothing to do"
  return 0
}

#
getPort() {

  local SERVICE_PID=$(getPID)
  if [ -z "$SERVICE_PID" ]; then
    log_failure "程序进程不存在"
    return 1
  fi
  local EXEC_CMD=$(which lsof 2>/dev/null)
  if [ -f "$EXEC_CMD" ]; then
    local APP_PORTS=$($EXEC_CMD -nP -iTCP | grep -i " $SERVICE_PID" | awk '{print $9}' | sed 's/^.*://g')
  else
    EXEC_CMD=$(which netstat 2>/dev/null)
    [ -f "$EXEC_CMD" ] && local APP_PORTS=$($EXEC_CMD -ntlp | grep -i " $SERVICE_PID/" | awk '{print $4}' | sed 's/^.*://g')
  fi
  echo $APP_PORTS
}

# 获取所有的进程对应端口，然后艾格执行
isHealth() {
  if [ "$checkHealthy" == "true" ]; then

    APP_PORTS=$(getPort)
    for APP_PORT in $APP_PORTS; do
      isOK=$(curl -XHEAD -I "http://localhost:$APP_PORT" 2>/dev/null | grep 200 | wc -l)
      if [ "$isOK" -eq "1" ]; then
        return 0
      else
        log_failure "程序端口${APP_PORT}Healthy检查失败"
      fi
    done
    return 1
  else
    return 0
  fi
}

check() {
  if exist; then
    echo "$APP_NAME is alive."
    if isHealth; then
      log_info "$APP_NAME IS HEALTHY"
    else
      log_failure "$APP_NAME IS UNHEALTHY"
    fi
    exit 0
  fi
  log_failure "$APP_NAME is dead."
  exit -1
}

show_help() {
  cat <<EOF
Tasks provided by the single.sh script:
    kill           - terminate instance in a drastic way by sending SIGKILL
    restart        - stop running instance (if there is one), start new instance
    console        - start broker in foreground, useful for debugging purposes
    status         - check if service process is running
    isHealth       - check if service process is health
    start          - start new instance if there no one
    stop           - stop the instance if there is one
Configuration of this script:
    The configuration of this script is read from the following files:
    $SERVICE_CONFIGS
    This script searches for the files in the listed order and reads the first available file.
    Modify $BASE_PATH/bin/env BASE_PATH/env.sh $BASE_PATH/bin/conf $BASE_PATH/bin/conf.sh
    $BASE_PATH/bin/config  $BASE_PATH/bin/config.sh
EOF
  exit 1
}

# ------------------------------------------------------------------------
# MAIN

# show help
if [ -z "$1" ]; then
  show_help
fi

case "$1" in

start)
  start
  ;;
console)
  runOnBackground=false
  start
  ;;
stop)
  stop
  ;;
kill)
  invoke_kill
  ;;
restart)
  stop
  start
  ;;
isHealth)
  isHealth
  ;;
status)
  check
  ;;
getPort)
  getPort
  ;;
*)
  log_failure "available operations: [start|stop|restart|status|kill|console]"
  exit 1
  ;;
esac
