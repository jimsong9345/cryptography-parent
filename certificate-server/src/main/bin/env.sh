## !/bin/bash
BASE_PATH=$(
  cd "$(dirname "$0")"
  pwd
)
[ -f "$BASE_PATH/log.sh" ] && . $BASE_PATH/log.sh
[ -f "$BASE_PATH/common.sh" ] && . $BASE_PATH/common.sh
[ -f "$BASE_PATH/envConfig.sh" ] && . $BASE_PATH/envConfig.sh
## 自定义 应用名称
## !!!!!! 请大家一定要修改APP_NAME作为定位APP的唯一表示 ！！！！
appName=${appName:-xxx-server}
## 程序执行指定用户，指定的话会检查当前用户是否是指定用户，不是不能启动，未指定则不限制
user=${user}
## 自定义 java 运行参数
defaultJavaOpts=${defaultJavaOpts}
## 未自定义 java 运行参数时：是否自动配置，默认 false 手动指定 XMX_VAR 参数
runOnJvmXmxAuto=${runOnJvmXmxAuto:-false}

#region 最终的APP_NAME
invokeAppName() {
  APP_NAME="bjca-app-$appName"
}
invokeAppName
#endregion

#region 根据入参和环境变量配置部分JAVA 运行参数
invokeDefaultJavaOpts() {
  if [[ "$defaultJavaOpts" != "" ]]; then
    log_info "use custom java parameters"
  else
    log_info "use default java parameters"
    # 根据当前机器内存和JDK版本决定GC，如果不想使用自动的配置，请关闭 $runOnJvmXmxAuto 并手动设置 XMX_VAR
    if [ "$runOnJvmXmxAuto" = "true" ]; then
      # 操作系统内存
      TOTAL_MEM=$(awk '/MemTotal/{print $2}' /proc/meminfo)

      $TOTAL_MEM
      if [ ! -n "$TOTAL_MEM" ]; then
        TOTAL_MEM=$(sysctl -n hw.memsize)
        TOTAL_MEM_MB=$(expr $TOTAL_MEM / 1024 / 1024)
      else
        TOTAL_MEM_MB=$(expr $TOTAL_MEM / 1024)
      fi
      log_info 'INFO: TOTAL_MEM_MB' $TOTAL_MEM_MB 'MB'

      # JVM 内存
      if [ $TOTAL_MEM_MB -lt 8192 ]; then
        log_info "$TOTAL_MEM_MB lt 8g"
        XMX_V=$(expr $TOTAL_MEM_MB / 2)
      elif [ $TOTAL_MEM_MB -lt 10240 ]; then
        log_info "$TOTAL_MEM_MB lt 10g"
        XMX_V=$(expr $TOTAL_MEM_MB - 3096)
      else
        log_info "$TOTAL_MEM_MB gt 10g"
        XMX_V=$(expr $TOTAL_MEM_MB \* 3 / 4)
      fi
      log_info "XMX_V is $XMX_V MB"
      XMN_V=$(expr $XMX_V / 2)
    else
      # 不自动配置(默认情况)
      TOTAL_MEM_MB=1024
      #数值*3/4,m
      XMX_V=$(expr $TOTAL_MEM_MB \* 3 / 4)
      #数值*3/8,m
      XMN_V=$(expr $TOTAL_MEM_MB \* 3 / 8)
    fi

    #设置java运行参数
    DEFAULT_JAVA_OPTS=" -server -Xmx${XMX_V}m -Xms${XMX_V}m -Xss256k -XX:+DisableExplicitGC -XX:LargePageSizeInBytes=128m -XX:+UseFastAccessorMethods "

    # 根据 JDK 版本和 JVM 内存大小设置 GC
    JDK_VERSION=$(java -version 2>&1 | sed '1!d' | sed -e 's/"//g' | awk '{print $3}')
    if [[ $JDK_VERSION > "1.7" ]]; then
      log_info "current JDK_VERSION is $JDK_VERSION gt 1.7"
      DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m "
      if [[ $XMX_V -gt 4096 ]]; then
        log_info "xmx gt 4G UseG1GC"
        DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -XX:+UseG1GC "
      else
        log_info "UseConcMarkSweepGC"
        DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -Xmn${XMN_V}m -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70 "
      fi
    else
      log_info "current JDK_VERSION is 1.7 $JDK_VERSION"
      DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -XX:PermSize=256m "
      DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -Xmn${XMN_V}m -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70 "
    fi
    defaultJavaOpts=$DEFAULT_JAVA_OPTS
  fi
}
invokeDefaultJavaOpts
# endregion
