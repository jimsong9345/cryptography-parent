// package com.study.crypto.certificate.server.databind.jackson;
//
// import com.fasterxml.jackson.core.JsonGenerator;
// import com.fasterxml.jackson.core.JsonParser;
// import com.fasterxml.jackson.core.JsonProcessingException;
// import com.fasterxml.jackson.databind.*;
// import org.bouncycastle.asn1.ASN1ObjectIdentifier;
// import org.springframework.boot.jackson.JsonComponent;
//
// import java.io.IOException;
//
// /**
//  * 用于序列化、反序列 ASN1ObjectIdentifier 类型值
//  * @author Songjin
//  * @since 2022-10-20 17:53
//  */
// @JsonComponent
// public class OidJsonComponent {
//
//     public static class Serializer extends JsonSerializer<ASN1ObjectIdentifier> {
//         @Override
//         public void serialize(ASN1ObjectIdentifier value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
//             gen.writeString(value.getId());
//         }
//     }
//
//     public static class Deserializer extends JsonDeserializer<ASN1ObjectIdentifier> {
//         @Override
//         public ASN1ObjectIdentifier deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
//             JsonNode value = jsonParser.getCodec().readTree(jsonParser);
//             // 获取到当前的json key 名称
//             String currentName = jsonParser.currentName();
//             System.out.println(currentName);
//             // 获取到当前的json转换的对象
//             Object currentValue = jsonParser.getCurrentValue();
//             System.out.println(currentValue);
//             return null;
//         }
//     }
// }
