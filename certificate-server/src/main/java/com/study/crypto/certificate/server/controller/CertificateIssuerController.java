package com.study.crypto.certificate.server.controller;

import com.study.crypto.dto.RequestRandomDto;
import com.study.crypto.dto.ResponseRandomDto;
import com.study.crypto.dto.ca.*;
import com.study.crypto.general.spring.annotation.RequireSignature;
import com.study.crypto.general.spring.annotation.RequireVerify;
import com.study.crypto.certificate.server.service.CertificateIssuerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 证书签发控制器
 * @author Songjin
 * @since 2021-01-03 11:12
 */
@Slf4j
@RestController
@RequestMapping(value = "/issue")
public class CertificateIssuerController {
    
    @Autowired
    private CertificateIssuerService certificateIssuerService;

    /**
     * 获取随机数
     * @param request 请求数据
     * @return ResponseRandomDto
     */
    @PostMapping(value = "/applyServiceRandom")
    public ResponseRandomDto applyServiceRandom(@Valid @RequestBody RequestRandomDto request) {
        return certificateIssuerService.applyServiceRandom(request);
    }

    /**
     * 证书申请请求，单证，只有签名证书
     * @param request 请求数据
     * @return ResponseSealCertDto
     * @throws Exception 异常
     */
    @PostMapping(value = "/single/applySealCert")
    @RequireSignature
    public ResponseCheckResultSealCertDto applySingleCert(@Valid @RequestBody @RequireVerify RequestSealCertDistinctNameDto request) throws Exception {
        return certificateIssuerService.applySingleCert(request);
    }

    /**
     * 用 pkcs10 申请签名证书
     * @param request 请求数据
     * @return ResponseCheckResultSealCertDto
     * @throws Exception 异常
     */
    @PostMapping(value = "/single/applyCertByPkcs10")
    @RequireSignature
    public ResponsePkcs10Dto applySingleCertByPkcs10(@Valid @RequestBody @RequireVerify RequestPkcs10Dto request) throws Exception {
        return certificateIssuerService.applySingleCertByPkcs10(request);
    }
    
    /**
     * 证书申请请求
     * @param request 请求数据
     * @return ResponseSealCertDto
     * @throws Exception 异常
     */
    @PostMapping(value = "/double/applySealCert")
    @RequireSignature
    public ResponseSealCertDto applySealCert(@Valid @RequestBody @RequireVerify RequestSealCertDto request) throws Exception {
        return certificateIssuerService.applySealCert(request);
    }
    
    /**
     * 使用taskId查询下载证书
     * @param request 请求数据
     * @return ResponseSealCertDto
     * @throws Exception 异常
     */
    @PostMapping(value = "/double/checkResult")
    @RequireSignature
    public ResponseCheckResultSealCertDto checkResult(@Valid @RequestBody @RequireVerify RequestCheckResultDto request) throws Exception {
        return certificateIssuerService.checkResult(request);
    }

    /**
     * 使用 pkcs10 申请国密双证书
     * @param request 请求数据
     * @return ResponseCheckResultSealCertDto
     * @throws Exception 异常
     */
    @PostMapping(value = "/double/applyCertByPkcs10")
    @RequireSignature
    public ResponsePkcs10Dto applyCertByPkcs10(@Valid @RequestBody @RequireVerify RequestPkcs10Dto request) throws Exception {
        return certificateIssuerService.applyCertByPkcs10(request);
    }
    
}














