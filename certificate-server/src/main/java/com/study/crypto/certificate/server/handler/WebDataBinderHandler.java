package com.study.crypto.certificate.server.handler;

import com.study.crypto.certificate.server.validator.RequestRandomValidator;
import com.study.crypto.certificate.server.validator.RequestSealCertDistinctNameValidator;
import com.study.crypto.certificate.server.validator.RequestSealCertValidator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Songjin
 * @since 2021-04-21 14:09
 */
@RestControllerAdvice
public class WebDataBinderHandler {
    
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        Object target = webDataBinder.getTarget();
        if (target != null) {
            RequestRandomValidator validator1 = new RequestRandomValidator();
            if (validator1.supports(target.getClass())) {
                webDataBinder.addValidators(validator1);
            }

            RequestSealCertValidator validator2 = new RequestSealCertValidator();
            if (validator2.supports(target.getClass())) {
                webDataBinder.addValidators(validator2);
            }

            RequestSealCertDistinctNameValidator validator3 = new RequestSealCertDistinctNameValidator();
            if (validator3.supports(target.getClass())) {
                webDataBinder.addValidators(validator3);
            }

        }
    }
}
