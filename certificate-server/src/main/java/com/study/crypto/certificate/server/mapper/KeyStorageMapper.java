package com.study.crypto.certificate.server.mapper;

import com.study.crypto.general.spring.common.GeneralMapper;
import com.study.crypto.certificate.server.entity.KeyStorage;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Songjin
 * @since 2021/1/12 22:10
 */
@Primary
@Repository
public interface KeyStorageMapper extends GeneralMapper<KeyStorage> {
}