package com.study.crypto.certificate.server.validator;

import com.study.crypto.dto.ca.RequestSealCertDistinctNameDto;
import org.springframework.validation.Errors;

/**
 * @author Songjin
 * @since 2022-11-01 18:45
 */
public class RequestSealCertDistinctNameValidator extends RequestSealCertValidator{

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(RequestSealCertDistinctNameDto.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RequestSealCertDistinctNameDto target = (RequestSealCertDistinctNameDto) o;
        validateInternal(target, errors);
    }
}
