package com.study.crypto.certificate.server.exception;

/**
 * 签名验签相关异常
 * @author Songjin
 * @since 2022-11-05 19:57
 */
public class SignatureException extends RuntimeException {

    private Object entity;

    public SignatureException(String message) {
        super(message);
    }

    public SignatureException(String message, Object entity) {
        super(message);
        this.entity = entity;
    }

    public SignatureException(String message, Throwable cause) {
        super(message, cause);
    }

    public SignatureException(String message, Object entity, Throwable cause) {
        super(message, cause);
        this.entity = entity;
    }

    public Object getEntity() {
        return entity;
    }
}