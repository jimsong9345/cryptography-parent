package com.study.crypto.certificate.server.entity;

import com.study.crypto.general.spring.common.GeneralEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 表名：bo_businesslog
 * @author Songjin
 * @since 2021-01-05 12:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Table(name = "bo_businesslog")
public class BusinessLog extends GeneralEntity {
    
    /** 登录IP地址 */
    @Column(name = "client_ip")
    private String clientIp;

    /** 操作对象 */
    @Column(name = "opt_obj")
    private String optObj;

    /** 操作结果 */
    @Column(name = "opt_result")
    private String optResult;

    /** 操作类型 */
    @Column(name = "opt_type")
    private String optType;

    /** 机构代码 */
    @Column(name = "org_code")
    private String orgCode;

    /** 所属机构ID */
    @Column(name = "org_id")
    private String orgId;

    /** 机构名称 */
    @Column(name = "org_name")
    private String orgName;

    /** 印章编号 */
    @Column(name = "seal_num")
    private String sealNum;

    /** 规则编号 */
    @Column(name = "rule_num")
    private String ruleNum;

    /** 印章名称 */
    @Column(name = "seal_name")
    private String sealName;

    /** 规则名称 */
    @Column(name = "rule_name")
    private String ruleName;

    /** 印章类型: 0 国办印章; 1 本地国办印章 */
    @Column(name = "seal_type")
    private String sealType;

    /** 文件名称 */
    @Column(name = "file_name")
    private String fileName;

    /** 业务系统名称 */
    @Column(name = "app_name")
    private String appName;

    /** 签章人姓名 */
    @Column(name = "signer")
    private String signer;

    /** 签章人身份唯一标识 */
    @Column(name = "signer_id")
    private String signerId;

    /** 签章人所属部门 */
    @Column(name = "signer_dept")
    private String signerDept;

    /** 详细信息 */
    @Column(name = "detail")
    private String detail;

    /** 机构标识 */
    @Column(name = "org_flag")
    private String orgFlag;
    
}