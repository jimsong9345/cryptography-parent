package com.study.crypto.certificate.server.entity;

import com.study.crypto.general.spring.common.GeneralEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @author Songjin
 * @since 2021/1/12 22:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Table(name = "bo_key_storage")
public class KeyStorage extends GeneralEntity {
    /** 别名 */
    @Column(name = "alias")
    private String alias;

    /** ip地址 */
    @Column(name = "ip")
    private String ip;

    /** 端口号 */
    @Column(name = "port")
    private Integer port;

    /** 算法名称 */
    @Column(name = "key_alg")
    private String keyAlg;

    /** 密钥字节大小 */
    @Column(name = "key_size")
    private Short keySize;

    /** 密钥类型: 1 签名密钥，2 加密密钥，3 对称密钥 */
    @Column(name = "key_type")
    private String keyType;
    /** 密钥类型: 1 签名密钥 */
    public static final String KEY_TYPE_SIGNATURE = "1";
    /** 密钥类型: 2 加密密钥 */
    public static final String KEY_TYPE_ENCRYPT = "2";
    /** 密钥类型: 3 对称密钥 */
    public static final String KEY_TYPE_SYMMETRIC = "3";

    /** 密钥名称 */
    @Column(name = "key_name")
    private String keyName;

    /** 私钥密码 */
    @Column(name = "pwd")
    private String pwd;

    /** 状态 */
    @Column(name = "status")
    private Byte status;

    /** 用户名 */
    @Column(name = "username")
    private String username;

    /** 密钥数据 */
    @Column(name = "key_bytes")
    private byte[] keyBytes;
    
}