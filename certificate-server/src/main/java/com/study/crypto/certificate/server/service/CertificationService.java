package com.study.crypto.certificate.server.service;

import com.study.crypto.certificate.server.config.CustomProperties;
import com.study.crypto.certificate.server.entity.Certification;
import com.study.crypto.certificate.server.mapper.CertificationMapper;
import com.study.crypto.general.spring.common.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author Songjin
 * @since 2021-02-21 23:02
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class CertificationService extends GeneralService<Certification> {

    @Autowired
    private CustomProperties properties;
    @Autowired
    private CertificationMapper certMapper;

}
