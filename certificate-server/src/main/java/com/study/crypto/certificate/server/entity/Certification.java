package com.study.crypto.certificate.server.entity;

import com.study.crypto.general.spring.common.GeneralEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 表名：bo_certificate
 * @author Songjin
 * @since 2021-02-21 23:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Table(name = "bo_certificate")
public class Certification extends GeneralEntity {
    
    /** 证书名称 */
    @Column(name = "cert_name")
    private String certName;

    /** 根证书id */
    @Column(name = "cert_chain_id")
    private String certChainId;

    /** 证书主题 */
    @Column(name = "cert_dn")
    private String certDn;

    /** 证书生效时间 */
    @Column(name = "begin_time")
    private LocalDateTime beginTime;

    /** 证书到期时间 */
    @Column(name = "end_time")
    private LocalDateTime endTime;

    /** 证书类型: RSA，SM2 */
    @Column(name = "cert_type")
    private String certType = CERT_TYPE_SM2;
    /** 证书类型: SM2 */
    public static final String CERT_TYPE_SM2 = "SM2";
    /** 证书类型: ECC */
    public static final String CERT_TYPE_ECC = "ECC";
    /** 证书类型: RSA */
    public static final String CERT_TYPE_RSA = "RSA";

    /** 证书序列号 */
    @Column(name = "serial_number")
    private String serialNumber;

    /** 关联的密钥id */
    @Column(name = "key_id")
    private String keyId;
    
    /** 证书申请id */
    @Column(name = "apply_id")
    private String applyId;

    /** 证书用途，比如签名证书、加密证书等 */
    @Column(name = "key_usage")
    private Integer keyUsage;

    /** 证书文件 */
    @Column(name = "certificate")
    private byte[] certificate;

    /** 密钥对保护结构 */
    @Column(name = "enveloped_key")
    private byte[] envelopedKey;
}