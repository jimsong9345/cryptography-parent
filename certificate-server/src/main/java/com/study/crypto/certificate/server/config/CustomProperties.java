package com.study.crypto.certificate.server.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Songjin
 * @since 2021-02-27 21:27
 */
@Component
@ConfigurationProperties(prefix = "custom")
@Getter @Setter
public class CustomProperties {
    
    private String defaultSerialNumberSm2;

    private String defaultSerialNumberRsa;
}
