package com.study.crypto.certificate.server.entity;

import com.study.crypto.general.spring.common.GeneralEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 表名：bo_seal
 * @author Songjin
 * @since 2021-01-03 21:22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Table(name = "bo_seal")
public class Seal extends GeneralEntity {
    
    /** 印章高度 */
    @Column(name = "seal_height")
    private Float sealHeight;

    /** 印章宽度 */
    @Column(name = "seal_width")
    private Float sealWidth;

    /** 印章名称 */
    @Column(name = "seal_name")
    private String sealName;

    /** 印章有效起始时间 */
    @Column(name = "not_before")
    private String notBefore;

    /** 印章有效终止时间 */
    @Column(name = "not_after")
    private String notAfter;

    /** base64编码的应用维护对称密钥密文 */
    @Column(name = "app_sym_key_enc")
    private String appSymKeyEnc;

    /** base64编码的数据加密对称密钥密文 */
    @Column(name = "data_sym_key_enc")
    private String dataSymKeyEnc;

    /** 印章使用主体单位id，关联bo_unit */
    @Column(name = "unit_id")
    private String unitId;

    /** 签名密钥id关联bo_signkey */
    @Column(name = "sign_key_id")
    private String signKeyId;

    /** 签名策略id关联bo_signpolicy */
    @Column(name = "sign_policy_id")
    private String signPolicyId;

    /** 印章结构hash */
    @Column(name = "seal_hash")
    private String sealHash;

    /** 印章状态：0 有效，1 废弃，2 注销，3 过期，4 停用。默认为禁用状态1 */
    @Column(name = "status")
    private String status;

    /** 印章状态变更时间 */
    @Column(name = "state_time")
    private LocalDateTime stateTime;

    /** 印章图片 */
    @Column(name = "seal_image")
    private String sealImage;

    /** 签名证书 */
    @Column(name = "sign_cert")
    private String signCert;

    /** 加密证书 */
    @Column(name = "enc_cert")
    private String encCert;

    /** 加密密钥对保护结构 */
    @Column(name = "enc_cert_key")
    private String encCertKey;

    /** 公安下发加密文件 */
    @Column(name = "enc_file")
    private String encFile;

    /** 备案签名证书P10请求 */
    @Column(name = "pkcs10")
    private String pkcs10;

    /** 印章结构文件 */
    @Column(name = "seal_file")
    private String sealFile;
    
}