package com.study.crypto.certificate.server.mapper;

import com.study.crypto.general.spring.common.GeneralMapper;
import com.study.crypto.certificate.server.entity.Certification;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Songjin
 * @since 2021-02-21 23:02
 */
@Primary
@Repository
public interface CertificationMapper extends GeneralMapper<Certification> {
    
    /**
     * 查询最大证书序列号
     * @param certType 证书类型:SM2、RSA
     * @return 证书序列号
     */
    @Select("select max(serial_number) from bo_certificate where cert_type = #{certType}")
    String selectMaxSerialNumber(@Param("certType") String certType);
}