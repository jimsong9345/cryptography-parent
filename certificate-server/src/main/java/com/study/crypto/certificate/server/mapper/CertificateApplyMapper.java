package com.study.crypto.certificate.server.mapper;

import com.study.crypto.certificate.server.entity.CertificateApply;
import com.study.crypto.general.spring.common.GeneralMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Songjin
 * @since 2021-01-10 14:12
 */
@Primary
@Repository
public interface CertificateApplyMapper extends GeneralMapper<CertificateApply> {
}