package com.study.crypto.certificate.server.mapper;

import com.study.crypto.general.spring.common.GeneralMapper;
import com.study.crypto.certificate.server.entity.Seal;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Songjin
 * @since 2021-01-05 14:30
 */
@Primary
@Repository
public interface SealMapper extends GeneralMapper<Seal> {
}