package com.study.crypto.certificate.server.service;

import com.study.crypto.certificate.server.entity.BusinessLog;
import com.study.crypto.general.spring.common.GeneralService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Songjin
 * @since 2021-01-05 11:59
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class BusinessLogService extends GeneralService<BusinessLog> {
}
