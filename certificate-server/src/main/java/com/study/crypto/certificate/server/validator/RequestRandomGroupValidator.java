package com.study.crypto.certificate.server.validator;

import com.study.crypto.dto.RequestRandomDto;
import com.study.crypto.dto.ca.CaConstants;
import com.study.crypto.dto.group.RandomForCA;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

/**
 * 继承 SmartValidator 可以实现自定义分组校验
 * <p>
 *     分组校验有个问题，必须给所有字段注解加上 groups 属性，将被标注 groups 的所有字段视为一个组，如果有字段未标注，那么不会参与校验
 * </p>
 * <p>
 *     另外，还需要将 controller 中的 @Valid 注解换成 @Validated(RandomForCA.class) 方式，两种校验方式不能共存
 * </p>
 * @author Songjin
 * @since 2022-11-01 17:13
 */
@Slf4j
public class RequestRandomGroupValidator implements SmartValidator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(RequestRandomDto.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        log.debug("请忽略，不会被调用的空方法");
    }

    @Override
    public void validate(Object o, Errors errors, Object... validationHints) {
        RequestRandomDto target = (RequestRandomDto) o;
        String taskCode = target.getTaskCode();
        if (StringUtils.isBlank(taskCode)) {
            return ;
        }

        if (validationHints.length > 0) {
            Class<?> group = (Class<?>) validationHints[0];
            boolean caGroup = group.isAssignableFrom(RandomForCA.class);
            boolean notEquals = !CaConstants.TASK_CODE_APPLY_RANDOM_CA.equals(taskCode);
            if (caGroup && notEquals) {
                errors.rejectValue("taskCode", null, "taskCode参数为固定值applyServiceRandom");
            }
        }
    }

}
