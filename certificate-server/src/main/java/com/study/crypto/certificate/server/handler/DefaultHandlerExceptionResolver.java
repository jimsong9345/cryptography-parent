package com.study.crypto.certificate.server.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.study.crypto.certificate.server.exception.SignatureException;
import com.study.crypto.general.spring.handler.GeneralDefaultHandlerExceptionResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

/**
 * 统一异常处理
 *
 * @author Songjin
 * @since 2020/9/26 23:53
 */
@RestControllerAdvice
@Slf4j
public class DefaultHandlerExceptionResolver extends GeneralDefaultHandlerExceptionResolver {

    /**
     * 签名验签异常处理
     * @param e 签名验签异常
     * @return Map映射
     */
    @ExceptionHandler(SignatureException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Map<String, Object> resovleSignatureException(SignatureException e) {
        log.error("异常堆栈", e);
        return (JSONObject) JSON.toJSON(e.getEntity());
    }

}
