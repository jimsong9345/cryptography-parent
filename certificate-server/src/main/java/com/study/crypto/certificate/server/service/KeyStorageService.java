package com.study.crypto.certificate.server.service;

import com.study.crypto.general.spring.common.GeneralService;
import com.study.crypto.certificate.server.entity.KeyStorage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Songjin
 * @since 2021-01-13 11:02
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class KeyStorageService extends GeneralService<KeyStorage> {
}
