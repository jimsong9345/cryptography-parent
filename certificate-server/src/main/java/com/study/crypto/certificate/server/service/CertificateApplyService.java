package com.study.crypto.certificate.server.service;

import com.study.crypto.general.spring.common.GeneralService;
import com.study.crypto.certificate.server.entity.CertificateApply;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Songjin
 * @since 2021-01-10 14:12
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class CertificateApplyService extends GeneralService<CertificateApply> {
}
