package com.study.crypto.certificate.server;

import com.study.crypto.general.spring.config.MyBatisPluginAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Songjin
 * @since 2021-01-03 0:33
 */
@MapperScan(basePackages = "com.study.crypto.certificate.server.mapper")
@EnableTransactionManagement
@SpringBootApplication
@Import(MyBatisPluginAutoConfiguration.class)
public class CertificateServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CertificateServerApplication.class, args);
    }

}
