package com.study.crypto.certificate.server.validator;

import com.study.crypto.dto.RequestRandomDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;

/**
 * @author Songjin
 * @since 2022-11-01 16:14
 */
@Slf4j
public class RequestRandomValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(RequestRandomDto.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RequestRandomDto target = (RequestRandomDto) o;
        String taskCode = target.getTaskCode();
        if (StringUtils.isBlank(taskCode)) {
            return ;
        }
        List<String> list = Arrays.asList("applyServiceRandom", "applyRandom");
        if (!list.contains(taskCode)) {
            errors.rejectValue("taskCode", null, "taskCode参数为固定值");
        }
    }


}
