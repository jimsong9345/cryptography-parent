package com.study.crypto.certificate.server.validator;

import com.study.crypto.dto.ca.CaConstants;
import com.study.crypto.dto.ca.RequestSealCertDto;
import com.study.crypto.dto.ca.base.RequestCaDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author Songjin
 * @since 2021-05-30 16:04
 */
public class RequestSealCertValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(RequestSealCertDto.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RequestSealCertDto target = (RequestSealCertDto) o;
        validateInternal(target, errors);
    }

    protected void validateInternal(RequestCaDto target, Errors errors) {
        String taskCode = target.getTaskCode();
        if (StringUtils.isBlank(taskCode)) {
            errors.rejectValue("taskCode", null, "taskCode参数为空");
            return;
        }
        if (!CaConstants.TASK_CODE_APPLY_SEAL_CERT.equals(taskCode)) {
            errors.rejectValue("taskCode", null, "taskCode参数为固定值applySealCert");
        }
    }
}
