package com.study.crypto.certificate.server.entity;

import com.study.crypto.general.spring.common.GeneralEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 表名：bo_system_attribute
 * @author Songjin
 * @since 2021-01-03 22:48
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Table(name = "bo_system_attribute")
public class SysAttr extends GeneralEntity {
    
    /** 参数名称 */
    @Column(name = "attr_id")
    private String attrId;

    /** 说明 */
    @Column(name = "attr_remake")
    private String attrRemake;

    /** 是否为系统参数：Y 是，N 否 */
    @Column(name = "sys_config")
    private String sysConfig;

    /** 参数值 */
    @Column(name = "attr_value")
    private String attrValue;
    
}