package com.study.crypto.gb.server.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Songjin
 * @since 2021-06-20 21:33
 */
@Getter @Setter
@Builder
public class GADataBean {
    
    /** base64编码应用维护对称密钥密文。gmt 0018标准 */
    private String appSymKeyEnc;
    
    /** base64编码数据加密对称密钥密文。gmt 0018标准 */
    private String dataSymKeyEnc;
    
    /** 设备编号 */
    private String deviceCode;
    
    /** 加密设备编号 */
    private String encDeviceCode;
    
    /** 公安下发的加密文件，base64编码，使用对称加密算法模式为sm4_ecb，填充方式为 pkcs7，加密文件的格式内容见B.3，最大不大于40K */
    private String encFile;
}
