package com.study.crypto.gb.server.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 国办鉴权参数
 * @author Songjin
 * @since 2021-05-31 19:13
 */
@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestAuthentication {
    
    /** appKey */
    @JSONField(name = "App_key")
    @JsonProperty("App_key")
    private String appKey;
    
    /** 摘要值 */
    @JSONField(name = "PasswdDigest")
    @JsonProperty("PasswdDigest")
    private String passwdDigest;
    
    /** 随机数 */
    @JSONField(name = "Nonce")
    @JsonProperty("Nonce")
    private String nonce;
    
    /** 租户名 */
    @JSONField(name = "Username")
    @JsonProperty("Username")
    private String username;
    
    /** 创建时间，格式: yyyy-MM-dd HH:mm:ss */
    @JSONField(name = "Created")
    @JsonProperty("Created")
    private String created;
    
    /** 实际的请求参数 */
    private String parameter;
    
    public void setApp_key(String appKey) {
        this.appKey = appKey;
    }
}
