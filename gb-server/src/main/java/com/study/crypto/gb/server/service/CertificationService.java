package com.study.crypto.gb.server.service;

import com.study.crypto.general.spring.common.GeneralService;
import com.study.crypto.gb.server.entity.Certification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Songjin
 * @since 2021-06-13 10:00
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class CertificationService extends GeneralService<Certification> {
}
