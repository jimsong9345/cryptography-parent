package com.study.crypto.gb.server.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 国办接口异常: 随机数异常、验签异常等
 * @author Songjin
 * @since 2022-04-12 14:22
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GbRuntimeException extends RuntimeException {

    private final int code;
    
    private final String taskCode;
    
    public GbRuntimeException(int code, String taskCode, String message) {
        super(message);
        this.code = code;
        this.taskCode = taskCode;
    }
    
    public GbRuntimeException(int code, String taskCode, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.taskCode = taskCode;
    }
    
}
