package com.study.crypto.gb.server.mapper;

import com.study.crypto.general.spring.common.GeneralMapper;
import com.study.crypto.gb.server.entity.SealApply;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Songjin
 * @since 2021-06-13 11:27
 */
@Primary
@Repository
public interface SealApplyMapper extends GeneralMapper<SealApply> {
}