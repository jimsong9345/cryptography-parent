package com.study.crypto.gb.server.advice;

import com.study.crypto.general.spring.advice.GeneralRequestResponseBodyLogAdvice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * 记录请求 @RequestBody、@ResponseBody 响应参数日志通知
 * @author Songjin
 * @since 2021-06-05 22:14
 */
@Slf4j
@Order(10)
@ControllerAdvice
public class RequestResponseBodyLogAdvice extends GeneralRequestResponseBodyLogAdvice {

}
