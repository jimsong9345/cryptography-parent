package com.study.crypto.gb.server.entity;

import com.study.crypto.general.spring.common.GeneralEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @author Songjin
 * @since 2021-06-13 11:25
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@Table(name = "bo_seal_data")
public class SealData extends GeneralEntity {

    /** 印章名称 */
    @Column(name = "yzmc")
    private String yzmc;

    /** 印章编码，省市区(6个字节)+印章赋码(8个字节) */
    @Column(name = "yzbm")
    private String yzbm;
    
    /** 申请状态: 0 备案完成，1 申请提交，2 印章撤销 */
    @Column(name = "status")
    private Integer status;
    
    public static final int STATUS_FINISHED = 0;
    public static final int STATUS_SUBMITTED = 1;
    public static final int STATUS_REVOKED = 2;

    /**
     * 国办C0122备案接口规定的印章类型
     * <ul>
     *  <li>电子法定名称章(代码:01)</li>
     *  <li>电子财务专用章(代码:02)</li>
     *  <li>电子发票专用章(代码:03)</li>
     *  <li>电子合同专用章(代码:04)</li>
     *  <li>电子名章(代码:05)</li>
     *  <li>业务专用章(代码:06)六类</li>
     * </ul>
     * 其中01、02、03、04、06表示公章1，05表示个人印章2
     */
    @Column(name = "yzlxdm")
    private String yzlxdm;

    /** 设备编号 */
    @Column(name = "device_code")
    private String deviceCode;

    /** 加密设备编号 */
    @Column(name = "enc_device_code")
    private String encDeviceCode;

    /** 经办人姓名 */
    @Column(name = "jbr_xm")
    private String jbrXm;

    /** 参见GA∕T 2000.156-2016《公安信息代码 第156部分：常用证件代码》，建议经办人证件类型采用居民身份证，代码为111。为3个字节 */
    @Column(name = "jbr_zjlx")
    private String jbrZjlx;

    /** 经办人证件号码。身份证号码，最大20个字节 */
    @Column(name = "jbr_zjhm")
    private String jbrZjhm;

    /** 印章制作日期。格式如下：2018-11-07 10:09:07 */
    @Column(name = "zzrq")
    private String zzrq;

    /** 印模类型 */
    @Column(name = "ymlx")
    private String ymlx;

    /** 印章使用单位_单位名称，最大150字节 */
    @Column(name = "yzsydw_dwmc")
    private String yzsydwDwmc;

    /** 印章使用单位_统一社会信用代码，18个字节 */
    @Column(name = "yzsydw_tyshxydm")
    private String yzsydwTyshxydm;

    /** 印章使用单位_单位少数民族文字名称。没有为空字符串，最大200个字节 */
    @Column(name = "yzsydw_dwssmzwzmc")
    private String yzsydwDwssmzwzmc;

    /** 印章使用单位_单位英文名称。没有为空字符串，最大200个字节 */
    @Column(name = "yzsydw_dwywmc")
    private String yzsydwDwywmc;

    /** 印章制作单位_单位名称，最大150个字节 */
    @Column(name = "yzzzdw_dwmc")
    private String yzzzdwDwmc;

    /** 印章制作单位编码，印章制作单位的统一社会信用代码，为18字节 */
    @Column(name = "yzzzdwbm")
    private String yzzzdwbm;

    /** 印章制作单位_单位少数民族文字名称。没有为空字符串，最大200个字节 */
    @Column(name = "yzzzdw_dwssmzwzmc")
    private String yzzzdwDwssmzwzmc;

    /** 印章制作单位_单位英文名称。没有为空字符串，最大200个字节 */
    @Column(name = "yzzzdw_dwywmc")
    private String yzzzdwDwywmc;

    /** 行政区划代码 */
    @Column(name = "district_code")
    private String districtCode;

    /** base64编码的印模数据，最大为30K */
    @Column(name = "ymsj")
    private String ymsj;
}