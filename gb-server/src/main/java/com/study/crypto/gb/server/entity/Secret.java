package com.study.crypto.gb.server.entity;

import com.study.crypto.general.spring.common.GeneralEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 表名：bo_secret
 * @author Songjin
 * @since 2021-06-05 8:50
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bo_secret")
public class Secret extends GeneralEntity {

    /** API访问的资源后缀 */
    @Column(name = "source")
    private String source;

    /** URL访问地址 */
    @Column(name = "url")
    private String url;

    /** appkey */
    @Column(name = "appkey")
    private String appkey;

    /** 每天凌晨刷新获得的secret */
    @Column(name = "secret")
    private String secret;

    /** 网关租户名称 */
    @Column(name = "username")
    private String username;

    /** 网关租户密码 */
    @Column(name = "password")
    private String password;

    /** 描述信息 */
    @Column(name = "description")
    private String description;
}