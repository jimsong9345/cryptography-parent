package com.study.crypto.gb.server.controller;

import com.study.crypto.dto.RequestRandomDto;
import com.study.crypto.dto.ResponseRandomDto;
import com.study.crypto.dto.gb.*;
import com.study.crypto.dto.group.RandomForGb;
import com.study.crypto.general.spring.annotation.RequireSignature;
import com.study.crypto.general.spring.annotation.RequireVerify;
import com.study.crypto.gb.server.dto.ReturnResult;
import com.study.crypto.gb.server.service.ProductionService;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * 制作系统接口
 * @author Songjin
 * @since 2021-05-31 16:59
 */
@Slf4j
@RestController
@RequestMapping(value = "/gb")
public class GbProductionController {
    
    @Autowired
    private ProductionService productionService;
    
    /**
     * 确认回执
     * @param request 请求
     * @return 响应
     */
    @PostMapping(value = "/confirmReceipt")
    @RequireSignature
    public ReturnResult<ResponseReceiptDto> confirmReceipt(@Valid @RequestBody @RequireVerify RequestReceiptDto request) {
        return productionService.confirmReceipt(request);
    }
    
    /**
     * 查询下载公安下发数据
     * @param request 请求
     * @return 响应
     */
    @PostMapping(value = "/checkResult")
    @RequireSignature
    public ReturnResult<ResponsePoliceDto> checkResult(@Valid @RequestBody @RequireVerify RequestPoliceDto request) {
        return productionService.checkResult(request);
    }

    /**
     * 上传备案信息
     * @param request 请求
     * @return 响应
     * @throws GeneralSecurityException 异常
     * @throws IOException 异常
     * @throws InvalidCipherTextException 异常
     */
    @PostMapping(value = "/uploadRecordInfos")
    @RequireSignature
    public ReturnResult<ResponseSealRecordDto> uploadRecordInfos(@Valid @RequestBody @RequireVerify RequestSealRecordDto request) throws GeneralSecurityException, IOException, InvalidCipherTextException {
        return productionService.uploadRecordInfos(request);
    }
    
    /**
     * 申请唯一赋码
     * @param request 请求
     * @return 响应
     */
    @PostMapping(value = "/applySealCode")
    @RequireSignature
    public ReturnResult<ResponseUniqueCodeDto> applySealCode(@Valid @RequestBody @RequireVerify RequestUniqueCodeDto request) {
        return productionService.applySealCode(request);
    }

    /**
     * 申请随机数
     * @param request 请求
     * @return 响应
     */
    @PostMapping(value = "/applyRandom")
    public ReturnResult<ResponseRandomDto> applyRandom(@Validated(RandomForGb.class) @RequestBody RequestRandomDto request) {
        return productionService.applyRandom(request);
    }


}
