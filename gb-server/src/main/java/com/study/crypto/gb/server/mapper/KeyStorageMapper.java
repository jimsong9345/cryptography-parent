package com.study.crypto.gb.server.mapper;

import com.study.crypto.general.spring.common.GeneralMapper;
import com.study.crypto.gb.server.entity.KeyStorage;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Songjin
 * @since 2021/1/12 22:10
 */
@Primary
@Repository
public interface KeyStorageMapper extends GeneralMapper<KeyStorage> {
}