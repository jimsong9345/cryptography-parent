package com.study.crypto.gb.server.exception;

/**
 * @author Songjin
 * @since 2021-06-11 19:51
 */
public class SignatureException extends RuntimeException {
    
    public SignatureException(String message) {
        super(message);
    }
    
    public SignatureException(String message, Throwable cause) {
        super(message, cause);
    }
}
