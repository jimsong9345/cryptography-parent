package com.study.crypto.gb.server.config;

import com.study.crypto.gb.server.advice.AuthenticateHandlerMethodArgumentResolver;
import com.study.crypto.general.spring.interceptor.TraceIdInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author Songjin
 * @since 2021-06-27 0:07
 */
@Configuration
public class WevMvcCustomConfigurer implements WebMvcConfigurer {
    
    @Autowired
    private AuthenticateHandlerMethodArgumentResolver authenticateArgumentResolver;
    
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(authenticateArgumentResolver);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new TraceIdInterceptor());
    }
}
