package com.study.crypto.gb.server.validator;

import com.study.crypto.dto.gb.GbConstants;
import com.study.crypto.dto.gb.RequestSealRecordDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author Songjin
 * @since 2021-06-19 18:22
 */
@Slf4j
public class RequestSealRecordValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(RequestSealRecordDto.class);
    }
    
    @Override
    public void validate(Object o, Errors errors) {
        log.info("校验上传备案信息请求: RequestSealRecordDto");
        RequestSealRecordDto target = (RequestSealRecordDto) o;
        String taskCode = target.getTaskCode();
        if (StringUtils.isBlank(taskCode)) {
            return ;
        }
        if (!GbConstants.UPLOAD_RECORD_INFOS.equals(taskCode)) {
            errors.rejectValue("taskCode", null, "taskCode参数必须为'uploadRecordInfos'");
        }
        String taskTypeCode = target.getTaskTypeCode();
        if (StringUtils.isBlank(taskTypeCode)) {
            return ;
        }
        if (!GbConstants.TASK_TYPE_CODE_UPLOAD_RECORD_INFOS.equals(taskTypeCode)) {
            errors.rejectValue("taskTypeCode", null, "taskTypeCode参数必须为'0'");
        }
    }
}
