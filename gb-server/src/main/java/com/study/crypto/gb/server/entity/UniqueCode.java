package com.study.crypto.gb.server.entity;

import com.study.crypto.general.spring.common.GeneralEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 表名：bo_unique_code
 * @author Songjin
 * @since 2021-06-06 23:29
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bo_unique_code")
public class UniqueCode extends GeneralEntity {

    /** 行政区划代码前3位，长度为 3 字节 */
    @Column(name = "area_number")
    private String areaNumber;

    /** 唯一赋码 */
    @Column(name = "unique_code")
    private String uniqueCode;

    /** 失效时间 */
    @Column(name = "deadline")
    private LocalDateTime deadline;
    
    public UniqueCode(String areaNumber, String uniqueCode) {
        this.areaNumber = areaNumber;
        this.uniqueCode = uniqueCode;
    }
}