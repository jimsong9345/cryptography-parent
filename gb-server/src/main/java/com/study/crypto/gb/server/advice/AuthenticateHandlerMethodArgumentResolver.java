package com.study.crypto.gb.server.advice;

import com.alibaba.fastjson.JSON;
import com.study.crypto.gb.server.annotation.AuthenticateParameter;
import com.study.crypto.gb.server.dto.RequestAuthentication;
import com.study.crypto.gb.server.entity.Secret;
import com.study.crypto.gb.server.exception.AuthenticationException;
import com.study.crypto.gb.server.service.SecretService;
import com.study.crypto.utils.EssPdfUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor;

/**
 * @author Songjin
 * @since 2021-06-05 7:40
 */
@Slf4j
@Component
public class AuthenticateHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
    
    private final ServletModelAttributeMethodProcessor methodProcessor;
    @Autowired
    private SecretService secretService;
    
    public AuthenticateHandlerMethodArgumentResolver() {
        methodProcessor = new ServletModelAttributeMethodProcessor(false);
    }
    
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(AuthenticateParameter.class);
    }
    
    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        log.info("AuthenticateHandlerMethodArgumentResolver.resolveArgument==========>begin");
        Object argument = methodProcessor.resolveArgument(parameter, mavContainer, webRequest, binderFactory);
        if (argument instanceof RequestAuthentication) {
            if (log.isInfoEnabled()) {
                log.info("请求参数: {}", JSON.toJSONString(argument));
            }
            // 进行 SM3 参数鉴权
            RequestAuthentication request = (RequestAuthentication) argument;
            String appKey = request.getAppKey();
            String nonce = request.getNonce();
            String created = request.getCreated();
            String username = request.getUsername();
            String passwdDigest = request.getPasswdDigest();
            boolean blank1 = StringUtils.isBlank(appKey);
            boolean blank2 = StringUtils.isBlank(nonce);
            boolean blank3 = StringUtils.isBlank(created);
            boolean blank4 = StringUtils.isBlank(username);
            boolean blank5 = StringUtils.isBlank(passwdDigest);
            if (blank1 || blank2 || blank3 || blank4 || blank5) {
                throw new AuthenticationException("鉴权失败，鉴权参数不全", "Not Found");
            }
            Secret secret = secretService.selectOne(Secret.builder().appkey(appKey).build());
            if (secret == null) {
                throw new AuthenticationException("鉴权失败，鉴权参数不全", "Not Found");
            }
            String plainText = nonce.concat("_").concat(created).concat("_").concat(secret.getSecret());
            String digestText = EssPdfUtil.digestHex(plainText);
            if (!StringUtils.equals(passwdDigest, digestText)) {
                throw new AuthenticationException("鉴权失败，SecretKey不正确", secret.getSource());
            }
        }
        log.info("AuthenticateHandlerMethodArgumentResolver.resolveArgument==========>end");
        return argument;
    }
    
}