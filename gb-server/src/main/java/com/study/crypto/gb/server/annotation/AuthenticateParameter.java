package com.study.crypto.gb.server.annotation;

import java.lang.annotation.*;

/**
 * 表示参数需要进行认证
 * @author Songjin
 * @since 2021-06-05 7:33
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthenticateParameter {
    
    boolean required() default true;
}
