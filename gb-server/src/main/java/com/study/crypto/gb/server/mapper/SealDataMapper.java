package com.study.crypto.gb.server.mapper;

import com.study.crypto.general.spring.common.GeneralMapper;
import com.study.crypto.gb.server.entity.SealData;
import org.apache.ibatis.annotations.Select;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Songjin
 * @since 2021-06-13 11:28
 */
@Primary
@Repository
public interface SealDataMapper extends GeneralMapper<SealData> {
    
    /**
     * 当前最大的设备编号
     * @return 设备编号
     */
    @Select("select max(device_code) from bo_seal_data")
    String selectMaxDeviceCode();
    
}