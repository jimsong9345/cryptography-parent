package com.study.crypto.gb.server.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author Songjin
 * @since 2021-05-30 10:46
 */
@Data
@AllArgsConstructor
@Builder
public class ReturnResult<T> {
    
    private static final long   serialVersionUID = 6948768594132295784L;
    /** 状态码 */
    private int code;
    /** 提示信息 */
    private String message;
    /** 请求流水号 */
    private String transId;
    /** 数据 */
    private T data;
    /** 错误信息 */
    private String moreInfo;
    
    public ReturnResult() {
    }
    
    public ReturnResult(int status) {
        this(status, null);
    }
    
    public ReturnResult(int status, String message) {
        this(status, message, null);
    }
    
    public ReturnResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
    
}
