package com.study.crypto.gb.server.dto;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Tolerate;

/**
 * 国办鉴权参数
 * @author Songjin
 * @since 2021-05-31 19:13
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseAuthentication {
    
    /** 状态码: 10表示成功 */
    private String code;
    
    /** 包装的数据 */
    private String data;
    
    /** 消息内容 */
    private String message;
    
    public static final String SUCCESS = "10";
    
    @Tolerate
    public ResponseAuthentication(String code, String message, JSON json) {
        this.code = code;
        this.message = message;
        this.data = json.toJSONString();
    }
    
}
