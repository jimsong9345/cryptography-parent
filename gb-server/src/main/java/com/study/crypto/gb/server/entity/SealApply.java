package com.study.crypto.gb.server.entity;

import com.study.crypto.general.spring.common.GeneralEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @author Songjin
 * @since 2021-06-13 11:24
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@SuperBuilder
@Table(name = "bo_seal_apply")
public class SealApply extends GeneralEntity {

    /** 行政区划代码 */
    @Column(name = "district_code")
    private String districtCode;

    /** 备案任务编号 */
    @Column(name = "task_id")
    private String taskId;

    /** 印章编码，省市区(6个字节)+印章赋码(8个字节) */
    @Column(name = "yzbm")
    private String yzbm;

    /** base64编码应用维护对称密钥密文。gmt 0018标准 */
    @Column(name = "app_sym_key_enc")
    private String appSymKeyEnc;

    /** base64编码数据加密对称密钥密文。gmt 0018标准 */
    @Column(name = "data_sym_key_enc")
    private String dataSymKeyEnc;

    /** base64编码签名证书 */
    @Column(name = "seal_sign_cert")
    private String sealSignCert;

    /** base64编码加密证书 */
    @Column(name = "seal_enc_cert")
    private String sealEncCert;

    /** 公安下发的加密文件，base64编码，使用对称加密算法模式为sm4_ecb，填充方式为 pkcs7，加密文件的格式内容见B.3，最大不大于40K */
    @Column(name = "enc_file")
    private String encFile;
    
    /** 印章备案信息表外键 */
    @Column(name = "info_id")
    private String infoId;
    
    /** 业务类型编码: 0 表示备案业务，1 表示保护密钥更新业务，2 表示印章撤销，3 表示申请电子印章唯一赋码业务 */
    @Column(name = "task_type")
    private String taskType;
    
}