package com.study.crypto.gb.server.exception;

/**
 * @author Songjin
 * @since 2021-06-11 19:27
 */
public class CertificateException extends RuntimeException {
    
    public CertificateException(String message) {
        super(message);
    }
    
    public CertificateException(String message, Throwable cause) {
        super(message, cause);
    }
}
