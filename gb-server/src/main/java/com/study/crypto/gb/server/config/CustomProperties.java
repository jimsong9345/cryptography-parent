package com.study.crypto.gb.server.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Songjin
 * @since 2021-02-27 21:27
 */
@Component
@ConfigurationProperties(prefix = "custom")
@Getter @Setter
public class CustomProperties {
    
    /** 当数据库中没有数据时，默认的设备编号 */
    private long defaultDeviceCode;
}
