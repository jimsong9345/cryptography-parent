package com.study.crypto.gb.server.mapper;

import com.study.crypto.general.spring.common.GeneralMapper;
import com.study.crypto.gb.server.entity.Secret;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Songjin
 * @since 2021-06-04 19:42
 */
@Primary
@Repository
public interface SecretMapper extends GeneralMapper<Secret> {
}