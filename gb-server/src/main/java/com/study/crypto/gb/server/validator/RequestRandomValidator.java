package com.study.crypto.gb.server.validator;

import com.study.crypto.dto.RequestRandomDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author Songjin
 * @since 2021-06-19 18:18
 */
@Slf4j
public class RequestRandomValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(RequestRandomDto.class);
    }
    
    @Override
    public void validate(Object o, Errors errors) {
        log.info("校验随机数请求: RequestRandomDto");
    }
}
