package com.study.crypto.gb.server.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.study.crypto.dto.RequestRandomDto;
import com.study.crypto.dto.gb.*;
import com.study.crypto.gb.server.annotation.AuthenticateParameter;
import com.study.crypto.gb.server.config.EnvironmentConfig;
import com.study.crypto.gb.server.dto.RequestAuthentication;
import com.study.crypto.gb.server.dto.ResponseAuthentication;
import com.study.crypto.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * 鉴权服务
 * @author Songjin
 * @since 2021-05-31 16:59
 */
@Slf4j
@RestController
@RequestMapping(value = "/auth")
public class AuthenticationController {
    
    @Autowired
    private EnvironmentConfig config;
    
    /**
     * 电子印章状态推送：身份认证
     *
     * @param request 请求数据
     * @return ResponseTransferDto
     * @throws IOException 异常
     */
    @PostMapping(value = "/identityAuthenticate")
    public ResponseAuthentication identityAuthenticate(@AuthenticateParameter RequestAuthentication request) throws IOException {
        String parameter = request.getParameter();
        RequestAuthenticateDto actualRequest = JSON.parseObject(parameter, RequestAuthenticateDto.class);
        String requestUrl = config.getHttpUrl() + "/publish/identityAuthenticate";
        JSONObject jsonResponse = HttpUtils.doPost(requestUrl, JSON.toJSONString(actualRequest));
        return new ResponseAuthentication(ResponseAuthentication.SUCCESS, "请求成功", jsonResponse);
    }
    
    /**
     * 确认回执
     * @param request 请求
     * @return 响应
     * @throws IOException 异常
     */
    @PostMapping(value = "/confirmReceipt")
    public ResponseAuthentication confirmReceipt(@AuthenticateParameter RequestAuthentication request) throws IOException {
        String parameter = request.getParameter();
        RequestReceiptDto actualRequest = JSON.parseObject(parameter, RequestReceiptDto.class);
        String requestUrl = config.getHttpUrl() + "/gb/confirmReceipt";
        JSONObject jsonResponse = HttpUtils.doPost(requestUrl, JSON.toJSONString(actualRequest));
        return new ResponseAuthentication(ResponseAuthentication.SUCCESS, "请求成功", jsonResponse);
    }
    
    /**
     * 查询下载公安下发数据
     * @param request 请求
     * @return 响应
     * @throws IOException 异常
     */
    @PostMapping(value = "/checkResult")
    public ResponseAuthentication checkResult(@AuthenticateParameter RequestAuthentication request) throws IOException {
        String parameter = request.getParameter();
        RequestPoliceDto actualRequest = JSON.parseObject(parameter, RequestPoliceDto.class);
        String requestUrl = config.getHttpUrl() + "/gb/checkResult";
        JSONObject jsonResponse = HttpUtils.doPost(requestUrl, JSON.toJSONString(actualRequest));
        return new ResponseAuthentication(ResponseAuthentication.SUCCESS, "请求成功", jsonResponse);
    }
    
    /**
     * 上传备案信息
     * @param request 请求
     * @return 响应
     * @throws IOException 异常
     */
    @PostMapping(value = "/uploadRecordInfos")
    public ResponseAuthentication uploadRecordInfos(@AuthenticateParameter RequestAuthentication request) throws IOException {
        String parameter = request.getParameter();
        RequestSealRecordDto actualRequest = JSON.parseObject(parameter, RequestSealRecordDto.class);
        String requestUrl = config.getHttpUrl() + "/gb/uploadRecordInfos";
        JSONObject jsonResponse = HttpUtils.doPost(requestUrl, JSON.toJSONString(actualRequest));
        return new ResponseAuthentication(ResponseAuthentication.SUCCESS, "请求成功", jsonResponse);
    }
    
    /**
     * 申请唯一赋码
     * @param request 请求
     * @return 响应
     * @throws IOException 异常
     */
    @PostMapping(value = "/applySealCode")
    public ResponseAuthentication applySealCode(@AuthenticateParameter RequestAuthentication request) throws IOException {
        String parameter = request.getParameter();
        RequestUniqueCodeDto actualRequest = JSON.parseObject(parameter, RequestUniqueCodeDto.class);
        String requestUrl = config.getHttpUrl() + "/gb/applySealCode";
        JSONObject jsonResponse = HttpUtils.doPost(requestUrl, JSON.toJSONString(actualRequest));
        return new ResponseAuthentication(ResponseAuthentication.SUCCESS, "请求成功", jsonResponse);
    }

    /**
     * 申请随机数
     * @param request 请求
     * @return 响应
     * @throws IOException 异常
     */
    @PostMapping(value = "/applyRandom")
    public ResponseAuthentication applyRandom(@AuthenticateParameter RequestAuthentication request) throws IOException {
        String parameter = request.getParameter();
        RequestRandomDto actualRequest = JSON.parseObject(parameter, RequestRandomDto.class);
        String requestUrl = config.getHttpUrl() + "/gb/applyRandom";
        JSONObject jsonResponse = HttpUtils.doPost(requestUrl, JSON.toJSONString(actualRequest));
        return new ResponseAuthentication(ResponseAuthentication.SUCCESS, "请求成功", jsonResponse);
    }


}
