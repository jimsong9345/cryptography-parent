package com.study.crypto.gb.server.validator;

import com.study.crypto.dto.gb.GbConstants;
import com.study.crypto.dto.gb.RequestReceiptDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;

/**
 * @author Songjin
 * @since 2021-06-27 23:07
 */
@Slf4j
public class RequestReceiptValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(RequestReceiptDto.class);
    }
    
    @Override
    public void validate(Object o, Errors errors) {
        log.info("校验确认回执请求: RequestReceiptDto");
        RequestReceiptDto target = (RequestReceiptDto) o;
        String taskCode = target.getTaskCode();
        if (StringUtils.isBlank(taskCode)) {
            return ;
        }
        if (!GbConstants.CONFIRM_RECEIPT.equals(taskCode)) {
            errors.rejectValue("taskCode", null, "taskCode参数必须为'confirmReceipt'");
        }
        String taskTypeCode = target.getTaskTypeCode();
        if (StringUtils.isBlank(taskTypeCode)) {
            return ;
        }
        List<String> list = Arrays.asList(GbConstants.TASK_TYPE_CODE_UPLOAD_RECORD_INFOS, GbConstants.TASK_TYPE_CODE_UPDATE_PROTECT_KEY);
        if (!list.contains(taskTypeCode)) {
            errors.rejectValue("taskTypeCode", null, "taskTypeCode参数只能为'0'或'1'");
        }
    }
}
