package com.study.crypto.gb.server.controller;

import com.study.crypto.dto.gb.RequestAuthenticateDto;
import com.study.crypto.dto.gb.RequestTransferDto;
import com.study.crypto.dto.gb.ResponseAuthenticateDto;
import com.study.crypto.dto.gb.ResponseTransferDto;
import com.study.crypto.gb.server.service.PublishService;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.crypto.CryptoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * 发布系统接口
 * @author Songjin
 * @since 2021-07-10 1:47
 */
@Slf4j
@RestController
@RequestMapping(value = "/publish")
public class GbPublishController {
    
    @Autowired
    private PublishService publishService;
    
    /**
     * 电子印章状态推送：数据传输
     * @param request 请求数据
     * @return ResponseTransferDto
     */
    @PostMapping(value = "/dataTransfer")
    public ResponseTransferDto dataTransfer(@RequestBody RequestTransferDto request) {
        return publishService.dataTransfer(request);
    }
    
    /**
     * 电子印章状态推送：身份认证
     * @param request 请求数据
     * @return ResponseAuthenticateDto
     * @throws GeneralSecurityException 异常
     * @throws CryptoException 异常
     * @throws CMSException 异常
     * @throws IOException 异常
     */
    @PostMapping(value = "/identityAuthenticate")
    public ResponseAuthenticateDto identityAuthenticate(@RequestBody RequestAuthenticateDto request) throws GeneralSecurityException, CryptoException, CMSException, IOException {
        return publishService.identityAuthenticate(request);
    }
}
