package com.study.crypto.gb.server.exception;

/**
 * @author Songjin
 * @since 2021-06-05 15:04
 */
public class AuthenticationException extends RuntimeException {
    
    private final String path;
    
    public AuthenticationException() {
        this.path = "";
    }
    
    public AuthenticationException(String message, String path) {
        super(message);
        this.path = path;
    }
    
    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
        this.path = "";
    }
    
    public AuthenticationException(Throwable cause) {
        super(cause);
        this.path = "";
    }
    
    public AuthenticationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.path = "";
    }
    
    public String getPath() {
        return path;
    }
}
