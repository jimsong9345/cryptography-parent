package com.study.crypto.gb.server.validator;

import com.study.crypto.dto.gb.GbConstants;
import com.study.crypto.dto.gb.RequestUniqueCodeDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author Songjin
 * @since 2021-06-19 18:21
 */
@Slf4j
public class RequestUniqueCodeValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(RequestUniqueCodeDto.class);
    }
    
    @Override
    public void validate(Object o, Errors errors) {
        log.info("校验唯一赋码请求: RequestUniqueCodeDto");
        RequestUniqueCodeDto target = (RequestUniqueCodeDto) o;
        String taskCode = target.getTaskCode();
        if (StringUtils.isBlank(taskCode)) {
            return ;
        }
        if (!GbConstants.APPLY_SEAL_CODE.equals(taskCode)) {
            errors.rejectValue("taskCode", null, "taskCode参数必须为'applySealCode'");
        }
        String taskTypeCode = target.getTaskTypeCode();
        if (StringUtils.isBlank(taskTypeCode)) {
            return ;
        }
        if (!GbConstants.TASK_TYPE_CODE_APPLY_SEAL_CODE.equals(taskTypeCode)) {
            errors.rejectValue("taskTypeCode", null, "taskTypeCode参数必须为'3'");
        }
    }
}
