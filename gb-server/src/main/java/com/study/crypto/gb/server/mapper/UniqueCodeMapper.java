package com.study.crypto.gb.server.mapper;

import com.study.crypto.general.spring.common.GeneralMapper;
import com.study.crypto.gb.server.entity.UniqueCode;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Songjin
 * @since 2021-06-06 23:32
 */
@Primary
@Repository
public interface UniqueCodeMapper extends GeneralMapper<UniqueCode> {
    
    /**
     * 查询行政区划下最大唯一赋码值
     * @param areaNumber 行政区划码，前3字节
     * @return 最大唯一赋码值
     */
    @Select("select max(unique_code) from bo_unique_code where area_number = #{areaNumber}")
    String selectMaxUniqueCode(@Param("areaNumber") String areaNumber);
}