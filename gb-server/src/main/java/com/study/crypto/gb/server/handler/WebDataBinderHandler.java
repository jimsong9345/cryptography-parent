package com.study.crypto.gb.server.handler;

import com.study.crypto.gb.server.validator.RequestRandomValidator;
import com.study.crypto.gb.server.validator.RequestReceiptValidator;
import com.study.crypto.gb.server.validator.RequestSealRecordValidator;
import com.study.crypto.gb.server.validator.RequestUniqueCodeValidator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Songjin
 * @since 2021-04-21 14:09
 */
@RestControllerAdvice
public class WebDataBinderHandler {
    
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        Object target = webDataBinder.getTarget();
        if (target != null) {
            RequestRandomValidator validator1 = new RequestRandomValidator();
            if (validator1.supports(target.getClass())) {
                webDataBinder.addValidators(validator1);
            }
            RequestUniqueCodeValidator validator2 = new RequestUniqueCodeValidator();
            if (validator2.supports(target.getClass())) {
                webDataBinder.addValidators(validator2);
            }
            RequestSealRecordValidator validator3 = new RequestSealRecordValidator();
            if (validator3.supports(target.getClass())) {
                webDataBinder.addValidators(validator3);
            }
            RequestReceiptValidator validator4 = new RequestReceiptValidator();
            if (validator4.supports(target.getClass())) {
                webDataBinder.addValidators(validator4);
            }
        }
    }
}
