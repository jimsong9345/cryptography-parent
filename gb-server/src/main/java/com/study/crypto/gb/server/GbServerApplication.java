package com.study.crypto.gb.server;

import com.study.crypto.general.spring.config.MyBatisPluginAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Songjin
 * @since 2021-05-31 13:05
 */
@MapperScan(basePackages = "com.study.crypto.basic.gb.mapper")
@EnableTransactionManagement
@SpringBootApplication
@Import(MyBatisPluginAutoConfiguration.class)
public class GbServerApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(GbServerApplication.class, args);
    }
}
