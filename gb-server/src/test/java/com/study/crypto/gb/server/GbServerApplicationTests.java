package com.study.crypto.gb.server;

import com.alibaba.fastjson.JSON;
import com.study.crypto.dto.RequestRandomDto;
import com.study.crypto.dto.SignInfoDto;
import com.study.crypto.dto.gb.RequestSealRecordDto;
import com.study.crypto.dto.gb.RequestSealRecordDto.RequestSealRecordDtoData;
import com.study.crypto.dto.gb.RequestUniqueCodeDto;
import com.study.crypto.dto.gb.RequestUniqueCodeDto.RequestUniqueCodeDtoData;
import com.study.crypto.dto.gb.SealDataDto;
import com.study.crypto.gb.server.entity.Certification;
import com.study.crypto.gb.server.entity.KeyStorage;
import com.study.crypto.gb.server.mapper.CertificationMapper;
import com.study.crypto.gb.server.mapper.KeyStorageMapper;
import com.study.crypto.gb.server.mapper.SealDataMapper;
import com.study.crypto.signer.Signer;
import com.study.crypto.signer.SignerFactory;
import com.study.crypto.utils.CertUtils;
import com.study.crypto.utils.DigestUtil;
import com.study.crypto.utils.EssPdfUtil;
import com.study.crypto.utils.KeyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.File;
import java.security.PrivateKey;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Songjin
 * @since 2021-06-04 19:51
 */
@Slf4j
@SpringBootTest(classes = GbServerApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
class GbServerApplicationTests {
    
    private Certification gomainClient;
    private KeyStorage    keyStorage;
    private PrivateKey    privateKey;
    private final String tokenInfo = "352afc9dfb0f47341f071ec4a6e04bf7";
    private final Signer signer = SignerFactory.produce(GMObjectIdentifiers.sm2sign_with_sm3);
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private KeyStorageMapper keyStorageMapper;
    @Autowired
    private CertificationMapper certificationMapper;
    @Autowired
    private SealDataMapper sealDataMapper;
    
    @BeforeEach
    void BeforeEach() {
        Certification gomainQuery = Certification.builder().ext1("GomainClient").build();
        gomainClient = certificationMapper.selectOne(gomainQuery);
        keyStorage = keyStorageMapper.selectByPrimaryKey(gomainClient.getKeyId());
        privateKey = KeyUtils.convertPrivateKey(ByteUtils.subArray(keyStorage.getKeyBytes(), 64));
    }
    
    @Test
    void testSimpleDatabase() throws Exception{
        // Certification gomainQuery = Certification.builder().ext1("Gomain").build();
        // Certification gomain = certificationMapper.selectOne(gomainQuery);
        // byte[] certificate = gomain.getCertificate();
        // // 计算证书表中，证书摘要值字段
        // String digestBase64 = DigestUtil.digestBase64(certificate);
        // gomain.setDigest(digestBase64);
        // certificationMapper.updateByPrimaryKey(gomain);
    
        byte[] cert = FileUtils.readFileToByteArray(new File("C:/Users/Administrator/Desktop/密码学记录/发布系统.cer"));
        Certificate certificate = Certificate.getInstance(cert);
        String digest = DigestUtil.sm3Base64(cert);
        
        LocalDateTime now = LocalDateTime.now();
        Certification certification = Certification.builder()
                                                   .id(EssPdfUtil.genRandomUuid())
                                                   .createTime(now)
                                                   .updateTime(now)
                                                   .certType(Certification.CERT_TYPE_SM2)
                                                   .certDn(certificate.getSubject().toString())
                                                   .beginTime(EssPdfUtil.of(certificate.getEndDate().getDate()))
                                                   .endTime(EssPdfUtil.of(certificate.getEndDate().getDate()))
                                                   .serialNumber(certificate.getSerialNumber().getPositiveValue().toString(16).toUpperCase())
                                                   .keyUsage(CertUtils.certificateUsage(cert))
                                                   .certificate(cert)
                                                   .digest(digest).build();
        certificationMapper.insert(certification);
    }
    
    /**
     * 上传印章备案信息接口
     */
    @Test
    void testUploadRecordInfos() throws Exception{
        byte[] imgBytes = FileUtils.readFileToByteArray(new File("C:/Users/Administrator/Desktop/密码学记录/1.png"));
        SealDataDto sealDataDto = SealDataDto.builder()
                                             .jbr_xm("")
                                             .jbr_zjlx("111")
                                             .jbr_zjhm("4201160199109157390")
                                             .ymsj(Base64.encodeBase64String(imgBytes))
                                             .yzbm("21110400000172")
                                             .yzlxdm("05")
                                             .yzmc("京东_法定名称章1008")
                                             .yzsydw_dwmc("京东")
                                             .yzsydw_dwssmzwzmc("")
                                             .yzsydw_dwywmc("")
                                             .yzsydw_tyshxydm("911100001011016602")
                                             .yzzzdw_dwmc("北京数字认证股份有限公司")
                                             .yzzzdw_dwssmzwzmc("北京数字认证股份有限公司")
                                             .yzzzdw_dwywmc("BJCA")
                                             .yzzzdwbm("")
                                             .zzrq("2021-06-11 12:44:06").build();
        RequestSealRecordDtoData sealRecordDtoData = RequestSealRecordDtoData.builder()
                                                                             .districtCode("211104")
                                                                             .sealSignCert("MIIB5TCCAYqgAwIBAgIKGhAAAAAAAAAADzAKBggqgRzPVQGDdTBDMQswCQYDVQQGEwJDTjENMAsGA1UECgwEQkpDQTENMAsGA1UECwwEQkpDQTEWMBQGA1UEAwwNU00yIFNlcnZlciBDQTAeFw0yMTA2MjAwODM2MTRaFw0yMjA2MjAwODM2MTRaMEkxCzAJBgNVBAYTAkNOMSEwHwYDVQQKDBg5MTExMDAwMDEwMTEwMTY2MDLkuqzkuJwxFzAVBgNVBAMMDjIxMTEwNDAwMDAwMTY4MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAElY+GYXwhQiSaiba5DY01A4z9WJEiPu9u0QNVsEbUCFSwQk/boLTSxspha4vMxeVCweIpT0JMw52zPFnRH+CEEaNgMF4wDAYDVR0TAQH/BAIwADAOBgNVHQ8BAf8EBAMCBsAwHwYDVR0jBBgwFoAU8tZzbdlMzz+r6V1bO92x7xgk6jQwHQYDVR0OBBYEFOSrOaOQSLOYzs5Fxe58wZr41ubqMAoGCCqBHM9VAYN1A0kAMEYCIQDWPJ2HVnsA5rNVl0vJW7BksIvF035Qr4hTv+znap9yhAIhAMf8F7i0CIbtF+3PjGYZePFW2s1it97X5/t0aqLSrooP")
                                                                             .sealEncCert("MIIB5DCCAYqgAwIBAgIKGhAAAAAAAAAAEDAKBggqgRzPVQGDdTBDMQswCQYDVQQGEwJDTjENMAsGA1UECgwEQkpDQTENMAsGA1UECwwEQkpDQTEWMBQGA1UEAwwNU00yIFNlcnZlciBDQTAeFw0yMTA2MjAwODM2MTRaFw0yMjA2MjAwODM2MTRaMEkxCzAJBgNVBAYTAkNOMSEwHwYDVQQKDBg5MTExMDAwMDEwMTEwMTY2MDLkuqzkuJwxFzAVBgNVBAMMDjIxMTEwNDAwMDAwMTY4MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEfw18HgQj4JZr5IamgwqIWWEa4kROmpTI2oHGdRQV/PGkCQIobNQnW5oTpBSExS5fDxcvEiAZmWNBv/nqDukwf6NgMF4wDAYDVR0TAQH/BAIwADAOBgNVHQ8BAf8EBAMCAzgwHwYDVR0jBBgwFoAU8tZzbdlMzz+r6V1bO92x7xgk6jQwHQYDVR0OBBYEFFAnFJDDtTkBz/ojKS3UfPYWOC3KMAoGCCqBHM9VAYN1A0gAMEUCIHGE0I/omiDfhvEd83Bsj2zYcdZCkf2MUIq9t8g380+TAiEA7IE9FpCmUgmnerWeX7G2KB0POoZu9mPR4U7FHzOz87s=")
                                                                             .sealData(sealDataDto).build();
        RequestSealRecordDto actualRequest = new RequestSealRecordDto();
        actualRequest.getData().add(sealRecordDtoData);
        actualRequest.setTokenInfo(tokenInfo);
        byte[] inData = JSON.toJSONBytes(actualRequest);
        byte[] signature = signer.sign(inData, privateKey);
        actualRequest.setSignInfo(new SignInfoDto(GMObjectIdentifiers.sm2sign_with_sm3.getId(), signature));
        String parameter = JSON.toJSONString(actualRequest);
    
        String username = "D05U023ZH001";
        String appKey = "39946967-0694-4713-bcdf-20206e729b58";
        String secret = "c894577ed82e6a556048af00f0af87983a76918fd3686afe59cfddb021a4e7e8";
        String nowDate = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        String nonce = EssPdfUtil.genRandomUuid();
        String plainText = nonce.concat("_").concat(nowDate).concat("_").concat(secret);
        String digestText = EssPdfUtil.digestHex(plainText);
        
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/uploadRecordInfos");
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED)
                      .param("App_key", appKey)
                      .param("Username", username)
                      .param("Nonce", nonce)
                      .param("PasswdDigest", digestText)
                      .param("Created", nowDate)
                      .param("parameter", parameter);
        ResultActions resultActions = mockMvc.perform(requestBuilder)
                                             .andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();
        String contentAsString = response.getContentAsString();
        log.info("上传备案信息响应数据: {}", contentAsString);
    }
    
    /**
     * 申请唯一赋码接口
     */
    @Test
    void testApplySealCode() throws Exception{
        RequestUniqueCodeDtoData uniqueCodeDtoData = RequestUniqueCodeDtoData.builder().areaNumber("110").build();
        RequestUniqueCodeDto actualRequest = new RequestUniqueCodeDto();
        actualRequest.setTokenInfo(tokenInfo);
        actualRequest.setData(uniqueCodeDtoData);
        byte[] inData = JSON.toJSONBytes(actualRequest);
        byte[] signature = signer.sign(inData, privateKey);
        actualRequest.setSignInfo(new SignInfoDto(GMObjectIdentifiers.sm2sign_with_sm3.getId(), signature));
        String parameter = JSON.toJSONString(actualRequest);
    
        String username = "D05U023ZH001";
        String appKey = "8c4f15f5-a8a2-44d8-bb3c-74f2186bc13f";
        String secret = "822adbabd2d09b1539106e5abc7ab58f09afc6b0a954c4d8e83b907f4a946577";
        String nowDate = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        String nonce = EssPdfUtil.genRandomUuid();
        String plainText = nonce.concat("_").concat(nowDate).concat("_").concat(secret);
        String digestText = EssPdfUtil.digestHex(plainText);
        
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/applySealCode");
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED)
                      .param("App_key", appKey)
                      .param("Username", username)
                      .param("Nonce", nonce)
                      .param("PasswdDigest", digestText)
                      .param("Created", nowDate)
                      .param("parameter", parameter);
        ResultActions resultActions = mockMvc.perform(requestBuilder)
                                             .andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult = resultActions.andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();
        String contentAsString = response.getContentAsString();
        log.info("申请唯一赋码响应数据: {}", contentAsString);
    }
    
    /**
     * 申请随机数
     */
    @Test
    void testApplyRandom() throws Exception {
        RequestRandomDto actualRequest = RequestRandomDto.getRequestRandomDtoGb();
        String applicantCert = Base64.encodeBase64String(gomainClient.getCertificate());
        actualRequest.setApplicantCert(applicantCert);
        actualRequest.setRandomA(EssPdfUtil.genRandomUuid().substring(0, 16));
        String parameter = JSON.toJSONString(actualRequest);
    
        String username = "D05U023ZH001";
        String appKey = "eb735ace-36f3-4af5-89f9-eee4f57906f5";
        String secret = "a2fd71fae31c7cc7511501a44a117701567a1bef72140b5186051ce88df7f42e";
        String nowDate = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        String nonce = EssPdfUtil.genRandomUuid();
        String plainText = nonce.concat("_").concat(nowDate).concat("_").concat(secret);
        String digestText = EssPdfUtil.digestHex(plainText);
        
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/applyRandom");
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED)
                      .param("App_key", appKey)
                      .param("Username", username)
                      .param("Nonce", nonce)
                      .param("PasswdDigest", digestText)
                      .param("Created", nowDate)
                      .param("parameter", parameter);
        mockMvc.perform(requestBuilder)
               .andExpect(MockMvcResultMatchers.status().isOk());
        log.info("获取token，randomA: {}", actualRequest.getRandomA());
    }
    
}
