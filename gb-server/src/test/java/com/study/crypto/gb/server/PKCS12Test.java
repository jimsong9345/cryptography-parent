package com.study.crypto.gb.server;

import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.util.Enumeration;

/**
 * @author Songjin
 * @since 2023-01-04 10:55
 */
class PKCS12Test {

    @Test
    void testReadJKS() throws Exception{
        String password = "msspmssp";
        String jksPath = "C:/Users/Administrator/Desktop/msspjcestore.jks";
        FileInputStream input = new FileInputStream(jksPath);
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(input, password.toCharArray());

        Enumeration<String> aliases = keyStore.aliases();
        int count = 0;
        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            Key key = keyStore.getKey(alias, password.toCharArray());
            Certificate certificate = keyStore.getCertificate(alias);
            System.out.println(key);
            System.out.println(certificate);
            count++;
        }
        System.out.println(count);
    }

}
