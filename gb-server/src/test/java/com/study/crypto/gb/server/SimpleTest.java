package com.study.crypto.gb.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.study.crypto.asn1.ga.GAData;
import com.study.crypto.dto.RequestRandomDto;
import com.study.crypto.dto.gb.RequestPoliceDto;
import com.study.crypto.dto.gb.SealDataDto;
import com.study.crypto.gb.server.entity.SealData;
import com.study.crypto.utils.ByteUtility;
import com.study.crypto.utils.EssPdfUtil;
import com.study.crypto.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.RequestBuilder;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.pqc.legacy.math.linearalgebra.LittleEndianConversions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanWrapperImpl;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Songjin
 * @since 2021-06-12 21:02
 */
@Slf4j
class SimpleTest {
    
    @Test
    void testApplyRandom() throws Exception{
        RequestRandomDto actualRequest = RequestRandomDto.getRequestRandomDtoGb();
        String           applicantCert = "MIICBTCCAaygAwIBAgIKGhAAAAAAAAAADjAKBggqgRzPVQGDdTBDMQswCQYDVQQGEwJDTjENMAsGA1UECgwEQkpDQTENMAsGA1UECwwEQkpDQTEWMBQGA1UEAwwNU00yIFNlcnZlciBDQTAeFw0yMTA2MDYxNTA2NDhaFw0yMjA2MDYxNTA2NDhaMGsxCzAJBgNVBAYTAkNOMS0wKwYDVQQKDCTmtYvor5XliLbkvZzns7vnu5/nrb7lkI3or4HkuabmtYvor5UxLTArBgNVBAMMJOa1i+ivleWItuS9nOezu+e7n+etvuWQjeivgeS5pua1i+ivlTBZMBMGByqGSM49AgEGCCqBHM9VAYItA0IABEmcASa5rd5wkulgDV4d7/vMcFkmfK92aTF2KkO1sEac/fIufYQduFL3UlGx1mK88972jQ98sKjydDekiQ/mxsWjYDBeMAwGA1UdEwEB/wQCMAAwDgYDVR0PAQH/BAQDAgbAMB8GA1UdIwQYMBaAFPLWc23ZTM8/q+ldWzvdse8YJOo0MB0GA1UdDgQWBBRJEJ1NIe4485HgP3zFFV9WlUQh0jAKBggqgRzPVQGDdQNHADBEAiAnodDPB13FY7Isz0IyXM5z4zgm3PySJV8/1PrP/VglEwIgZLMFxuFWrw79mwvHZ2FzOuBvgl9GNAMOGdH/1M0troY=";
        actualRequest.setApplicantCert(applicantCert);
        actualRequest.setRandomA(EssPdfUtil.genRandomUuid().substring(0, 16));
        String parameter = JSON.toJSONString(actualRequest);
    
        String username = "D05U023ZH001";
        String appKey = "eb735ace-36f3-4af5-89f9-eee4f57906f5";
        String secret = "a2fd71fae31c7cc7511501a44a117701567a1bef72140b5186051ce88df7f42e";
        String nowDate = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        String nonce = EssPdfUtil.genRandomUuid();
        String plainText = nonce.concat("_").concat(nowDate).concat("_").concat(secret);
        String digestText = EssPdfUtil.digestHex(plainText);
    
        List<NameValuePair> parameters = RequestBuilder.post()
                                                       .addParameter("App_key", appKey)
                                                       .addParameter("Username", username)
                                                       .addParameter("Nonce", nonce)
                                                       .addParameter("PasswdDigest", digestText)
                                                       .addParameter("Created", nowDate)
                                                       .addParameter("parameter", parameter)
                                                       .getParameters();
        String apiUrl = "http://127.0.0.1/auth/applyRandom";
        String str = HttpUtils.doPost(apiUrl, parameters);
        System.out.println(str);
    }
    
    @Test
    void testBeanWrapper() throws Exception{
        RequestPoliceDto policeDto = new RequestPoliceDto();
        BeanWrapperImpl beanWrapper = new BeanWrapperImpl(policeDto);
        Object version = beanWrapper.getPropertyValue("version");
        System.out.println(version);
    
        beanWrapper.setPropertyValue("version", "2.0");
        System.out.println(policeDto);
    }
    
    @Test
    void testSignatureParsed2() throws Exception {
        String signVal   = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCSCt16TDC9iVrV78vvj7gR7wOIMGMGlSBWk22LDD9qQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6POd3l58AcdJ3N1w45uwPnWcg+dyzG59P/mVtW6H9sg=";
        byte[] signature = Base64.decodeBase64(signVal);
        System.out.println(signature);
    
        byte[] a = {1, 2, 3};
        byte[] b = {4, 5, 6, 7};
        byte[][] ab = {a, b};
        System.out.println(ab);
    
        byte[] concatenate = ByteUtils.concatenate(ab);
        System.out.println(concatenate);
    }
    
    @Test
    void testSignatureParsed() throws Exception{
        String signVal = "jy2sW9DVGZYPEF1sXh/p4soemetxSxlFjwUeetWvlzQmlqGVmhbE3xqq6/nVD/3LpLrpd00MyIXPn1P6QfuqIQ==";
        byte[] signature = Base64.decodeBase64(signVal);
        ASN1InputStream inputStream = new ASN1InputStream(signature);
        inputStream.readObject();
    } 
    
    /**
     * fastjson 会自动识别json串中的下划线，如果 bean 里面的属性是驼峰式的，会对应赋值;
     * 如果json串中是驼峰式的，而 bean 是下划线的，也会对应赋值
     */
    @Test
    void testJSON() throws Exception{
        File        file        = new File("C:/Users/Administrator/Desktop/密码学记录/11.json");
        String      str         = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        JSONObject jsonObject = JSON.parseObject(str);
        JSONObject  sealData = jsonObject.getJSONArray("data").getJSONObject(0).getJSONObject("sealData");
        SealDataDto sealDataDto = sealData.toJavaObject(SealDataDto.class);
        SealData sealData1 = sealData.toJavaObject(SealData.class);
        GAData gaData;
        System.out.println(sealDataDto);
    }
    
    @Test
    void testLocalDateTime() throws Exception{
        LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(1624016125, 0, ZoneOffset.ofHours(8));
        System.out.println(localDateTime);
    
        byte[] bytes = new byte[180];
        System.out.println(bytes);
    
        byte[] bytes2 = LittleEndianConversions.I2OSP(16);
        System.out.println(bytes2);
        byte[] bytes1 = ByteUtility.intToByte(16);
        System.out.println(bytes1);
    }
    
    @Test
    public void testSM2Encrpt() throws Exception{
        String enccertHex = "308203DB30820381A003020102020A1A1000000000013DD1B5300A06082A811CCF550183753044310B300906035504061302434E310D300B060355040A0C04424A4341310D300B060355040B0C04424A43413117301506035504030C0E4265696A696E6720534D32204341301E170D3230303130373033323234375A170D3235303130353033323234375A3064310B300906035504061302434E313C303A060355040A0C33393131313031313131303237363239393741E58C97E4BAACE5B882E688BFE5B1B1E58CBAE7949FE68081E78EAFE5A283E5B1803117301506035504030C0E31313030313130303031353239323059301306072A8648CE3D020106082A811CCF5501822D034200041F2162553799D4DAA9A7B522BE2049AEE4AE17BA8C253B31170F0F5D2241836C0821741C82F320069A86A24D83BE21310918F1EF8AB5B8D9915183C34A8FCA43A382023930820235301F0603551D230418301680141FE6CFD48FC5222A974A298A15E716C99234C4B6301D0603551D0E041604144F753C4005E93930FB69B5C71C38C669315BB50E30819F0603551D1F0481973081943061A05FA05DA45B3059310B300906035504061302434E310D300B060355040A0C04424A4341310D300B060355040B0C04424A43413117301506035504030C0E4265696A696E6720534D32204341311330110603550403130A6361323163726C313035302FA02DA02B8629687474703A2F2F63726C2E626A63612E6F72672E636E2F63726C2F6361323163726C3130352E63726C3024060A2A811C86EF320201010104160C144A4A3931313130313131313032373632393937413022060A2A811C86EF320201010804140C12393131313031313131303237363239393741301F060A2A811C86EF320201010E04110C0F313032303830303035353031393732302D060A2A811C86EF3202010117041F0C1D31403231353030394A4A303931313130313131313032373632393937413014060A2A811C86EF320201011E04060C04323536333027060A2A811C86EF320201011D04190C173142404A4A393131313031313131303237363239393741306B06082B06010505070101045F305D302806082B06010505073001861C687474703A2F2F6F6373702E626A63612E6F72672E636E2F6F637370303106082B060105050730028625687474703A2F2F6F6373702E626A63612E6F72672E636E2F424A4341534D3243412E703762300B0603551D0F040403020338300A06082A811CCF5501837503480030450220558C7031C9FE774F0D1F81F2055E856B2C349F33F08110F4E24DB82687CAFD02022100C7F1A9FB79DCD6DBA62F8E182524D4E509A0C379EA83BDE79AFF369D298CA934";
        byte[] decode = Hex.decode(enccertHex);
        Certificate certificate = Certificate.getInstance(decode);
        SubjectPublicKeyInfo publicKeyInfo = certificate.getSubjectPublicKeyInfo();
        byte[] bytes = publicKeyInfo.getPublicKeyData().getBytes();
        System.out.println(bytes);
    }
    
    @Test
    void testCommon() {
        String str = GMObjectIdentifiers.sm2sign_with_sm3.getId();
        log.info("SM3WithSM2签名算法标识: {}", str);
    
        String abc = StringUtils.reverse("abc");
        System.out.println(abc);
        
        List<String> list = Arrays
                .asList("0005000000054E2E", "0005000000054E23", "0005000000024DAA", "0005000000024E10",
                        "000500000002570C", "0005000000024E2E", "0005000000025675", "0005000000024DA5",
                        "0005000000024DA7", "0005000000024E51", "0005000000025208", "0005000000000000");
        list.forEach(hex -> {
            byte[] decode = Hex.decode(hex);
            // long l = ByteUtility.byteArrayToLong(decode);
            // System.out.println(l);
            //
            // byte[] bytes = ByteUtility.longToByte(l);
            // System.out.println(bytes);
            //
            // ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
            // buffer.putLong(l);
            // byte[] array = buffer.array();
            // System.out.println(array);
    
            ByteBuffer buffer2 = ByteBuffer.allocate(Long.BYTES);
            buffer2.put(decode);
            buffer2.flip();
            long aLong = buffer2.getLong();
            
            ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
            buffer.putLong(aLong);
            byte[] array = buffer.array();
    
            System.out.println(hex + "--->" + aLong + "--->" + Hex.toHexString(array).toUpperCase());
        });
    
    }
}
