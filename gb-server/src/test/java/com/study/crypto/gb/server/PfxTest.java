package com.study.crypto.gb.server;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.pkcs.ContentInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jce.PKCS12Util;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.pkcs.PKCS12PfxPdu;
import org.bouncycastle.pkcs.PKCS12SafeBag;
import org.bouncycastle.pkcs.PKCS12SafeBagFactory;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import org.bouncycastle.pkcs.jcajce.JcePKCSPBEInputDecryptorProviderBuilder;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Enumeration;

/**
 * @author Songjin
 * @since 2021-06-25 17:04
 */
class PfxTest {

    @Test
    void getPrivateKeyFromPfx() throws Exception {
        // byte[] pfxDER, String passwd
        Security.addProvider(new BouncyCastleProvider());
        char[] passwd = "12345678".toCharArray();
        byte[] pfxDER = FileUtils.readFileToByteArray(new File("F:/密码设备部/商密产品检测/北京CA远程用/sm2_signserver.pfx"));
        InputDecryptorProvider inputDecryptorProvider = new JcePKCSPBEInputDecryptorProviderBuilder()
                .setProvider(BouncyCastleProvider.PROVIDER_NAME).build(passwd);
        PKCS12PfxPdu pfx = new PKCS12PfxPdu(pfxDER);

        ContentInfo[] infos = pfx.getContentInfos();
        if (infos.length != 2) {
            throw new Exception("Only support one pair ContentInfo");
        }

        for (int i = 0; i != infos.length; i++) {
            if (!infos[i].getContentType().equals(PKCSObjectIdentifiers.encryptedData)) {
                PKCS12SafeBagFactory dataFact = new PKCS12SafeBagFactory(infos[i]);
                PKCS12SafeBag[] bags = dataFact.getSafeBags();
                PKCS8EncryptedPrivateKeyInfo encInfo = (PKCS8EncryptedPrivateKeyInfo) bags[0].getBagValue();
                PrivateKeyInfo info = encInfo.decryptPrivateKeyInfo(inputDecryptorProvider);
                BCECPrivateKey privateKey = convertPKCS8ToECPrivateKey(info.getEncoded());
                System.out.println(privateKey);
            }
        }
    }

    public static BCECPrivateKey convertPKCS8ToECPrivateKey(byte[] pkcs8Key) throws Exception {
        PKCS8EncodedKeySpec peks = new PKCS8EncodedKeySpec(pkcs8Key);
        KeyFactory kf = KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);
        return (BCECPrivateKey) kf.generatePrivate(peks);
    }

    @Test
    void testPKCS12Format() throws Exception{
        String path = "C:/Users/Administrator/Desktop/sm2_signserver.pfx";
        byte[] pfxBytes = FileUtils.readFileToByteArray(new File(path));
        byte[] pkcs12_ = PKCS12Util.convertToDefiniteLength(pfxBytes);
        FileUtils.writeByteArrayToFile(new File("C:/Users/Administrator/Desktop/sm2_signserver2.pfx"), pkcs12_);
    }


    @Test
    void testPKCS12Store() throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        // byte[] pkcs12 = FileUtils.readFileToByteArray(new File("D:/java/JetBrains/workspace01/cryptography-parent/gb-server/src/test/resources/密码74827440.pfx"));
        byte[] pkcs12 = FileUtils.readFileToByteArray(new File("D:/java/JetBrains/workspace01/cryptography-parent/gb-server/src/test/resources/密码111111.pfx"));
        // byte[] pkcs12 = FileUtils.readFileToByteArray(new File("C:/Users/Administrator/Desktop/test.pfx"));
        byte[] pkcs12_ = PKCS12Util.convertToDefiniteLength(pkcs12);
        char[] passwd = "111111".toCharArray();
        KeyStore store = KeyStore.getInstance("PKCS12", BouncyCastleProvider.PROVIDER_NAME);
        ByteArrayInputStream stream = new ByteArrayInputStream(pkcs12_);
    
        store.load(stream, passwd);
    
        Enumeration<String> en = store.aliases();
        String keyName = null;
        String certificateName = null;
        while (en.hasMoreElements()) {
            String n = en.nextElement();
            boolean certificateEntry = store.isCertificateEntry(n);
            boolean keyEntry = store.isKeyEntry(n);
            if (certificateEntry) {
                certificateName = n;
            }
            if (keyEntry) {
                keyName = n;
            }
        }
        if (StringUtils.isNotBlank(certificateName)) {
            X509Certificate cert = (X509Certificate) store.getCertificate(certificateName);
            System.out.println(Base64.encodeBase64String(cert.getEncoded()));
        }
        if (StringUtils.isNotBlank(keyName)) {
            // PrivateKey key = (PrivateKey) store.getKey(keyName, null);
            // System.out.println(Base64.encodeBase64String(key.getEncoded()));
            BCECPrivateKey key = (BCECPrivateKey) store.getKey(keyName, null);
            System.out.println(Base64.encodeBase64String(key.getD().toByteArray()));
        }
        // Signer signer = SignerFactory.produce(PKCSObjectIdentifiers.sha256WithRSAEncryption);
        // byte[] sign = signer.sign("123456".getBytes(), key);
        // System.out.println(key);
        // System.out.println(Base64.encodeBase64String(sign));
    }
    
}
