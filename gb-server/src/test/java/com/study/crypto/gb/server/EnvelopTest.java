package com.study.crypto.gb.server;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientInfoGenerator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author Songjin
 * @since 2022-02-10 11:44
 */
class EnvelopTest {
    
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void testRsaEnvelop() throws Exception{
        String certpath = "D:/rsa.cer";
        byte[] data = "rootdsadsd".getBytes();
        CertificateFactory certificatefactory = CertificateFactory.getInstance("X.509", "BC");
        FileInputStream bais = new FileInputStream(certpath);
        X509Certificate cert = (X509Certificate) certificatefactory.generateCertificate(bais);
    
        CMSTypedData msg = new CMSProcessableByteArray(data);
        CMSEnvelopedDataGenerator edGen = new CMSEnvelopedDataGenerator();
        edGen.addRecipientInfoGenerator(new JceKeyTransRecipientInfoGenerator(cert).setProvider("BC"));
    
        CMSEnvelopedData ed = edGen.generate(msg, new JceCMSContentEncryptorBuilder(PKCSObjectIdentifiers.rc4).setProvider("BC").build());
        byte[] encoded = ed.toASN1Structure().getEncoded(ASN1Encoding.DER);
        System.out.println(Base64.encodeBase64String(encoded));
    }

    @Test
    void testRsaEnvelope() throws Exception {
        String keypath = "D:/rsa.jks";
        String passwd = "4766868153DC8D3B";
        String envelopedText = "MIIBvQYJKoZIhvcNAQcDoIIBrjCCAaoCAQAxggF8MIIBeAIBADBgMFIxCzAJBgNVBAYTAkNOMQ0wCwYDVQQKDARCSkNBMRgwFgYDVQQLDA9QdWJsaWMgVHJ1c3QgQ0ExGjAYBgNVBAMMEVB1YmxpYyBUcnVzdCBDQS0yAgobQAAAAAAABdq4MA0GCSqGSIb3DQEBAQUABIIBAApEptMOdSMto0wQCVNGqSw2NRSSMqDRJNxeaNz8qodJ54VoZSqWr2NZt4+8uNjv8C5fff6DhPlrzr0NTujB207CvwhGu/DDVt9ZanNej796xDnJ8yJpW7Tw2Rqfu4Xo/fNUMUOtSO61jsgE3PVC7f3SSYIw+YbrWEiZ9UngpnkU0T0HPFiS0sQjXhDky10CNxvl4zntSOUdxKsTPhoiUE834C9RxrlB+tU0Km7NDwP4aAK6h2/4/6CMhYzpAjwfrU11wJ126jAntucSvznv8gx0jHZNVqjEr5mx1xAjZcSn2E6oIin3A/pAeudrQoZXzQSrtEtsUXue+MTcdudsDDEwJQYJKoZIhvcNAQcBMAwGCCqGSIb3DQMEBQCACuY0dMZGSV9NoP8=";
        byte[] envelopedData = Base64.decodeBase64(envelopedText);
        CMSEnvelopedData ed = new CMSEnvelopedData(envelopedData);
        
        RecipientInformationStore recipients = ed.getRecipientInfos();
        
        Collection<RecipientInformation> c = recipients.getRecipients();
        Iterator<RecipientInformation> it = c.iterator();
        
        KeyStore ks = KeyStore.getInstance("jks");
        ks.load(new FileInputStream(keypath), passwd.toCharArray());
        
        String priKeyName = null;
        if (ks.aliases().hasMoreElements()) {
            priKeyName = ks.aliases().nextElement();
        }
        
        PrivateKey prikey = (PrivateKey) ks.getKey(priKeyName, passwd.toCharArray());
        
        if (it.hasNext()) {
            RecipientInformation recipient = it.next();
            byte[] result = recipient.getContent(new JceKeyTransEnvelopedRecipient(prikey).setProvider("BC"));
            System.out.println(new String(result));
        }
    }
    
}
