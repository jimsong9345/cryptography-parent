package com.study.crypto.basic.swxa;

import com.sansec.devicev4.util.BytesUtil;
import com.sansec.jce.provider.SwxaProvider;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.Security;
import java.util.Arrays;

/**
 * 三未信安加密机，加解密测试
 * @author Songjin
 * @since 2021-02-08 14:36
 */
@Slf4j
class SymmetryTest {
    
    private final static String[] ALGS_3DES = {"3DES/ECB/PKCS5PADDING", "3DES/ECB/PKCS5PADDING", "3DES/CBC/PKCS5PADDING", "3DES/CBC/PKCS5PADDING"};
    private final static String[] ALGS_AES  = {"AES/ECB/PKCS5PADDING",  "AES/CBC/PKCS5PADDING",  "AES/CFB/PKCS5PADDING",  "AES/OFB/PKCS5PADDING"};
    private final static String[] ALGS_SM1  = {"SM1/ECB/PKCS5PADDING",  "SM1/CBC/PKCS5PADDING",  "SM1/ECB/PKCS5PADDING",  "SM1/CBC/PKCS5PADDING"};
    private final static String[] ALGS_SM4  = {"SM4/ECB/PKCS5PADDING",  "SM4/CBC/PKCS5PADDING",  "SM4/ECB/PKCS5PADDING",  "SM4/CBC/PKCS5PADDING"};
    public static SwxaProvider swxaProvider = new SwxaProvider();
    static {
        Security.addProvider(swxaProvider);
    }
    
    @Test
    void testSingle() throws Exception{
        KeyGenerator keyGenerator = KeyGenerator.getInstance("SM4", swxaProvider);
        keyGenerator.init(1 << 16);
        SecretKey secretKey = keyGenerator.generateKey();
        String ciphertextHex = "0B75EDFDE1B72BFE6A9D3F3DD236C66B";
        String transformation = "SM4/CBC/PKCS5PADDING";
        log.info("加密之后的密文: {}", ciphertextHex);
        Cipher decCipher = Cipher.getInstance(transformation, swxaProvider);
        decCipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decipher = decCipher.doFinal(BytesUtil.hex2bytes(ciphertextHex));
        if (decipher == null) {
            log.error("{} 解密过程报错", transformation);
        } else {
            log.info("解密之后的明文: {}", BytesUtil.bytes2hex(decipher));
        }
    } 
    
    /**
     * 对称加解密加解密
     */
    @Test
    void test01() throws Exception{
        boolean internalOr = true;
        String[] chooseAlgs = ALGS_SM4;
        int keySize = 128;
        if (internalOr) {
            // 如果使用内部密钥，keySize 为密钥索引，将索引值按位左移 16
            keySize = 1 << 16;
        }
        byte[] plain = "hello world".getBytes();
        for (String algs : chooseAlgs) {
            String[]  algs_     = algs.split("/");
            SecretKey secretKey = this.generateSecretKey(algs_[0], keySize);
            log.info("产生对称密钥: {}", secretKey);
            this.doEncDec(secretKey, algs, plain);
        }
    }
    
    /**
     * 加解密
     * @param secretKey 对称密钥
     * @param transformation transformation
     * @param plain 原文
     */
    private void doEncDec(SecretKey secretKey, String transformation, byte[] plain) throws Exception {
        log.info("------ {} External key operation ------", transformation);
        Cipher encCipher = Cipher.getInstance(transformation, swxaProvider);
        encCipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] ciphertext = encCipher.doFinal(plain);
        if (ciphertext == null) {
            log.error("{} 加密过程报错!", transformation);
        } else {
            log.info("加密之后的密文: {}", BytesUtil.bytes2hex(ciphertext));
            Cipher decCipher = Cipher.getInstance(transformation, swxaProvider);
            decCipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] decipher = decCipher.doFinal(ciphertext);
            if (decipher == null) {
                log.error("{} 解密过程报错", transformation);
            } else {
                log.info("解密之后的明文: {}", BytesUtil.bytes2hex(decipher));
                log.info("比较解密结果: {}", Arrays.equals(plain, decipher));
            }
        }
        System.out.println();
    }
    
    /**
     * 生成对称密钥，只能产生外部密钥，内部密钥得通过管理界面生成
     * @param alg 算法名称
     * @param keySize 密钥大小
     * <ul>
     *     <li>密钥长度：外部密钥，长度</li>
     *     <li>密钥索引：内部密钥，索引值左移16位，例一号对称密钥，1<<16</li>
     * </ul>
     * @return SecretKey
     */
    private SecretKey generateSecretKey(String alg, int keySize) throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(alg, swxaProvider);
        keyGenerator.init(keySize);
        SecretKey key = keyGenerator.generateKey();
        if (key == null) {
            log.warn("产生对称密钥异常.");
        } else {
            log.info("产生对称密钥成功.");
            log.info("Key: {}", BytesUtil.bytes2hex(key.getEncoded()));
        }
        return key;
    }
    
}
