package com.study.crypto.signer;

import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

/**
 * RSA 签名验签
 * @author Songjin
 * @since 2021-01-03 15:11
 */
public class RSASigner extends Signer {
   
    @Override
    public byte[] sign(byte[] inData, PrivateKey privateKey) throws GeneralSecurityException {
        Signature signer = Signature.getInstance(SIGN_ALGO_SHA256_RSA, BC);
        signer.initSign(privateKey);
        signer.update(inData);
        return signer.sign();
    }
    
    @Override
    public boolean verify(byte[] inData, byte[] signature, PublicKey publicKey) throws GeneralSecurityException {
        Signature signer = Signature.getInstance(SIGN_ALGO_SHA256_RSA, BC);
        signer.initVerify(publicKey);
        signer.update(inData);
        return signer.verify(signature);
    }
}
