package com.study.crypto.signer;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;

/**
 * @author Songjin
 * @since 2021-01-03 15:31
 */
public class SignerFactory {
    
    /**
     * 产生签名者实例
     * @param algorithm  签名算法标识
     * @return 签名者
     */
    public static Signer produce(ASN1ObjectIdentifier algorithm) {
        if (GMObjectIdentifiers.sm2sign_with_sm3.equals(algorithm)) {
            return new JdkSM2Signer();
        } else if (X9ObjectIdentifiers.ecdsa_with_SHA256.equals(algorithm)) {
            return new ECDSASigner();
        } else if (PKCSObjectIdentifiers.sha256WithRSAEncryption.equals(algorithm)) {
            return new RSASigner();
        } else if (GMObjectIdentifiers.sm2sign.equals(algorithm)) {
            // return signer4;
            throw new IllegalArgumentException("不支持的签名算法标识: " + algorithm);
        } else {
            throw new IllegalArgumentException("不支持的签名算法标识: " + algorithm);
        }
    }
    
}
