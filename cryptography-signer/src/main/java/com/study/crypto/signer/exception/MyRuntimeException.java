package com.study.crypto.signer.exception;

/**
 * @author Songjin
 * @since 2022-01-28 15:45
 */
public class MyRuntimeException extends RuntimeException {
    
    public MyRuntimeException(String message) {
        super(message);
    }

    public MyRuntimeException(Throwable cause) {
        super(cause);
    }

    public MyRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
