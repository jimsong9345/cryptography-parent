package com.study.crypto.signer;

import org.bouncycastle.crypto.CryptoException;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

/**
 * ecc 签名验签
 * @author Songjin
 * @since 2021-01-03 15:26
 */
public class ECDSASigner extends Signer {
    
    @Override
    public byte[] sign(byte[] inData, PrivateKey privateKey) throws IOException, CryptoException, GeneralSecurityException {
        Signature signer = Signature.getInstance(SIGN_ALGO_SHA256_ECDSA, BC);
        signer.initSign(privateKey);
        signer.update(inData);
        return signer.sign();
    }
    
    @Override
    public boolean verify(byte[] inData, byte[] signature, PublicKey publicKey) throws GeneralSecurityException {
        Signature signer = Signature.getInstance(SIGN_ALGO_SHA256_ECDSA, BC);
        signer.initVerify(publicKey);
        signer.update(inData);
        return signer.verify(signature);
    }
}
