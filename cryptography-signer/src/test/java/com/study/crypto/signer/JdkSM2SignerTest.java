package com.study.crypto.signer;

import com.study.crypto.utils.KeyUtils;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @author Songjin
 * @since 2022-04-25 13:13
 */
class JdkSM2SignerTest {
    
    /**
     * 验签测试
     */
    @Test
    void testVerify() throws Exception{
        String certBase64 = "MIIDijCCAy+gAwIBAgIINKPuix42SbUwDAYIKoEcz1UBg3UFADCBgjELMAkGA1UEBhMCQ04xEjAQBgNVBAgMCUd1YW5nZG9uZzERMA8GA1UEBwwIU2hlbnpoZW4xJzAlBgNVBAoMHlNoZW5aaGVuIENlcnRpZmljYXRlIEF1dGhvcml0eTENMAsGA1UECwwEc3pjYTEUMBIGA1UEAwwLU1pDQSBTTTIgQ0EwHhcNMTkxMjA5MDIwNDA1WhcNMjIxMjA4MDIwNDA1WjB9MQswCQYDVQQGEwJDTjESMBAGA1UECAwJ5bm/5Lic55yBMRIwEAYDVQQHDAnmt7HlnLPluIIxETAPBgNVBAoMCOa3seWcs0NBMRswGQYDVQQLDBI2MTAxMjIxOTg1MTAxNjQzMjExFjAUBgNVBAMMDea1i+ivleWFrOWPuDEwggEzMIHsBgcqhkjOPQIBMIHgAgEBMCwGByqGSM49AQECIQD////+/////////////////////wAAAAD//////////zBEBCD////+/////////////////////wAAAAD//////////AQgKOn6np2fXjRNWp5Lz2UJp/OXifUVq4+S3by9QU2UDpMEQQQyxK4sHxmBGV+ZBEZqOcmUj+MLv/JmC+FxWkWJM0x0x7w3NqL09necWb3O42tpIVPQqYd8xipHQALfMuUhOfCgAiEA/////v///////////////3ID32shxgUrU7v0CTnVQSMCAQEDQgAEqh63DDE4YSmveUl/tpVhjBS9QoKOm2e77C/mI5uqicdUV3810yhe0LYsp2AzZJeqp/UZTlg/Bi2MoNJxTg7BdqOBtDCBsTAfBgNVHSMEGDAWgBRF6jeNcj+1pgRvwyrJrd3u5stOjTAgBggqVgsHg8zpfQQUDBI2MTAxMjIxOTg1MTAxNjQzMjEwLAYDVR0fBCUwIzAhoB+gHYYbaHR0cDovLzEyNy4wLjAuMS9jcmwxMzIuY3JsMBIGCCpWCweDzOoQBAYMBExMS0owCwYDVR0PBAQDAgbAMB0GA1UdDgQWBBSzBXTBWiO0Wu7b0TDY0d2eEgm2xjAMBggqgRzPVQGDdQUAA0cAMEQCIDqp4ad+bWs+4MwL96fPj+x0iFVoJ6zVmgdqzdlK3yZ8AiAdn6Dvu0+ZsNb0dwXS1rTdv5vnHbWI5QwtnhXn6NK/Nw==";
        Certificate certificate = Certificate.getInstance(Base64.decode(certBase64));
        JdkSM2Signer signer = new JdkSM2Signer();
        byte[] inData = "123".getBytes();
        String signHex = "3045022100e512e4040e46100a383a43d7ff65a3fc3ffcac1c3192a9d4296dbdfbad0fb9df02202a3fe7e2d13d1bbffa8bb6cf4cc35c26423d5bbbb0ffd5424a11474990014419";
        byte[] sign = Hex.decode(signHex);
        boolean verify = signer.verify(inData, sign, certificate);
        System.out.println(verify);
    }
    
    /**
     * 签名验签测试
     */
    @Test
    void testSignAndVerify2() throws Exception{
        byte[]     privateKeyBytes = FileUtils.readFileToByteArray(new File("target/privateKey.dat"));
        byte[]     publicKeyBytes  = FileUtils.readFileToByteArray(new File("target/publicKey.dat"));
        PrivateKey privateKey      = KeyUtils.convertPrivateKey(privateKeyBytes);
        PublicKey publicKey       = KeyUtils.convertPublicKey(publicKeyBytes);
        JdkSM2Signer signer = new JdkSM2Signer();
        byte[] inData = "123".getBytes();
        byte[] sign  = signer.sign(inData, privateKey);
        System.out.println(Hex.toHexString(sign));
        
        HashedSM2SignerTest signer1 = new HashedSM2SignerTest();
        // boolean   verify  = signer1.verify(inData, sign, publicKey);
        // System.out.println("verify: " + verify);
    }
    
    @Test
    void testSM3WithSM2_() throws Exception{
        String keyHex = "4AFAB22DBCD45251711359D22662134DFE69379F9A67081AEC33FF4F3EB6B9F03BE809C4687A0F194761055801BD5F20621126B7272F64388BBE93A8DD6A0CCD234DE3EF0E55BEF55BD9B49D00DF4867CB0F429BFB0871694BAE86C02121DFE6";
        byte[] bytes = Hex.decode(keyHex);
        byte[] privateKeyBytes = ByteUtils.subArray(bytes, 64);
        PrivateKey privateKey = KeyUtils.convertPrivateKey(privateKeyBytes);
        
        byte[] plaintext = "123456".getBytes();
        JdkSM2Signer signer = new JdkSM2Signer();
        byte[] sign = signer.sign(plaintext, privateKey);
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64String(sign));
    }
    
}
