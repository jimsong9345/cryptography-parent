package com.study.crypto.signer;

import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.security.*;

/**
 * RSA 算法测试类
 * @author Songjin
 * @since 2022-07-08 15:42
 */
@Slf4j
class RSASignerTest {
    
    static {
        Security.addProvider(new BouncyCastleProvider());
    }
    
    @Test
    void testGenerateKeyPair() throws Exception{
        // 生成密钥对
        RSAKeyPairGenerator keyPairGenerator = new RSAKeyPairGenerator();
        BigInteger publicExponent = BigInteger.valueOf(3);
        SecureRandom random = new SecureRandom();
        RSAKeyGenerationParameters parameters = new RSAKeyGenerationParameters(publicExponent, random, 1542, 25);
        // 初始化参数
        keyPairGenerator.init(parameters);
        AsymmetricCipherKeyPair keyPair = keyPairGenerator.generateKeyPair();
        // 公钥、私钥
        AsymmetricKeyParameter publicKey = keyPair.getPublic();
        AsymmetricKeyParameter privateKey = keyPair.getPrivate();
        log.info("公钥: {}", publicKey);
        log.info("私钥: {}", privateKey);
    
        KeyPairGenerator keyPairGenerator1 = KeyPairGenerator.getInstance("RSA", "BC");
        keyPairGenerator1.initialize(4096);
        KeyPair keyPair1 = keyPairGenerator1.genKeyPair();
        PrivateKey privateKey1 = keyPair1.getPrivate();
        PublicKey publicKey1 = keyPair1.getPublic();
        log.info("公钥: {}", privateKey1);
        log.info("私钥: {}", publicKey1);
    }
    
}
