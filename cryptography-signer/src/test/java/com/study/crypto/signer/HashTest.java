package com.study.crypto.signer;

import com.study.crypto.utils.ByteUtility;
import com.study.crypto.utils.CertUtils;
import com.study.crypto.utils.DigestUtil;
import com.study.crypto.utils.KeyUtils;
import com.study.crypto.utils.digest.SM3PublicKeyDigest;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.math.BigInteger;

/**
 * 摘要计算测试
 * @author Songjin
 * @since 2022-03-29 9:57
 */
class HashTest {
    
    /**
     * 带公钥计算 sm3 摘要
     */
    @Test
    void testHashSM3WithPublicKey() throws Exception{
        byte[] plaintext = "123".getBytes();
        byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        Certificate certificate = CertUtils.rebuildCertificate(certBytes);
        byte[] publicKeyBytes = certificate.getSubjectPublicKeyInfo().getPublicKeyData().getBytes();
        ECPoint publicPoint = KeyUtils.sm2p256v1.getCurve().decodePoint(publicKeyBytes);
        BigInteger affineX = publicPoint.getXCoord().toBigInteger();
        BigInteger affineY = publicPoint.getYCoord().toBigInteger();
        
        byte[] hash = new byte[32];
        com.study.crypto.utils.digest.SM3Digest digest = new com.study.crypto.utils.digest.SM3Digest();
        digest.addId(affineX, affineY, Signer.SM2_ID);
        digest.update(plaintext, 0, plaintext.length);
        digest.doFinal(hash, 0);
        System.out.println(Hex.toHexString(hash));
    }
    
    @Test
    void testHashSM3WithPublicKey2() throws Exception{
        byte[] plaintext = "12345678901".getBytes();
        byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        Certificate certificate = CertUtils.rebuildCertificate(certBytes);
        byte[] publicKeyBytes = certificate.getSubjectPublicKeyInfo().getPublicKeyData().getBytes();
        
        byte[] hash = new byte[32];
        SM3PublicKeyDigest digest = new SM3PublicKeyDigest(publicKeyBytes);
        digest.update(plaintext, 0, plaintext.length);
        digest.doFinal(hash, 0);
        System.out.println(Hex.toHexString(hash));
        System.out.println(Base64.encodeBase64String(hash));
    }

    @Test
    void testHashSM3WithPublicKey3() throws Exception{
        String text = "343739202020333031353C5472616E446174613E3C486561643E3C42616E6B436F64653E424F433C2F42616E6B436F64653E3C5472616E446174653E323032322D30372D30353C2F5472616E446174653E3C5472616E54696D653E3138343730313C2F5472616E54696D653E3C5A6F6E653E3531303030303C2F5A6F6E653E3C42724E6F3E31373836343C2F42724E6F3E3C5472616E436F64653E333031353C2F5472616E436F64653E3C4368616E6E656C3E30313C2F4368616E6E656C3E3C5472616E734E6F3E323032323037303535353132303839343C2F5472616E734E6F3E3C2F486561643E3C426F64793E3C506F6C6963794E6F3E3032303439303030303030303030313C2F506F6C6963794E6F3E3C4170706E744E616D653EB2E2CAD4D4B13C2F4170706E744E616D653E3C4170706E7447656E6465723E323C2F4170706E7447656E6465723E3C4170706E7443657274547970653E303C2F4170706E7443657274547970653E3C4170706E7443657274436F64653E3331303130343239323932393030393939393C2F4170706E7443657274436F64653E3C42757369547970653E593C2F42757369547970653E3C4163636F756E744E6F3E3132333435343332313C2F4163636F756E744E6F3E3C2F426F64793E3C2F5472616E446174613E";
        String signcertB64 = "MIIC5TCCAomgAwIBAgIFIFRjACQwDAYIKoEcz1UBg3UFADBdMQswCQYDVQQGEwJDTjEwMC4GA1UECgwnQ2hpbmEgRmluYW5jaWFsIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRwwGgYDVQQDDBNDRkNBIFRFU1QgU00yIE9DQTExMB4XDTIyMDQxMTAxNTgwN1oXDTI3MDQxMTAxNTgwN1owgYcxCzAJBgNVBAYTAkNOMRwwGgYDVQQKDBNDRkNBIFRFU1QgT0NBMTEgU00yMQ4wDAYDVQQLDAVMU0NDQjEZMBcGA1UECwwQT3JnYW5pemF0aW9uYWwtMjEvMC0GA1UEAwwmMDUxQOWbveWvhuaUuemAoEBOOTE1MTAwMDA3MDkwMTcwMzdLQDIwWTATBgcqhkjOPQIBBggqgRzPVQGCLQNCAARBx6sYjhWqlFt92mIdwZeHMOuqVX6RLcDtNFsZ/mFDtzZVpEnm3ydClYVnMlBgIuoKxKRTrj6f00jkNjPoZyW8o4IBBzCCAQMwHwYDVR0jBBgwFoAUvqZ+TT18j6BV5sEvCS4sIEOzQn8wDAYDVR0TAQH/BAIwADBIBgNVHSAEQTA/MD0GCGCBHIbvKgECMDEwLwYIKwYBBQUHAgEWI2h0dHA6Ly93d3cuY2ZjYS5jb20uY24vdXMvdXMtMTUuaHRtMDoGA1UdHwQzMDEwL6AtoCuGKWh0dHA6Ly8yMTAuNzQuNDIuMy9PQ0ExMS9TTTIvY3JsMTgwMDAuY3JsMA4GA1UdDwEB/wQEAwIGwDAdBgNVHQ4EFgQUdN8rFGOqFAsDuP5xywf0Zu/Ij7wwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMAwGCCqBHM9VAYN1BQADSAAwRQIgHzXgI7o5gwklp4B0ql//kuqg/iLXCSvDXkfDVxkiuwACIQDENNRA98pb8QbpIT9zuel2uwgOrcw/uFw/+3v5NmhW6A==";
        byte[] plaintext = Hex.decode(text);
        byte[] certBytes = Base64.decodeBase64(signcertB64);
        Certificate certificate = CertUtils.rebuildCertificate(certBytes);
        byte[] publicKeyBytes = certificate.getSubjectPublicKeyInfo().getPublicKeyData().getBytes();

        byte[] hash = new byte[32];
        SM3PublicKeyDigest digest = new SM3PublicKeyDigest(publicKeyBytes);
        digest.update(plaintext, 0, plaintext.length);
        digest.doFinal(hash, 0);
        System.out.println(Hex.toHexString(hash));
        System.out.println(Base64.encodeBase64String(hash));
    }

    @Test
    void testHashSM3WithPublicKey4() throws Exception{
        byte[] userId = "1234567812345678".getBytes();
        byte[] entl = ByteUtils.subArray(ByteUtility.intToByte(userId.length * 8), 2);
        byte[] a = Hex.decodeStrict("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC");
        byte[] b = Hex.decodeStrict("28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93");
        byte[] Gx = Hex.decodeStrict("32C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7");
        byte[] Gy = Hex.decodeStrict("BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0");
        byte[] userX = Hex.decodeStrict("E2A0EAE784EE407D6F56A0BD3267D373EAC550E7ACE7588D1E95DDB28FA64D0E");
        byte[] userY = Hex.decodeStrict("F78F7721589D64B53D4DFAA66C3C97851C3C1F2A11AEEBE491D10102FC081DEF");
        byte[] Z = new byte[32];
        SM3Digest digest = new SM3Digest();
        digest.update(entl, 0, entl.length);
        digest.update(userId, 0, userId.length);
        digest.update(a, 0, a.length);
        digest.update(b, 0, b.length);
        digest.update(Gx, 0, Gx.length);
        digest.update(Gy, 0, Gy.length);
        digest.update(userX, 0, userX.length);
        digest.update(userY, 0, userY.length);
        digest.doFinal(Z, 0);
        System.out.println("Z: " + Hex.toHexString(Z));

        digest.reset();
        byte[] hash = new byte[32];
        byte[] plaintext = "12345678901".getBytes();
        digest.update(Z, 0, Z.length);
        digest.update(plaintext, 0, plaintext.length);
        digest.doFinal(hash, 0);
        System.out.println("hash: " + Hex.toHexString(hash));
    }


    @Test
    void testHashSM3Oid() {
        byte[] bytes = DigestUtil.sm3("1234567890".getBytes());
        System.out.println(Hex.toHexString(bytes));
        String id = GMObjectIdentifiers.sm2p256v1.getId();
        System.out.println("1.2.156.10197.1.301".equals(id));
    }
    
    @Test
    void testHashMD5() throws Exception{
        byte[] md5Bytes = DigestUtils.md5("123456\n".getBytes());
        System.out.println("MD5摘要值: " + Hex.toHexString(md5Bytes));
        byte[] md5Bytes2 = DigestUtils.md5("hello world".getBytes());
        System.out.println("MD5摘要值: " + Hex.toHexString(md5Bytes2));
    }
    
    @Test
    void testHashSHA() throws Exception{
        String plaintext = "123456123456";
        
        String sha256Hex = DigestUtils.sha256Hex(plaintext);
        System.out.println("SHA256摘要值: " + sha256Hex);
    
        String sha384Hex = DigestUtils.sha384Hex(plaintext);
        System.out.println("SHA384摘要值: " + sha384Hex);
    
        String sha512Hex = DigestUtils.sha512Hex(plaintext);
        System.out.println("SHA512摘要值: " + sha512Hex);
    }
    
    
}
