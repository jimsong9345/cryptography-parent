package com.study.crypto.signer;

import com.study.crypto.utils.digest.SM3PublicKeyDigest;
import com.study.crypto.utils.CertUtils;
import com.study.crypto.utils.KeyUtils;
import com.study.crypto.signer.HashedSM2Signer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @author Songjin
 * @since 2022-04-25 14:45
 */
@Slf4j
class HashedSM2SignerTest {
    
    String keyStorageHex = "e2a0eae784ee407d6f56a0bd3267d373eac550e7ace7588d1e95ddb28fa64d0ef78f7721589d64b53d4dfaa66c3c97851c3c1f2a11aeebe491d10102fc081defe921c7baf041fe4c44b0fd0bb0b7e4a22a7a41d92e08c10dfa73aefcb313b00d";
    
    /**
     * 传入摘要值，计算签名值
     */
    @Test
    void testSign() throws Exception{
        byte[] plaintext = "123".getBytes();
        
        byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        Certificate certificate = CertUtils.convertCertificate(certBytes);
        SM3PublicKeyDigest digest = new SM3PublicKeyDigest(certificate);
        
        // 1.计算摘要，带公钥计算
        byte[] hash = new byte[32];
        digest.update(plaintext, 0, plaintext.length);
        digest.doFinal(hash, 0);
    
        // 2.使用私钥计算摘要签名值
        byte[] keyStorage = Hex.decode(keyStorageHex);
        PrivateKey privateKey = KeyUtils.convertPrivateKey(ByteUtils.subArray(keyStorage, 64));
        AsymmetricKeyParameter keyParameter = ECUtil.generatePrivateKeyParameter(privateKey);
        HashedSM2Signer signer = new HashedSM2Signer();
        signer.initHashed(true, keyParameter);
        signer.updateHashed(hash);
        byte[] signVal = signer.generateHashedSignature();
        log.info("摘要值: {}", Base64.encodeBase64String(hash));
        log.info("摘要签名: {}", Base64.encodeBase64String(signVal));
    }
    
    @Test
    void testVerify() throws Exception{
        String hashB64 = "lnStZA7RVqRAeiDb/19fPRtJfHcZsboQYZpebyKYs2A=";
        String signB64 = "MEUCIAVbP4NhyT53cyEI06sJLCzzm5jbMcCDVhxpTsqgynrrAiEAuCtuPOfU968O1l+Ykzr9MgDxbzkOg83pH8IwMz6n7d4=";
        
        byte[] keyStorage = Hex.decode(keyStorageHex);
        byte[] publicKeyBytes = ByteUtils.subArray(keyStorage, 0, 64);
        PublicKey publicKey = KeyUtils.convertPublicKey(publicKeyBytes);
        AsymmetricKeyParameter keyParameter = ECUtil.generatePublicKeyParameter(publicKey);
        HashedSM2Signer signer = new HashedSM2Signer();
        signer.initHashed(false, keyParameter);
        signer.updateHashed(Base64.decodeBase64(hashB64));
        boolean verify = signer.verifyHashedSignature(Base64.decodeBase64(signB64));
        log.info("验证: {}", verify);
    
    }
    
    
}
