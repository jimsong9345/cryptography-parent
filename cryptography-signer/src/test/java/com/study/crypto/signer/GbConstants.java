package com.study.crypto.signer;

import java.util.HashMap;
import java.util.Map;

/**
 * 国办备案相关常量
 * @author Songjin
 * @since 2019/10/28 11:41
 */
public class GbConstants {
	
	public static final Map<String, String> SEAL_TYPE_MAP = new HashMap<String, String>();
	static {
		SEAL_TYPE_MAP.put("01", "法定名称章");
		SEAL_TYPE_MAP.put("02", "财务专用章");
		SEAL_TYPE_MAP.put("03", "发票专用章");
		SEAL_TYPE_MAP.put("04", "合同专用章");
		SEAL_TYPE_MAP.put("05", "电子名章");
		SEAL_TYPE_MAP.put("06", "业务专用章");
	}
 
	/**
	 * 协议版本号 1.0
	 */
	public static final String VERSION = "1.0";
	/**
	 * 获取随机数接口GB，命令编码
	 */
	public static final String APPLY_RANDOM_GB = "applyRandom";
	/**
	 * 电子印章信息的备案协议，命令编码
	 */
	public static final String UPLOAD_RECORD_INFOS = "uploadRecordInfos";
	/**
	 * 电子印章信息的备案协议，命令编码
	 */
	public static final String UPDATE_PROTECT_KEY = "updateProtectKey";
	/**
	 * 上传电子印章撤销信息接口，命令编码
	 */
	public static final String REVOCATION_SEAL_INFO = "revocationSealInfo";
	/**
	 * 异步请求获取请求结果命令编码
	 * <ul>
	 *     <li>查询并下载公安下发数据接口</li>
	 * </ul>
	 */
	public static final String CHECK_RESULT = "checkResult";
	/**
	 * 赋码申请协议，命令编码
	 */
	public static final String APPLY_SEAL_CODE = "applySealCode";
	/**
	 * 确认回执，命令编码
	 */
	public static final String CONFIRM_RECEIPT = "confirmReceipt";
	/**
	 * 印章制作，命令编码
	 */
	public static final String SEALMAKING = "sealMaking";
	/**
	 * 印章下载，命令编码
	 */
	public static final String DOWNLOADSEAL = "downloadSeal";
	/**
	 * 业务类型编码，0 表示备案业务
	 */
	public static final String TASK_TYPE_CODE_UPLOAD_RECORD_INFOS = "0";
	/**
	 * 业务类型编码，1 表示保护密钥业务
	 */
	public static final String TASK_TYPE_CODE_UPDATE_PROTECT_KEY = "1";
	/**
	 * 业务类型编码，2 表示电子印章撤销业务
	 */
	public static final String TASK_TYPE_CODE_REVOCATION_SEAL_INFO = "2";
	/**
	 * 业务类型编码，3 申请电子印章唯一赋码业务
	 */
	public static final String TASK_TYPE_CODE_APPLY_SEAL_CODE = "3";
	/**
	 * 业务类型编码，6 印章制作业务
	 */
	public static final String TASK_TYPE_CODE_SEAL_MAKING_CODE = "6";
	/**
	 * 业务类型编码，7 印章下载业务
	 */
	public static final String TASK_TYPE_CODE_SEAL_DOWNLOAD_CODE = "7";
	/**
	 * 签名算法 oid 字符串，
	 */
	public static final String SIGN_ALGORITHM = "1.2.156.10197.1.501";
	
	// --以下常量为国办代理相关状态码--------------------------------------------------------------------------------
	/**
	 * 接口调用成功状态
	 */
	public static final String PROXY_SUCCESS = "10";
	/**
	 * 接口调用成功状态
	 */
	public static final String PROXY_SUCCESS_INNER = "0";
	/**
	 * 接口调用失败状态
	 */
	public static final String PROXY_FAIL_INNER = "-1";
	/**
	 * 印章制作成功
	 */
	public static final String MAKE_SEAL_SUCCESS = "印章制作成功";
	/**
	 * 印章制作失败
	 */
	public static final String MAKE_SEAL_FAIL = "印章制作失败";
	/**
	 * 下载印章成功
	 */
	public static final String DOWNLOAD_SEAL_SUCCESS = "下载印章成功";
	/**
	 * 印章不存在或还未新制印章
	 */
	public static final String SEAL_NOTEXSIT = "印章不存在或还未新制印章";

	// --以下常量为应用系统相关状态码--------------------------------------------------------------------------------
	/**
	 * Token类型-国办
	 */
	public static final String TOKEN_GB = "1";
	/**
	 * Token类型-CA
	 */
	public static final String TOKEN_CA = "2";
	/**
	 * 验证签名错误
	 */
	public static final String VERIFY_FAIL = "20101";
	/**
	 * 身份认证失败
	 */
	public static final String AUTH_FAIL = "20102";
	/**
	 * 缓存(memcache、redis)业务处理状态: 正在处理中
	 */
	public static final String CACHE_PROCESSING = "20211";
	/**
	 * 缓存(memcache、redis)业务处理状态: 业务已处理完
	 */
	public static final String CACHE_COMPLETE = "20212";
	
	/**
	 * redis 设置过期时间
	 */
	public static final int EXPIRE = 900;
	/**
	 * 数据为空
	 */
	public static final String NULL_ERROR = "20214";
}
