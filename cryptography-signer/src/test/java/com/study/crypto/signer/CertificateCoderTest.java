package com.study.crypto.signer;

import org.apache.commons.codec.binary.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;
  
/** 
 * 证书操作验证类 
 *  
 * @author <a href="mailto:zlex.dongliang@gmail.com">梁栋</a> 
 * @version 1.0 
 * @since 1.0 
 */  
class CertificateCoderTest {
    private final String certificatePath = "zlex.crt";
    private final String keyStorePath = "zlex.keystore";
    private final String keyStorePassword = "123456";
    private final String aliasPassword = "123456";
    private final String alias = "1";
  
    @Test
    void test() throws Exception {
        System.err.println("公钥加密——私钥解密");  
        String inputStr = "Ceritifcate";  
        byte[] data = inputStr.getBytes();  
  
        byte[] encrypt = CertificateCoder.encryptByPublicKey(data,  
                certificatePath);  
  
        byte[] decrypt = CertificateCoder.decryptByPrivateKey(encrypt,  
                keyStorePath, alias, keyStorePassword, aliasPassword);  
        String outputStr = new String(decrypt);  
  
        System.err.println("加密前: " + inputStr + "\n\r" + "解密后: " + outputStr);  
  
        // 验证数据一致
        Assertions.assertArrayEquals(data, decrypt);
  
        // 验证证书有效  
        Assertions.assertTrue(CertificateCoder.verifyCertificate(certificatePath));
  
    }  
  
    @Test
    void testSign() throws Exception {
        System.err.println("私钥加密——公钥解密");  
  
        String inputStr = "sign";  
        byte[] data = inputStr.getBytes();  
  
        byte[] encodedData = CertificateCoder.encryptByPrivateKey(data,  
                keyStorePath, keyStorePassword, alias, aliasPassword);  
  
        byte[] decodedData = CertificateCoder.decryptByPublicKey(encodedData,  
                certificatePath);  
  
        String outputStr = new String(decodedData);  
        System.err.println("加密前: " + inputStr + "\n\r" + "解密后: " + outputStr);
        Assertions.assertEquals(inputStr, outputStr);
  
        System.err.println("私钥签名——公钥验证签名");  
        // 产生签名  
        byte[] sign = CertificateCoder.sign(encodedData, keyStorePath, alias,  
                keyStorePassword, aliasPassword);  
        System.err.println("签名:\r" + Hex.encodeHexString(sign));  
  
        // 验证签名  
        boolean status = CertificateCoder.verify(encodedData, sign,  
                certificatePath);  
        System.err.println("状态:\r" + status);
        Assertions.assertTrue(status);
    }  
  
    @Test
    void testVerify() throws Exception {
        System.err.println("密钥库证书有效期验证");  
        boolean status = CertificateCoder.verifyCertificate(new Date(),  
                keyStorePath, keyStorePassword, alias);  
        System.err.println("证书状态:\r" + status);
        Assertions.assertTrue(status);
    }  
}  
