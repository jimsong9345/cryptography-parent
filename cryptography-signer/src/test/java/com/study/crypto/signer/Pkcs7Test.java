package com.study.crypto.signer;

import com.study.crypto.asn1.oid.CMSObjectIdentifiers;
import com.study.crypto.utils.ASN1Utils;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.cms.*;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.*;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.CollectionStore;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Songjin
 * @since 2021-02-09 14:12
 */
class Pkcs7Test {
    
    /**
     * 产生 CMS 签名数据结构
     * @param signingKey    签名密钥
     * @param signingCert   签名证书
     * @param plaintext     原文字节数组
     * @return cms签名结构
     */
    public static CMSSignedData createSignedData(PrivateKey signingKey, X509CertificateHolder signingCert, byte[] plaintext) throws GeneralSecurityException, IOException, CryptoException, CMSException {
        Signer signer = SignerFactory.produce(GMObjectIdentifiers.sm2sign_with_sm3);
        byte[] signedBytes = signer.sign(plaintext, signingKey);
        Certificate certificate = signingCert.toASN1Structure();
        // 封装 signerInfo 结构
        IssuerAndSerialNumber issuerSerial = new IssuerAndSerialNumber(certificate);
        SignerIdentifier sid = new SignerIdentifier(issuerSerial);
        AlgorithmIdentifier digAlgorithm = new AlgorithmIdentifier(GMObjectIdentifiers.hmac_sm3, DERNull.INSTANCE);
        AlgorithmIdentifier digEncryptionAlgorithm = new AlgorithmIdentifier(GMObjectIdentifiers.sm2sign, DERNull.INSTANCE);
        DEROctetString encryptedDigest = new DEROctetString(signedBytes);
        SignerInfo signerInfo = new SignerInfo(sid, digAlgorithm, (ASN1Set) null, digEncryptionAlgorithm, encryptedDigest, null);
        
        // 封装 signedData 结构
        ASN1EncodableVector signedDataVector = new ASN1EncodableVector();
        signedDataVector.add(new ASN1Integer(1));
        signedDataVector.add(ASN1Utils.createDerSetFromList(digAlgorithm));
        signedDataVector.add(new ContentInfo(CMSObjectIdentifiers.data, null));
        signedDataVector.add(new DERTaggedObject(false, 0, ASN1Utils.createDerSetFromList(certificate)));
        signedDataVector.add(new DERSet());
        signedDataVector.add(ASN1Utils.createDerSetFromList(signerInfo));
        DERSequence signedData_ = new DERSequence(signedDataVector);
        SignedData signedData = SignedData.getInstance(signedData_);
        
        // 封装外层 contentInfo 结构
        CMSTypedData contentInfo = new CMSProcessableByteArray(CMSObjectIdentifiers.data, plaintext);
        ContentInfo innerContentInfo = new ContentInfo(CMSObjectIdentifiers.signedData, signedData);
        return new CMSSignedData(contentInfo, innerContentInfo);
    }
    
    /**
     * 产生 CMS 签名数据结构
     * @param signingKey    签名密钥
     * @param signingCert   签名证书
     * @param plaintext     原文字节数组
     * @param encapsulate   是否压缩
     * @return cms签名结构
     */
    public static CMSSignedData createSignedData_(PrivateKey signingKey, X509CertificateHolder signingCert, byte[] plaintext, boolean encapsulate) throws OperatorCreationException, CMSException {
        JcaContentSignerBuilder sm3WithSM2 = new JcaContentSignerBuilder("SM3WithSM2");
        sm3WithSM2.setProvider(BouncyCastleProvider.PROVIDER_NAME);
        ContentSigner contentSigner = sm3WithSM2.build(signingKey);

        JcaDigestCalculatorProviderBuilder digestProviderBuilder = new JcaDigestCalculatorProviderBuilder();
        digestProviderBuilder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
        DigestCalculatorProvider digestCalcProvider = digestProviderBuilder.build();

        SignerInfoGeneratorBuilder signerInfoBuilder = new SignerInfoGeneratorBuilder(digestCalcProvider);
        SignerInfoGenerator signerInfoGenerator = signerInfoBuilder.build(contentSigner, signingCert);

        CollectionStore<X509CertificateHolder> certs = new CollectionStore<>(Collections.singletonList(signingCert));
        CMSTypedData typedMsg = new CMSProcessableByteArray(CMSObjectIdentifiers.data, plaintext);

        CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
        generator.addSignerInfoGenerator(signerInfoGenerator);
        generator.addCertificates(certs);
        return generator.generate(typedMsg, encapsulate);
    }
    
    @Test
    void testPkcs7Verify() throws Exception{
        byte[] bytes = FileUtils.readFileToByteArray(new File("D:/simple-p7.der"));
        CMSSignedData signedData = new CMSSignedData(bytes);
        CollectionStore<X509CertificateHolder> certificates_ = (CollectionStore<X509CertificateHolder>) signedData.getCertificates();
        List<Certificate> certificates = new ArrayList<>();
        certificates_.forEach(cert -> {
            certificates.add(cert.toASN1Structure());
        });
        System.out.println(certificates);
    }
    
    @Test
    void testPkcs7Sign() throws Exception{
        // String dir = "../certificate-server/src/main/resources/certs";
        // byte[] certBytes = FileUtils.readFileToByteArray(new File(dir, "sm2-server-cert.cer"));
        // String privateKeyPem = FileUtils.readFileToString(new File(dir, "sm2-server-key.keystore"), StandardCharsets.UTF_8);
        // byte[] certBytes = FileUtils.readFileToByteArray(new File(dir, "sm2-server-cert.cer"));
        // String privateKeyPem = FileUtils.readFileToString(new File(dir, "sm2-server-key.keystore"), StandardCharsets.UTF_8);
        //
        // Certificate cert = Certificate.getInstance(certBytes);
        // X509CertificateHolder certHolder = new X509CertificateHolder(cert);
        // PrivateKey privateKey = KeyUtils.privateKey(privateKeyPem);
        //
        // CMSSignedData signedData = createSignedData(privateKey, certHolder, "HelloWorld".getBytes());
        // byte[] ders = signedData.getEncoded(ASN1Encoding.DER);
        // FileUtils.writeByteArrayToFile(new File("D:/simple-p7.der"), ders);
        //
        // SM2CMSSigner signer = (SM2CMSSigner) SignerFactory.produce(CMSObjectIdentifiers.signedData, cert);
        // byte[] ders_ = signer.sign("HelloWorld".getBytes(), privateKey);
        // FileUtils.writeByteArrayToFile(new File("D:/simple-p7_.der"), ders_);
    }
    
}
