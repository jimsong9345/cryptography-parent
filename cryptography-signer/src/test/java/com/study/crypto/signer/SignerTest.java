package com.study.crypto.signer;

import com.study.crypto.utils.CertUtils;
import com.study.crypto.utils.KeyUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.pqc.legacy.math.linearalgebra.BigEndianConversions;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @author Songjin
 * @since 2021-01-15 14:45
 */
class SignerTest {
	
	@Test
	void testPkcs7Sign() throws Exception{
		byte[] keystore = FileUtils.readFileToByteArray(new File("D:/production.keystore"));
		byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
		byte[] privateKeyBytes = ByteUtils.subArray(keystore, 64);
		
		PrivateKey privateKey = KeyUtils.convertPrivateKey(privateKeyBytes);
		Certificate certificate = Certificate.getInstance(certBytes);
		
		PKCS7GmSigner signer = new PKCS7GmSigner(certificate, null, null);
		byte[]        sign   = signer.sign("123".getBytes(), privateKey);
		System.out.println(Hex.toHexString(sign));
	}
	
	@Test
	void testSM2Sign() throws Exception{
		byte[] keystore = FileUtils.readFileToByteArray(new File("D:/production.keystore"));
		byte[] publicKeyBytes = ByteUtils.concatenate(new byte[]{4}, ByteUtils.subArray(keystore, 0, 64));
		byte[] privateKeyBytes = ByteUtils.subArray(keystore, 64);
		
		ECPoint publicPoint = KeyUtils.sm2p256v1.getCurve().decodePoint(publicKeyBytes);
		BigInteger affineX = publicPoint.getXCoord().toBigInteger();
		BigInteger affineY = publicPoint.getYCoord().toBigInteger();
		
		byte[] plaintext = "123".getBytes();
		
		byte[] hash = new byte[32];
		com.study.crypto.utils.digest.SM3Digest digest = new com.study.crypto.utils.digest.SM3Digest();
		digest.addId(affineX, affineY, Signer.SM2_ID);
		digest.update(plaintext, 0, plaintext.length);
		digest.doFinal(hash, 0);
		
		PrivateKey privateKey = KeyUtils.convertPrivateKey(privateKeyBytes);
		AsymmetricKeyParameter privateKeyParameter = ECUtil.generatePrivateKeyParameter(privateKey);
		
		HashedSM2Signer signer = new HashedSM2Signer();
		signer.initHashed(true, privateKeyParameter);
		signer.updateHashed(hash);
		byte[] sm2SigValue = signer.generateHashedSignature();
		System.out.println(Hex.toHexString(sm2SigValue));
	}
	
	@Test
	void testSM2SignVerify() throws Exception{
		byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
		String signValueHex = "304402204e8343f0f09f4efeb2e9ebba128b4350e46da20db4afed71920cb017efd0c78402207cdc6b7a4c42fa1d79e433c028eb6669af1d160c9c0bf945303001a58948583e";
		Certificate certificate = CertUtils.convertCertificate(certBytes);
		byte[] publicKeyBytes = certificate.getSubjectPublicKeyInfo().getPublicKeyData().getBytes();
		if (publicKeyBytes.length == 65) {
			publicKeyBytes = ByteUtils.subArray(publicKeyBytes, 1);
		}
		PublicKey publicKey = KeyUtils.convertPublicKey(publicKeyBytes);
		AsymmetricKeyParameter publicKeyParameter = ECUtil.generatePublicKeyParameter(publicKey);
		
		HashedSM2Signer signer = new HashedSM2Signer();
		signer.initHashed(false, publicKeyParameter);
		String hash = "9674ad640ed156a4407a20dbff5f5f3d1b497c7719b1ba10619a5e6f2298b360";
		signer.updateHashed(Hex.decode(hash));
		boolean verify = signer.verifyHashedSignature(Hex.decode(signValueHex));
		System.out.println(verify);
		
		Signer produce = SignerFactory.produce(GMObjectIdentifiers.sm2sign_with_sm3);
		boolean verify1 = produce.verify("123".getBytes(), Hex.decode(signValueHex), publicKey);
		System.out.println(verify1);
	}
	
	/**
	 * 签名验签测试
	 */
	@Test
	void testSignAndVerify() throws Exception{
		// 从密钥对文件中获取公钥、私钥
		// byte[] keybytes = FileUtils.readFileToByteArray(new File("D:/11010800016981.keystore"));
		byte[] keybytes = Hex.decode("28552B1051671BD19CFD038D327E29E89FF2FB9EA6840FF450276E88530FA179518AF5E540E18154D62DDDEEB96B176B8D8729CE8FA025F35435E8381251FC0F50F6B2AE18CEBDEBDA92DEBE893BDF09E0CC71B5B32E03B983016556C01932A8");
		byte[] privateKeyBytes = ByteUtils.subArray(keybytes, 64);
		byte[] publicKeyBytes = ByteUtils.subArray(keybytes, 0, 64);
		
		// // 从私钥字符串、公钥证书中分别获取私钥、公钥
		// byte[] privateKeyBytes = Base64.decodeBase64("nVe3Zjx/AACuMuYUnBz9uo4DwKPDqGJ8yo5X8MN6B60=");
		// String certHex = "308201E43082018AA003020102020A1A100000000000000007300A06082A811CCF550183753043310B300906035504061302434E310D300B060355040A0C04424A4341310D300B060355040B0C04424A43413116301406035504030C0D534D3220536572766572204341301E170D3231303532383039303632305A170D3232303532383039303632305A3049310B300906035504061302434E3121301F060355040A0C18393131313030303031303131303136363032E4BAACE4B89C3117301506035504030C0E32313131303430303030303136353059301306072A8648CE3D020106082A811CCF5501822D0342000428552B1051671BD19CFD038D327E29E89FF2FB9EA6840FF450276E88530FA179518AF5E540E18154D62DDDEEB96B176B8D8729CE8FA025F35435E8381251FC0FA360305E300C0603551D130101FF04023000300E0603551D0F0101FF040403020338301F0603551D23041830168014F2D6736DD94CCF3FABE95D5B3BDDB1EF1824EA34301D0603551D0E04160414AC8A2196ADA6EED2F90410397A6A8216EB794771300A06082A811CCF550183750348003045022036D2902BDBE2F0185F52F4244757093B8BDA0BC713397DBB9AC39FC1FDF0FB40022100BEFD0F8040BD40B828A1FE92EAC52556F60065572C7292E234990DE66971BB42";
		// byte[] publicKeyBytes = KeyUtils.obtainPublicKeyBytes(Hex.decode(certHex));
		PrivateKey privateKey      = KeyUtils.convertPrivateKey(privateKeyBytes);
		PublicKey  publicKey       = KeyUtils.convertPublicKey(publicKeyBytes);
		Signer signer = SignerFactory.produce(GMObjectIdentifiers.sm2sign_with_sm3);
		byte[] inData = "456".getBytes();
		byte[] signedData = signer.sign(inData, privateKey);
		System.out.println(Hex.toHexString(signedData));
		boolean verify = signer.verify(inData, signedData, publicKey);
		System.out.println(verify);
	}
	
	@Test
	void testSM3WithSM2() throws Exception {
		String     keyHex          = "4AFAB22DBCD45251711359D22662134DFE69379F9A67081AEC33FF4F3EB6B9F03BE809C4687A0F194761055801BD5F20621126B7272F64388BBE93A8DD6A0CCD234DE3EF0E55BEF55BD9B49D00DF4867CB0F429BFB0871694BAE86C02121DFE6";
		byte[]     bytes           = Hex.decode(keyHex);
		byte[]     publicKeyBytes  = ByteUtils.subArray(bytes, 0, 64);
		byte[]     privateKeyBytes = ByteUtils.subArray(bytes, 64);
		PublicKey  publicKey       = KeyUtils.convertPublicKey(publicKeyBytes);
		PrivateKey privateKey      = KeyUtils.convertPrivateKey(privateKeyBytes);
		AsymmetricKeyParameter privateKeyParameter = ECUtil.generatePrivateKeyParameter(privateKey);
		
		com.study.crypto.utils.digest.SM3Digest digest = new com.study.crypto.utils.digest.SM3Digest();
		byte[]     hash        = new byte[32];
		byte[]     plaintext   = "123456".getBytes();
		ECPoint    publicPoint = KeyUtils.sm2p256v1.getCurve().decodePoint(ByteUtils.concatenate(new byte[]{4}, publicKeyBytes));
		BigInteger affineX     = publicPoint.getXCoord().toBigInteger();
		BigInteger affineY     = publicPoint.getYCoord().toBigInteger();
		digest.addId(affineX, affineY, Signer.SM2_ID);
		digest.update(plaintext, 0, plaintext.length);
		digest.doFinal(hash, 0);
		HashedSM2Signer signer = new HashedSM2Signer();
		signer.initHashed(true, privateKeyParameter);
		signer.updateHashed(hash);
		byte[] sm2SigValue = signer.generateHashedSignature();
		System.out.println(Base64.encodeBase64String(sm2SigValue));
	}
	
	@Test
	void testHash() throws Exception{
		byte[] in = "123456".getBytes();
		SM3Digest digest = new SM3Digest();
		digest.update(in, 0, in.length);
		byte[] hash = new byte[digest.getDigestSize()];
		digest.doFinal(hash, 0);
		System.out.println(Hex.toHexString(hash));
		
		BigInteger integer = BigInteger.valueOf(Long.MAX_VALUE);
		byte[] bytes = integer.toByteArray();
		System.out.println(Hex.toHexString(bytes));
	}
	
	@Test
	void testSM3Hash() throws Exception{
		com.study.crypto.utils.digest.SM3Digest digest = new com.study.crypto.utils.digest.SM3Digest();
		byte[] hash = new byte[32];
		byte[] plaintext = "这是一段明文".getBytes();
		byte[] usercert = FileUtils.readFileToByteArray(new File("D:/production.cer"));
		byte[] publicKeyBytes = KeyUtils.obtainPublicKeyBytes(usercert);
		byte[] concatenate = ByteUtils.concatenate(new byte[]{4}, publicKeyBytes);
		ECPoint publicPoint = KeyUtils.sm2p256v1.getCurve().decodePoint(concatenate);
		BigInteger affineX     = publicPoint.getXCoord().toBigInteger();
		BigInteger affineY     = publicPoint.getYCoord().toBigInteger();
		digest.addId(affineX, affineY, Signer.SM2_ID);
		digest.update(plaintext, 0, plaintext.length);
		digest.doFinal(hash, 0);
		System.out.println(Base64.encodeBase64String(hash));
	}
	
	@Test
	void test01() throws Exception{
		int[] V = new int[8];
		V[0] = 0x7380166F;
		V[1] = 0x4914B2B9;
		V[2] = 0x172442D7;
		V[3] = 0xDA8A0600;
		V[4] = 0xA96F30BC;
		V[5] = 0x163138AA;
		V[6] = 0xE38DEE4D;
		V[7] = 0xB0FB0E4E;
		byte[] bytes = BigEndianConversions.toByteArray(V);
		System.out.println(Hex.toHexString(bytes));
	}
	
	
}
