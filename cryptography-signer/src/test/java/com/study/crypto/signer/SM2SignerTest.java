package com.study.crypto.signer;

import com.study.crypto.utils.KeyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.signers.SM2Signer;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @author Songjin
 * @since 2022-03-29 10:29
 */
@Slf4j
class SM2SignerTest {
    
    @Test
    void testSM2Signer() throws Exception{
        byte[] keystore = FileUtils.readFileToByteArray(new File("D:/production.keystore"));
        byte[] privateKeyBytes = ByteUtils.subArray(keystore, 64);
        PrivateKey privateKey = KeyUtils.convertPrivateKey(privateKeyBytes);
        CipherParameters parameters = ECUtil.generatePrivateKeyParameter(privateKey);
        byte[] plaintext = "123456".getBytes();
        SM2Signer sm2Signer = new SM2Signer();
        sm2Signer.init(true, parameters);
        sm2Signer.update(plaintext, 0, plaintext.length);
        byte[] signature = sm2Signer.generateSignature();
        String signVal = Base64.encodeBase64String(signature);
        log.info("sm2签名值: {}", signVal);
    }
    
    @Test
    void testSM2Verify() throws Exception{
        byte[] keystore = FileUtils.readFileToByteArray(new File("D:/production.keystore"));
        byte[] publicKeyBytes = ByteUtils.subArray(keystore, 0, 64);
        PublicKey publicKey = KeyUtils.convertPublicKey(publicKeyBytes);
        AsymmetricKeyParameter parameters = ECUtil.generatePublicKeyParameter(publicKey);
        byte[] plaintext = "123456".getBytes();
        byte[] signature = Base64.decodeBase64("MEYCIQClECBK3A0pB9oLM5auu5JmQsGoTNO2kHbwuJK17MOngAIhAJnt30JKM3cVFyQF2gaCq3dEVvipPeMx7MK+3IcNvOuL");
        SM2Signer sm2Signer = new SM2Signer();
        sm2Signer.init(false, parameters);
        sm2Signer.update(plaintext, 0, plaintext.length);
        boolean verify = sm2Signer.verifySignature(signature);
        log.info("验证结果: {}", verify);
    }
    
    
}
