package com.study.crypto.signer;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 签名数据
 * @author Songjin
 * @since 2019/10/25 19:13
 */
public class SignInfoDto {
	
	/**
	 * 签名算法 oid 字符串，1.2.156.10197.1.501(默认) 或者 1.2.156.10197.1.301
	 */
	@JSONField(ordinal = 1)
	private String signAlgorithm;
	/**
	 * <ul>
	 *     <li>签名值的base64编码,编码前的签名值长度为64字节，其中r和s分别为32字节</li>
	 *     <li>该签名值是由制章系统的签名私钥进行签名生成的</li>
	 * </ul>
	 */
	@JSONField(ordinal = 2)
	private String signValue;
	
	public SignInfoDto() {
		this.setSignAlgorithm(GbConstants.SIGN_ALGORITHM);
	}
	
	/**
	 * 获取 签名算法 oid 字符串，1.2.156.10197.1.501
	 *
	 * @return signAlgorithm 签名算法 oid 字符串，1.2.156.10197.1.501
	 */
	public String getSignAlgorithm() {
		return this.signAlgorithm;
	}
	
	/**
	 * 设置 签名算法 oid 字符串，1.2.156.10197.1.501
	 *
	 * @param signAlgorithm 签名算法 oid 字符串，1.2.156.10197.1.501
	 */
	public void setSignAlgorithm(String signAlgorithm) {
		this.signAlgorithm = signAlgorithm;
	}
	
	/**
	 * <ul>
	 *     <li>签名值的base64编码,编码前的签名值长度为64字节，其中r和s分别为32字节</li>
	 *     <li>该签名值是由制章系统的签名私钥进行签名生成的</li>
	 * </ul>
	 * @return signValue
	 */
	public String getSignValue() {
		return this.signValue;
	}
	
	/**
	 * <ul>
	 *     <li>签名值的base64编码,编码前的签名值长度为64字节，其中r和s分别为32字节</li>
	 *     <li>该签名值是由制章系统的签名私钥进行签名生成的</li>
	 * </ul>
	 * @param signValue 签名值
	 */
	public void setSignValue(String signValue) {
		this.signValue = signValue;
	}
}
