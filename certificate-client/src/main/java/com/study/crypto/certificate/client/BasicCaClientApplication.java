package com.study.crypto.certificate.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Songjin
 * @since 2021-01-11 16:45
 */
@MapperScan(basePackages = "com.study.crypto.certificate.client.mapper")
@EnableTransactionManagement
@SpringBootApplication
public class BasicCaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasicCaClientApplication.class, args);
    }
}
