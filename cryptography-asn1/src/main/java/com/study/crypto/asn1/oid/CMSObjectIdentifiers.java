package com.study.crypto.asn1.oid;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

/**
 * @author Songjin
 * @since 2021-02-11 17:24
 */
public interface CMSObjectIdentifiers {
    
    /** SM2 密码算法加密签名消息语法规范 */
    ASN1ObjectIdentifier gm_cms_scheme = new ASN1ObjectIdentifier("1.2.156.10197.6.1.4.2");
    /** 数据类型 */
    ASN1ObjectIdentifier data = gm_cms_scheme.branch("1");
    /** 签名数据类型 */
    ASN1ObjectIdentifier signedData = gm_cms_scheme.branch("2");
    /** 数字信封数据类型 */
    ASN1ObjectIdentifier envelopedData = gm_cms_scheme.branch("3");
    /** 签名及数字信封数据类型 */
    ASN1ObjectIdentifier signedAndEnvelopedData = gm_cms_scheme.branch("4");
    /** 加密数据类型 */
    ASN1ObjectIdentifier encryptedData = gm_cms_scheme.branch("5");
    /** 密钥协商类型 */
    ASN1ObjectIdentifier keyAgreementInfo = gm_cms_scheme.branch("6");
    
    
}
