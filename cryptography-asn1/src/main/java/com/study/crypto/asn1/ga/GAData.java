package com.study.crypto.asn1.ga;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.*;

import java.security.InvalidParameterException;

/**
 * @author Songjin
 * @since 2021-06-13 11:45
 */
public class GAData extends ASN1Object {
    
    /** 设备编号 */
    private final DERIA5String  deviceCode;
    /** 印章名称 */
    private final DERUTF8String yzmc;
    /** 印章编码 */
    private final DERIA5String  yzbm;
    /** 印章制作单位编码 */
    private final DERIA5String  yzzzdwbm;
    /** 印章类型代码 */
    private final DERIA5String  yzlxdm;
    /** 经办人姓名 */
    private final DERUTF8String jbrXm;
    /** 经办人证件类型 */
    private final DERIA5String  jbrZjlx;
    /** 经办人证件号码 */
    private final DERIA5String  jbrZjhm;
    /** 制作日期 */
    private final DERIA5String  zzrq;
    /** 印章使用单位_单位名称 */
    private final DERUTF8String yzsydwDwmc;
    /** 印章使用单位_单位少数民族文字名称 */
    private final DERUTF8String yzsydwDwssmzwzmc;
    /** 印章使用单位_单位英文名称 */
    private final DERUTF8String yzsydwDwywmc;
    /** 印章制作单位_单位名称 */
    private final DERUTF8String yzzzdwDwmc;
    /** 印章制作单位_单位少数民族文字名称 */
    private final DERUTF8String yzzzdwDwssmzwzmc;
    /** 印章制作单位_单位英文名称 */
    private final DERUTF8String yzzzdwDwywmc;
    /** 印章使用单位_统一社会信用代码 */
    private final DERUTF8String yzsydwTyshxydm;
    
    public GAData(GADataStructure structure) {
        if (StringUtils.isBlank(structure.getDeviceCode())) {
            throw new InvalidParameterException("deviceCode参数为空");
        }
        if (StringUtils.isBlank(structure.getYzmc())) {
            throw new InvalidParameterException("yzmc参数为空");
        }
        if (StringUtils.isBlank(structure.getYzbm())) {
            throw new InvalidParameterException("yzbm参数为空");
        }
        if (StringUtils.isBlank(structure.getYzzzdwbm())) {
            throw new InvalidParameterException("yzzzdwbm参数为空");
        }
        if (StringUtils.isBlank(structure.getYzlxdm())) {
            throw new InvalidParameterException("yzlxdm参数为空");
        }
        if (StringUtils.isBlank(structure.getJbrXm())) {
            throw new InvalidParameterException("jbrXm参数为空");
        }
        if (StringUtils.isBlank(structure.getJbrZjlx())) {
            throw new InvalidParameterException("jbrZjlx参数为空");
        }
        if (StringUtils.isBlank(structure.getJbrZjhm())) {
            throw new InvalidParameterException("jbrZjhm参数为空");
        }
        if (StringUtils.isBlank(structure.getZzrq())) {
            throw new InvalidParameterException("zzrq参数为空");
        }
        if (StringUtils.isBlank(structure.getYzsydwDwmc())) {
            throw new InvalidParameterException("yzsydwDwmc参数为空");
        }
        if (StringUtils.isBlank(structure.getYzzzdwDwmc())) {
            throw new InvalidParameterException("yzzzdwDwmc参数为空");
        }
        if (StringUtils.isBlank(structure.getYzsydwTyshxydm())) {
            throw new InvalidParameterException("yzsydwTyshxydm参数为空");
        }
        this.deviceCode = new DERIA5String(structure.getDeviceCode());
        this.yzmc = new DERUTF8String(structure.getYzmc());
        this.yzbm = new DERIA5String(structure.getYzbm());
        this.yzzzdwbm = new DERIA5String(structure.getYzzzdwbm());
        this.yzlxdm = new DERIA5String(structure.getYzlxdm());
        this.jbrXm = new DERUTF8String(structure.getJbrXm());
        this.jbrZjlx = new DERIA5String(structure.getJbrZjlx());
        this.jbrZjhm = new DERIA5String(structure.getJbrZjhm());
        this.zzrq = new DERIA5String(structure.getZzrq());
        this.yzsydwDwmc = new DERUTF8String(structure.getYzsydwDwmc());
        this.yzsydwDwssmzwzmc = new DERUTF8String(structure.getYzsydwDwssmzwzmc());
        this.yzsydwDwywmc = new DERUTF8String(structure.getYzsydwDwywmc());
        this.yzzzdwDwmc = new DERUTF8String(structure.getYzzzdwDwmc());
        this.yzzzdwDwssmzwzmc = new DERUTF8String(structure.getYzzzdwDwssmzwzmc());
        this.yzzzdwDwywmc = new DERUTF8String(structure.getYzzzdwDwywmc());
        this.yzsydwTyshxydm = new DERUTF8String(structure.getYzsydwTyshxydm());
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector v = new ASN1EncodableVector(16);
        v.add(this.deviceCode);
        v.add(this.yzmc);
        v.add(this.yzbm);
        v.add(this.yzzzdwbm);
        v.add(this.yzlxdm);
        v.add(this.jbrXm);
        v.add(this.jbrZjlx);
        v.add(this.jbrZjhm);
        v.add(this.zzrq);
        v.add(this.yzsydwDwmc);
        v.add(this.yzsydwDwssmzwzmc);
        v.add(this.yzsydwDwywmc);
        v.add(this.yzzzdwDwmc);
        v.add(this.yzzzdwDwssmzwzmc);
        v.add(this.yzzzdwDwywmc);
        v.add(this.yzsydwTyshxydm);
        return new DERSequence(v);
    }
}
