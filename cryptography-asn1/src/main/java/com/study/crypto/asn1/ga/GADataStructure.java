package com.study.crypto.asn1.ga;

import lombok.Builder;
import lombok.Data;

/**
 * @author Songjin
 * @since 2021-06-13 12:01
 */
@Builder
@Data
public final class GADataStructure {
    
    /** 设备编号 */
    private String deviceCode;
    /** 印章名称 */
    private String yzmc;
    /** 印章编码 */
    private String yzbm;
    /** 印章制作单位编码 */
    private String yzzzdwbm;
    /** 印章类型代码 */
    private String yzlxdm;
    /** 经办人姓名 */
    private String jbrXm;
    /** 经办人证件类型 */
    private String jbrZjlx;
    /** 经办人证件号码 */
    private String jbrZjhm;
    /** 制作日期 */
    private String zzrq;
    /** 印章使用单位_单位名称 */
    private String yzsydwDwmc;
    /** 印章使用单位_单位少数民族文字名称 */
    private String yzsydwDwssmzwzmc;
    /** 印章使用单位_单位英文名称 */
    private String yzsydwDwywmc;
    /** 印章制作单位_单位名称 */
    private String yzzzdwDwmc;
    /** 印章制作单位_单位少数民族文字名称 */
    private String yzzzdwDwssmzwzmc;
    /** 印章制作单位_单位英文名称 */
    private String yzzzdwDwywmc;
    /** 印章使用单位_统一社会信用代码 */
    private String yzsydwTyshxydm;
}
