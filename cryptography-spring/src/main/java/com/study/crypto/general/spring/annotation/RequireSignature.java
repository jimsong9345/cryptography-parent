package com.study.crypto.general.spring.annotation;

import java.lang.annotation.*;

/**
 * 注解当前请求方法的返回值是否需要进行签名
 * @author Songjin
 * @since 2021-06-08 19:56
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequireSignature {
    
    boolean required() default true;
}
