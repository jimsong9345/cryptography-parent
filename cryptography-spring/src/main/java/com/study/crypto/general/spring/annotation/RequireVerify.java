package com.study.crypto.general.spring.annotation;

import java.lang.annotation.*;

/**
 * 注解当前请求参数是否需要进行验签
 * @author Songjin
 * @since 2021-06-08 19:54
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequireVerify {
    
    boolean required() default true;
}
