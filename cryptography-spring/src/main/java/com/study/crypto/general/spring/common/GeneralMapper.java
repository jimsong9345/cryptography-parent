package com.study.crypto.general.spring.common;

import tk.mybatis.mapper.additional.idlist.IdListMapper;
import tk.mybatis.mapper.additional.insert.InsertListMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Songjin
 * @since 2021-01-05 14:35
 */
public interface GeneralMapper<T> extends Mapper<T>, InsertListMapper<T>, IdListMapper<T, String> {
}
