package com.study.crypto.general.spring.common;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Songjin
 * @since 2021-01-05 14:09
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class GeneralEntity implements Serializable {
    
    /** 主键 */
    @Id
    @Column(name = "id")
    private String id;
    
    /** 扩展字段1 */
    @Column(name = "ext1")
    private String ext1;
    
    /** 扩展字段2 */
    @Column(name = "ext2")
    private String ext2;
    
    /** 扩展字段3 */
    @Column(name = "ext3")
    private String ext3;
    
    /** 创建时间 */
    @Column(name = "create_time")
    private LocalDateTime createTime;
    
    /** 更新时间 */
    @Column(name = "update_time")
    private LocalDateTime updateTime;
    
    /** 创建用户id */
    @Column(name = "user_id")
    private String userId;
    
}
