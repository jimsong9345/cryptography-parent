package com.study.crypto.utils;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

import java.nio.charset.StandardCharsets;
import java.security.Security;

/**
 * 摘要计算工具类
 * @author Songjin
 * @since 2020-12-28 15:20
 */
public final class DigestUtil {
	
	static {
		Security.addProvider(new BouncyCastleProvider());
	}
	
	private DigestUtil() {
	}
	
	/**
	 * 计算原文摘要(使用SM3算法)，将摘要值转换为十六进制字符串返回
	 * @param plainText 原文字符串
	 * @return 十六进制摘要值
	 */
	public static String sm3Hex(final String plainText) {
		return sm3Hex(plainText.getBytes(StandardCharsets.UTF_8));
	}
	
	/**
	 * 计算原文摘要(使用SM3算法)，将摘要值转换为十六进制字符串返回
	 * @param bytes 原文字节数据
	 * @return 十六进制摘要值
	 */
	public static String sm3Hex(final byte[] bytes) {
		byte[] digest = sm3(bytes);
		return Hex.toHexString(digest);
	}
	
	/**
	 * 计算原文摘要(使用SM3算法)，将摘要值转换为 base64 字符串返回
	 * @param bytes 原文字节数据
	 * @return 十六进制摘要值
	 */
	public static String sm3Base64(final byte[] bytes) {
		byte[] digest = sm3(bytes);
		return Base64.encodeBase64String(digest);
	}
	
	/**
	 * 国密sm3摘要计算
	 * @param in 摘要原文
	 * @return 摘要值
	 */
	public static byte[] sm3(byte[] in) {
		SM3Digest digest = new SM3Digest();
		digest.update(in, 0, in.length);
		byte[] hash = new byte[digest.getDigestSize()];
		digest.doFinal(hash, 0);
		return hash;
	}

}
