package com.study.crypto.utils.digest;

import org.bouncycastle.crypto.digests.EncodableDigest;
import org.bouncycastle.crypto.digests.SM3Digest;

/**
 * 支持保存摘要中间状态的 sm3 摘要计算
 *
 * @author Songjin
 * @since 2022-09-01 13:03
 */
public class SM3EncodableDigest extends SM3Digest implements EncodableDigest {

    @Override
    public byte[] getEncodedState() {
        // byte[] state = new byte[52 + xOff * 4];
        //
        // super.populateState(state);
        //
        // Pack.intToBigEndian(H1, state, 16);
        // Pack.intToBigEndian(H2, state, 20);
        // Pack.intToBigEndian(H3, state, 24);
        // Pack.intToBigEndian(H4, state, 28);
        // Pack.intToBigEndian(H5, state, 32);
        // Pack.intToBigEndian(H6, state, 36);
        // Pack.intToBigEndian(H7, state, 40);
        // Pack.intToBigEndian(H8, state, 44);
        // Pack.intToBigEndian(xOff, state, 48);
        //
        // for (int i = 0; i != xOff; i++) {
        //     Pack.intToBigEndian(X[i], state, 52 + (i * 4));
        // }
        //
        // return state;
        return null;
    }
}
