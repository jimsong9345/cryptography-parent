package com.study.crypto.utils;

/**
 * AES 加解密工具类
 * @author Songjin
 * @since 2022-08-03 18:02
 */
public final class AesUtils extends GeneralSymmetric {

    static {
        ALGORITHM_NAME = "AES";
        ALGORITHM_NAME_ECB_PADDING = "AES/ECB/PKCS5Padding";
        ALGORITHM_NAME_ECB_NOPADDING = "AES/ECB/NoPadding";
        ALGORITHM_NAME_CBC_PADDING = "AES/CBC/PKCS5Padding";
        ALGORITHM_NAME_CBC_NOPADDING = "AES/CBC/NoPadding";
    }

    private AesUtils() {
    }
}
