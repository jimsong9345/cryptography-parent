package com.study.crypto.utils;

import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.util.encoders.Hex;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.UUID;

/**
 * @author Songjin
 * @since 2021-01-04 18:47
 */
public final class EssPdfUtil {
    
    private EssPdfUtil() {
    }
    
    /**
     * <p>产生随机UUID</p>
     * @return String
     */
    public static String genRandomUuid() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-", "");
    }
    
    /**
     * 将 LocalDateTime 转换为 Date
     * @param datetime LocalDateTime类型
     * @return Date类型
     */
    public static Date of(LocalDateTime datetime) {
        return Date.from(datetime.atZone(ZoneId.systemDefault()).toInstant());
    }
    
    /**
     * 将 Date 转换为 LocalDateTime
     * @param date Date类型
     * @return LocalDateTime类型
     */
    public static LocalDateTime of(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }
    
    /**
     * 计算原文摘要(使用SM3算法)，将摘要值转换为十六进制字符串返回
     * @param plainText 原文字符串
     * @return 十六进制摘要值
     */
    public static String digestHex(final String plainText) {
        return digestHex(plainText.getBytes(StandardCharsets.UTF_8));
    }
    
    /**
     * 计算原文摘要(使用SM3算法)，将摘要值转换为十六进制字符串返回
     * @param bytes 原文字节数据
     * @return 十六进制摘要值
     */
    public static String digestHex(final byte[] bytes) {
        byte[] digest = digestSM3(bytes);
        return Hex.toHexString(digest);
    }
    
    /**
     * 国密sm3摘要计算
     * @param in 摘要原文
     * @return 摘要值
     */
    public static byte[] digestSM3(byte[] in) {
        SM3Digest digest = new SM3Digest();
        digest.update(in, 0, in.length);
        byte[] hash = new byte[digest.getDigestSize()];
        digest.doFinal(hash, 0);
        return hash;
    }
}
