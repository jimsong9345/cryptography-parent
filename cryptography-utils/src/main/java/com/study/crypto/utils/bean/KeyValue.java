package com.study.crypto.utils.bean;

/**
 * 键值对
 * @author Songjin
 * @since 2022-10-20 11:22
 */
public class KeyValue<K, V> {

    /** The key */
    private K key;
    /** The value */
    private V value;

    public KeyValue() {
    }

    /**
     * Constructs a new pair with the specified key and given value.
     *
     * @param key  the key for the entry, may be null
     * @param value  the value for the entry, may be null
     */
    public KeyValue(final K key, final V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}
