package com.study.crypto.utils;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * @author Songjin
 * @since 2023-03-23 15:12
 */
public final class ResourceUtils {

    private ResourceUtils() {
    }

    /**
     * 读取文件字节流
     * @param path 文件路径
     * @return 文件字节流
     * @throws IOException io异常
     */
    public static byte[] toByteArray(String path) throws IOException {
        ClassLoader defaultClassLoader = getDefaultClassLoader();
        assert defaultClassLoader != null;
        URL resource = defaultClassLoader.getResource(path);
        assert resource != null;
        return IOUtils.toByteArray(resource);
    }

    /**
     * 读取文件字节流
     * @param path 文件路径
     * @return 文件字节流
     * @throws IOException io异常
     */
    public static String readFileToString(String path) throws IOException {
        ClassLoader classLoader = getDefaultClassLoader();
        return IOUtils.resourceToString(path, StandardCharsets.UTF_8, classLoader);
    }

    /**
     * Return the default ClassLoader to use: typically the thread context
     * ClassLoader, if available; the ClassLoader that loaded the ClassUtils
     * class will be used as fallback.
     * <p>Call this method if you intend to use the thread context ClassLoader
     * in a scenario where you clearly prefer a non-null ClassLoader reference:
     * for example, for class path resource loading (but not necessarily for
     * {@code Class.forName}, which accepts a {@code null} ClassLoader
     * reference as well).
     * @return the default ClassLoader (only {@code null} if even the system
     * ClassLoader isn't accessible)
     * @see Thread#getContextClassLoader()
     * @see ClassLoader#getSystemClassLoader()
     */
    public static ClassLoader getDefaultClassLoader() {
        ClassLoader cl = null;
        try {
            cl = Thread.currentThread().getContextClassLoader();
        }
        catch (Throwable ex) {
            // Cannot access thread context ClassLoader - falling back...
        }
        if (cl == null) {
            // No thread context class loader -> use class loader of this class.
            cl = ResourceUtils.class.getClassLoader();
            if (cl == null) {
                // getClassLoader() returning null indicates the bootstrap ClassLoader
                try {
                    cl = ClassLoader.getSystemClassLoader();
                }
                catch (Throwable ex) {
                    // Cannot access system ClassLoader - oh well, maybe the caller can live with null...
                }
            }
        }
        return cl;
    }
}
