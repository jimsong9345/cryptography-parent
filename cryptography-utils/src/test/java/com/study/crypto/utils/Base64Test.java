package com.study.crypto.utils;

import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.Test;

/**
 * @author Songjin
 * @since 2023-05-16 14:06
 */
class Base64Test {

    @Test
    void testBase64() throws Exception{
        String str = "YW55IGNhcm5hbCBwbGVhc3VyZQ==";
        byte[] bytes = Base64.decodeBase64(str);
        System.out.println(bytes);
        System.out.println(Byte.MAX_VALUE);
        System.out.println(Byte.MIN_VALUE);
    }

}
