package com.study.crypto.utils;

import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/**
 * @author Songjin
 * @since 2023-01-18 17:38
 */
class HexTest {

    @Test
    void testHex() throws Exception {
        String plaintext = "123abc;*{}";
        String hex = Hex.toHexString(plaintext.getBytes());
        System.out.println("十六进制: " + hex);

        byte[] decode = Hex.decode(hex);
        System.out.println("还原字符串: " + new String(decode));
    }

}
