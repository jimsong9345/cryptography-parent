package com.study.crypto.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.*;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.security.PrivateKey;

/**
 * SM2 算法测试
 * @author Songjin
 * @since 2022-07-29 15:21
 */
class SM2UtilsTest {

    /**
     * 测试封装 sm2 加密结构
     */
    @Test
    void testEncapsulateCipher() throws Exception{
        String cipherHex = "99503EFD61CE5C9A7D535D4C079EF31C523BA05BA27F854DE91E428AEA1D1F1EFEDF168CB3A0E4386AD3B52E7B4D7A086F272431021E45F0B234ADD780C3DEAD25D179CC900550704531B4AB8D3A43F7E9F2FD0A6B22DBA683C952556F840E99C55051B4959585E6E7FA58A94ED3FFA0";
        byte[] cipherBytes = Hex.decode(cipherHex);

        byte[] publicKeyBytes = ByteUtils.subArray(cipherBytes, 0, 64);
        // C1: 公钥x、y
        byte[] publicKeyX = ByteUtils.subArray(publicKeyBytes, 0, 32);
        byte[] publicKeyY = ByteUtils.subArray(publicKeyBytes, 32);
        // C2: 密文
        byte[] cipher = ByteUtils.subArray(cipherBytes, 96);
        // C3: 摘要
        byte[] hash_ = ByteUtils.subArray(cipherBytes, 64, 96);
        ASN1EncodableVector vector = new ASN1EncodableVector();
        ASN1Integer x = new ASN1Integer(BigIntegers.fromUnsignedByteArray(publicKeyX));
        ASN1Integer y = new ASN1Integer(BigIntegers.fromUnsignedByteArray(publicKeyY));
        DEROctetString hash = new DEROctetString(hash_);
        DEROctetString ciphertext = new DEROctetString(cipher);
        vector.add(x);
        vector.add(y);
        vector.add(hash);
        vector.add(ciphertext);
        byte[] encoded = new DERSequence(vector).getEncoded(ASN1Encoding.DER);
        Assertions.assertNotNull(encoded, "封装 sm2 密文结构");
        System.out.println(Hex.toHexString(encoded));
        FileUtils.writeByteArrayToFile(new File("C:/Users/Administrator/Desktop/新建文件夹/asn1-sm2-cipher.dat"), encoded);
    }

    /**
     * 测试解密 sm2 密文结构
     */
    @Test
    void testDecryptCipher() throws Exception{
        String cipherPath = "C:/Users/Administrator/Desktop/新建文件夹/asn1-sm2-cipher.dat";
        byte[] cipherBytes = FileUtils.readFileToByteArray(new File(cipherPath));
        String privateKeyB64 = "9tFomaJ0GBhKYLavrDIpZxdM34H3KCQbsIiYU+cnYZ8=";
        PrivateKey privateKey = KeyUtils.convertPrivateKey(Base64.decodeBase64(privateKeyB64));
        byte[] decrypt = SM2Utils.decrypt(privateKey, cipherBytes, SM2Engine.Mode.C1C3C2);
        Assertions.assertNotNull(decrypt, "解密失败");
        System.out.println(Hex.toHexString(decrypt).toUpperCase());
    }

    
}
