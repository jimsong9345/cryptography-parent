package com.study.crypto.utils;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.ASN1BitString;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Security;

/**
 * asn1 结构解析
 * @author Songjin
 * @since 2022-09-01 18:36
 */
class Asn1ParseTest {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * 使用sm4 ecb解密sm2私钥密文
     */
    @Test
    void testSM4Decrypt() throws Exception{
        String keyB64 = "ZRFdEEWsiDAL/ZeavfFYoQ==";
        byte[] key = Base64.decodeBase64(keyB64);
        String encryptedPrivateKeyB64 = "aenzUl+w3h6HnC2O35j3uQQy+MHVBig4pazWLLCDqeM=";
        byte[] encryptedPrivateKey = Base64.decodeBase64(encryptedPrivateKeyB64);
        SecretKey secretKey = new SecretKeySpec(key, "SM4");
        Cipher cipher = Cipher.getInstance("SM4/ECB/NoPadding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] privateKey = cipher.doFinal(encryptedPrivateKey);
        Assertions.assertNotNull(encryptedPrivateKey, "获取私钥密文数据为空");
        System.out.println("sm2私钥Base64字符串: " + Base64.encodeBase64String(privateKey));
    }

    /**
     * 从sm2数字信封中解析sm2加密结构字节流、私钥密文字节流
     */
    @Test
    void testObtainSM2CiperFromSM2Envelop() throws Exception{
        String sm2EnvelopB64 = "MIHxMAwGCCqBHM9VAWgBBQAwegIhAJUJjxrx9sX8T7Y074fF7Gzk8/EvkAtnFZ+xK3M/BjjVAiEAkfmIIUlC//Ov2ECdCuSXTi/Tf5zgEy2iCkio8j3kBUYEIAjGmEIVEojF2G5rxaUL54asudmnVnF3UcYmrcAa98b3BBDck77sPQZPR0jSPZV5TzmZA0IABP1E4qdmdI5pQJqkYVdowhnVA7VvdoWlu7zVTleYp+qN07zVOdFO0U3WoJPcAXEa5KwMie05/oNmLPKiBI2OIgcDIQBp6fNSX7DeHoecLY7fmPe5BDL4wdUGKDilrNYssIOp4w==";
        byte[] encBytes = Base64.decodeBase64(sm2EnvelopB64);
        ASN1InputStream input = new ASN1InputStream(encBytes);
        ASN1Primitive asn1 = input.readObject();
        ASN1Sequence sequence = ASN1Sequence.getInstance(asn1);
        ASN1Sequence sm2Ciper = (ASN1Sequence) sequence.getObjectAt(1);
        ASN1BitString asn1EncryptedPrivateKey = (ASN1BitString) sequence.getObjectAt(3);
        byte[] sm2CiperEncoded = sm2Ciper.getEncoded();
        byte[] encryptedPrivateKey = asn1EncryptedPrivateKey.getOctets();
        input.close();
        Assertions.assertNotNull(sm2CiperEncoded, "获取sm2加密结构为空");
        Assertions.assertNotNull(encryptedPrivateKey, "获取私钥密文数据为空");
        System.out.println("sm2加密结构Base64字符串: " + Base64.encodeBase64String(sm2CiperEncoded));
        System.out.println("私钥密文Base64字符串: " + Base64.encodeBase64String(encryptedPrivateKey));
    }
    
}
