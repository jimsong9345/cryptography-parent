package com.study.crypto.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.security.Security;

/**
 * AES 加解密算法测试
 * @author Songjin
 * @since 2022-08-31 16:10
 */
@Slf4j
class AesTest {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    byte[] plaintext = "1234567ad890".getBytes();
    
    @Test
    void testAesCBCEncrypt() throws Exception{
        // 生成 AES 随机密钥
        int keyLen = 256;
        byte[] key = new byte[keyLen / 8];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(key);

        // 生成初始化向量 IV
        byte[] iv = new byte[16];
        secureRandom.nextBytes(iv);
        IvParameterSpec ivspec = new IvParameterSpec(iv);

        // 进行数据加密
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivspec);
        byte[] encrypted = cipher.doFinal(plaintext);
        Assertions.assertNotNull(encrypted, "加密结果为空");
        log.info("加密密钥Base64: {}", Base64.encodeBase64String(key));
        log.info("初始化向量Base64: {}", Base64.encodeBase64String(iv));
        log.info("加密产生密文: {}", Base64.encodeBase64String(encrypted));
    }

    @Test
    void testAesCBCDecrypt() throws Exception{
        byte[] encrypted = Base64.decodeBase64("7N4+/JObDGsHUqY4IUN5GA==");
        byte[] key = Base64.decodeBase64("uUUidurW6xglLXJKmrxiP1EF6rdB74+DZkL/3ZurvEU=");
        byte[] iv = Base64.decodeBase64("W/MVNrEzU+LJEf8Y4oHZtg==");
        IvParameterSpec ivspec = new IvParameterSpec(iv);
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivspec);
        byte[] decrypted = cipher.doFinal(encrypted);
        Assertions.assertNotNull(decrypted, "解密结果为空");
        Assertions.assertArrayEquals(plaintext, decrypted, "解密结果与原文不一致");
        log.info("加密密钥Base64: {}", Base64.encodeBase64String(key));
        log.info("解密出明文: {}", new String(decrypted));
    }

    /**
     * 通过修改密钥长度参数，进行 AES 128、192、256 加解密计算
     */
    @Test
    void testAesECBEncrypt() throws Exception{
        // 生成 AES 随机密钥
        int keyLen = 128;
        byte[] key = new byte[keyLen / 8];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(key);

        // 进行数据加密
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] encrypted = cipher.doFinal(plaintext);
        Assertions.assertNotNull(encrypted, "加密结果为空");
        log.info("加密密钥Base64: {}", Base64.encodeBase64String(key));
        log.info("加密产生密文: {}", Base64.encodeBase64String(encrypted));
    } 

    @Test
    void testAesECBDecrypt() throws Exception{
        byte[] encrypted = Base64.decodeBase64("uFVIxYrYV1Ju+WzH9lyaDw==");
        byte[] key = Base64.decodeBase64("U2cDg3B6lH+oOB19o1+Sag==");
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, keySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        Assertions.assertNotNull(decrypted, "解密结果为空");
        Assertions.assertArrayEquals(plaintext, decrypted, "解密结果与原文不一致");
        log.info("加密密钥Base64: {}", Base64.encodeBase64String(key));
        log.info("解密出明文: {}", new String(decrypted));
    }
    
}
