package com.study.crypto.utils;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;
import java.security.Security;
import java.util.Arrays;
import java.util.List;

/**
 * @author Songjin
 * @since 2023-03-21 17:31
 */
class Pkcs10Test {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void test1() throws Exception{
        String certB64 = "MIIByzCCAXKgAwIBAgIKGhAAAAAAAAAAfjAKBggqgRzPVQGDdTBDMQswCQYDVQQGEwJDTjENMAsGA1UECgwEQkpDQTENMAsGA1UECwwEQkpDQTEWMBQGA1UEAwwNU00yIFNlcnZlciBDQTAeFw0yMzAzMjIwNzI0MTBaFw0yNDAzMjIwNzI0MTBaMDExCzAJBgNVBAYTAkNOMQ0wCwYDVQQKDARiamNhMRMwEQYDVQQDDApEZXZpY2VDZXJ0MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAELoNnED3zTwxNXzkX3oqn8MfzucmbmZWPw5Z5uiprp7aM7bMJRNEYHilPsxBp9bQmYIYM05xTH0kiS0NumXx52KNgMF4wDAYDVR0TAQH/BAIwADAOBgNVHQ8BAf8EBAMCBsAwHwYDVR0jBBgwFoAU8tZzbdlMzz+r6V1bO92x7xgk6jQwHQYDVR0OBBYEFGLbfQImUB+kclqMIsPk3BI/Pt4ZMAoGCCqBHM9VAYN1A0cAMEQCIFFPb57WBjxzNuk9ttruAt+mTkR1zR4SnuQ+wJylvNtMAiApHs6uZzrGLbnG4O0hqY0lkaS2+PZs882bMCO93pxQ8Q==";
        Certificate certificate = CertUtils.convertCertificate(Base64.decodeBase64(certB64));
        SubjectPublicKeyInfo publicKeyInfo = certificate.getSubjectPublicKeyInfo();
        byte[] bytes = publicKeyInfo.getPublicKeyData().getBytes();
        System.out.println("公钥: " + Base64.encodeBase64String(bytes));

        String dn = "C=CN,O=NHSA,ST=10,L=71,L=00,L=01,OU=01,T=91110108722619411A,CN=北京数字认证测试";
        X500Name x500Name = CertUtils.buildDistinctName(dn);
        System.out.println(x500Name.toString());

        List<ASN1ObjectIdentifier> list = Arrays.asList(BCStyle.C, BCStyle.CN, BCStyle.O);
        boolean contains = list.contains(new ASN1ObjectIdentifier("2.5.4.6"));
        System.out.println(contains);
    }

    @Test
    void testCreateCSR() throws Exception {
        String signalg = "";
        int alglength = 0;
        String keyAlg = "";
        String alg = "SM2";
        String dn = "C=CN,O=INFOSEC Technologies RSA,CN=RSARoot";
        if (alg.toUpperCase().equals("RSA1024")) {
            signalg = "SHA1WithRSA";
            alglength = 1024;
            keyAlg = "RSA";
        } else if (alg.toUpperCase().equals("RSA2048")) {
            signalg = "SHA1WithRSA";
            alglength = 2048;
            keyAlg = "RSA";
        } else if (alg.toUpperCase().equals("SM2")) {
            signalg = "SM3withSM2";
            alglength = 256;
            keyAlg = "SM2";
        }
        KeyPair kp = KeyUtils.generateKeyPair(KeyUtils.ALGO_SM2);
        X500Name subject = new X500Name(dn);
        String csr = Pkcs10Utils.createCertificateRequest(kp.getPrivate(), kp.getPublic(), dn, signalg);
        System.out.println(csr);
    }

}
