package com.study.crypto.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.SecureRandom;
import java.security.Security;

/**
 * 3DES 加解密测试
 * @author Songjin
 * @since 2022-08-31 15:54
 */
@Slf4j
class DESedeTest {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    byte[] plaintext = "1234567890".getBytes();

    @Test
    void test3DESCBCEncrypt() throws Exception{
        byte[] key = new byte[DESedeKeySpec.DES_EDE_KEY_LEN];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(key);
        DESedeKeySpec spec = new DESedeKeySpec(key);
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
        SecretKey deskey = keyfactory.generateSecret(spec);
        String ivText = "12345678";
        IvParameterSpec iv = new IvParameterSpec(ivText.getBytes());
        Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, deskey, iv);
        byte[] encrypted = cipher.doFinal(plaintext);
        Assertions.assertNotNull(encrypted, "加密结果为空");
        log.info("加密密钥Base64: {}", Base64.encodeBase64String(key));
        log.info("加密参数iv: {}", ivText);
        log.info("加密产生密文: {}", Base64.encodeBase64String(encrypted));
    }

    @Test
    void test3DESCBCDecrypt() throws Exception{
        byte[] encrypted = Base64.decodeBase64("VZH1P7M8HMFagnAlNS59jw==");
        byte[] key = Base64.decodeBase64("mnFc+5ytC5FYmeo+PwXsTwK5vLpWS+uI");
        DESedeKeySpec keySpec = new DESedeKeySpec(key);
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
        SecretKey deskey = keyfactory.generateSecret(keySpec);
        String ivText = "12345678";
        IvParameterSpec iv = new IvParameterSpec(ivText.getBytes());
        Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, deskey, iv);
        byte[] decrypted = cipher.doFinal(encrypted);
        Assertions.assertNotNull(decrypted, "解密结果为空");
        Assertions.assertArrayEquals(plaintext, decrypted, "解密结果与原文不一致");
    }

    /**
     * 3DES 算法密钥长度为 24 字节(192 位)
     */
    @Test
    void test3DESECBEncrypt() throws Exception{
        // 生成 3DES 随机密钥
        byte[] key = new byte[DESedeKeySpec.DES_EDE_KEY_LEN];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(key);
        DESedeKeySpec spec = new DESedeKeySpec(key);
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
        SecretKey deskey = keyfactory.generateSecret(spec);
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, deskey);
        byte[] encrypted = cipher.doFinal(plaintext);
        Assertions.assertNotNull(encrypted, "加密结果为空");
        log.info("加密密钥Base64: {}", Base64.encodeBase64String(key));
        log.info("加密产生密文: {}", Base64.encodeBase64String(encrypted));
    }
    
    @Test
    void test3DESECBDecrypt() throws Exception{
        byte[] encrypted = Base64.decodeBase64("dImZ2A6tmIzuOuYcz/45ig==");
        byte[] key = Base64.decodeBase64("ywE4x6wXcKkdVjjBV3XAYzA14xntcKuW");
        DESedeKeySpec keySpec = new DESedeKeySpec(key);
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
        SecretKey deskey = keyfactory.generateSecret(keySpec);
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, deskey);
        byte[] decrypted = cipher.doFinal(encrypted);
        Assertions.assertNotNull(decrypted, "解密结果为空");
        Assertions.assertArrayEquals(plaintext, decrypted, "解密结果与原文不一致");
        log.info("加密密钥Base64: {}", Base64.encodeBase64String(key));
        log.info("解密出明文: {}", new String(decrypted));
    }

}
