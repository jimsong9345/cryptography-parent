package com.study.crypto.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.BigIntegers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Songjin
 * @since 2022-10-17 16:45
 */
@Slf4j
class RSTest {

    /**
     * R、S签名值包装成 DER 编码签名值
     */
    @Test
    void testSignRSEncapsulate() throws Exception{
        String rsB64 = "9tI7CF3N8lqIS7cIVBnA+cRAk8I9Xlq/T5WMrDpLp6piqV9JOOB00LJMtG8vn4sqnr7mPdgiZ66z6IOkMcBQwQ==";
        byte[] signVal = Base64.decodeBase64(rsB64);
        byte[] r = ByteUtils.subArray(signVal, 0, 32);
        byte[] s = ByteUtils.subArray(signVal, 32);
        Assertions.assertEquals(32, r.length, "r分量长度不是32字节");
        Assertions.assertEquals(32, s.length, "s分量长度不是32字节");
        ASN1EncodableVector vector = new ASN1EncodableVector();
        vector.add(new ASN1Integer(BigIntegers.fromUnsignedByteArray(r)));
        vector.add(new ASN1Integer(BigIntegers.fromUnsignedByteArray(s)));
        byte[] encoded = new DERSequence(vector).getEncoded(ASN1Encoding.DER);
        String signValB64 = Base64.encodeBase64String(encoded);
        System.out.println("签名值Der编码，Base64: " + signValB64);
    }

}
