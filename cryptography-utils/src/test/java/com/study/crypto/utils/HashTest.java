package com.study.crypto.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 * @author Songjin
 * @since 2022-08-31 13:39
 */
@Slf4j
class HashTest {

    private byte[] plaintext;
    private final int length = 1024;

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void testSHA1() throws Exception{
        SHA1Digest digest = new SHA1Digest();
        byte[] hash = new byte[digest.getDigestSize()];
        byte[] plaintext = "123".getBytes();
        digest.update(plaintext, 0, plaintext.length);
        digest.doFinal(hash, 0);
        Assertions.assertNotNull(hash, "计算hash结果为空");
        System.out.println("hash十六进制字符串: " + Hex.toHexString(hash));
    }

    @Test
    void testSHA256() {
        SHA256Digest digest = new SHA256Digest();
        byte[] hash = new byte[digest.getDigestSize()];
        digest.update(plaintext, 0, plaintext.length);
        digest.doFinal(hash, 0);
        Assertions.assertNotNull(hash, "计算hash结果为空");
        System.out.println("hash十六进制字符串: " + Hex.toHexString(hash));
    }

    @Test
    void testPrivateKey() throws Exception{
        String pkcs8 = "MIGHAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBG0wawIBAQQgbdT9v2SDIRY89+arHxeLDq6OQWV9JCsynrUV0ikzgYmhRANCAASVTcxZAlsIzZ5A2C9WGp/CX75Y2/uUlWm/9BW1kIAtTVZhFPrP/8TqZC6dc6IXrdeKhHJ7taZfHthyMCULkuD9";
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(pkcs8));
        KeyFactory keyFactory = KeyFactory.getInstance("EC", new BouncyCastleProvider());
        PrivateKey privateKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
        System.out.println(privateKey);
    }

    @Test
    void testSHA256MapReduce() throws Exception{
        testSHA256();

        byte[][] segments = ByteUtils.split(plaintext, length / 2);
        SHA256Digest digest1 = new SHA256Digest();
        byte[] hash = new byte[digest1.getDigestSize()];
        digest1.update(segments[0], 0, segments[0].length);
        byte[] encodedState = digest1.getEncodedState();

        SHA256Digest digest2 = new SHA256Digest(encodedState);
        digest2.update(segments[1], 0, segments[1].length);
        digest2.doFinal(hash, 0);
        Assertions.assertNotNull(hash, "计算hash结果为空");
        System.out.println("hash十六进制字符串: " + Hex.toHexString(hash));
    }

    @Test
    void testHashSM3() throws Exception{
        byte[] hash = new byte[32];
        SM3Digest digest = new SM3Digest();
        digest.update(plaintext, 0, plaintext.length);
        digest.doFinal(hash, 0);
        Assertions.assertNotNull(hash, "计算hash结果为空");
        log.info("hash十六进制字符串: {}", Hex.toHexString(hash));
    }

    @BeforeEach
    void beforeAll() throws Exception{
        plaintext = new byte[length];
        SecureRandom rand = new SecureRandom();
        rand.nextBytes(plaintext);
    }

    /**
     * 分段计算摘要，与单个整体计算结果是否相同
     */
    @Test
    void testHashSM3MapReduce() throws Exception{
        testHashSM3();

        byte[][] segments = ByteUtils.split(plaintext, length / 2);
        byte[] hash = new byte[32];
        SM3Digest digest1 = new SM3Digest();
        digest1.update(segments[0], 0, segments[0].length);
    }
    
    
}
