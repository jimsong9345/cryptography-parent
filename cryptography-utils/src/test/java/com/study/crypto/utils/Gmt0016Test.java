package com.study.crypto.utils;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * @author Songjin
 * @since 2022-09-09 9:38
 */
class Gmt0016Test {

    /**
     * 解析 GMT 0016 EnvelopedKeyBlob 数据流，封装为 GBT 25276 sm2数字信封
     */
    @Test
    void testParseEnvelopedKeyBlob() throws Exception{
        // 教育部CA签发的0016数字信封
        String envelopedKeyBlobB64 = "AQAAAAEBAAAAAQAAIclFJS1iJ61qGEh0zY+PSyHJRSUtYietahhIdM2Pj0vo1TNDnpa/unK1Jx7CzAk/CihIOrv/SemF1ZYjmEKIkwABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADfuTan/JM3ggSaCj/p7AzA7bG/+MhLhDhZ/ieO5k2fkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANzljQra+7j/qme/KroM30jTn92MszFYWiBkHm9rKl4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA075g1LcwnSA9Zq5eTmijsSi/duG2pXNwvFtNNdONkPUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSOjgKq+Od3ieMdmTtYhPOuds5oM9N5R8PBRcywpwqCpLr6sbx9f1hHXNsfCv2kUK+Q81Wy40FQYMHvfcssFE8QAAAA8cvgtQ3LU/aauN/xoipj5wAAAAAAAAAAAAAAAAAAAAA=";
        byte[] envelopedKeyBlob = Base64.decodeBase64(envelopedKeyBlobB64);
        System.out.println("字节数: " + envelopedKeyBlob.length);

        byte[] cipherPublicKeyXBytes = ByteUtils.subArray(envelopedKeyBlob, 240, 272);
        byte[] cipherPublicKeyYBytes = ByteUtils.subArray(envelopedKeyBlob, 304, 336);
        byte[] cipherHashBytes = ByteUtils.subArray(envelopedKeyBlob, 336, 368);
        byte[] cipherLenBytes = ByteUtils.subArray(envelopedKeyBlob, 368, 372);
        int cipherLen = ByteBuffer.wrap(cipherLenBytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
        System.out.println("对称密钥密文长度: " + cipherLen);
        byte[] cipherBytes = ByteUtils.subArray(envelopedKeyBlob, 372, 388);
        ASN1EncodableVector cipherVector = new ASN1EncodableVector();
        cipherVector.add(new ASN1Integer(new BigInteger(1, cipherPublicKeyXBytes)));
        cipherVector.add(new ASN1Integer(new BigInteger(1, cipherPublicKeyYBytes)));
        cipherVector.add(new DEROctetString(cipherHashBytes));
        cipherVector.add(new DEROctetString(cipherBytes));
        DERSequence sm2Cipher = new DERSequence(cipherVector);

        byte[] symmAlgIdBytes = ByteUtils.subArray(envelopedKeyBlob, 4, 8);
        int symmAlgId = ByteBuffer.wrap(symmAlgIdBytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
        System.out.println(Integer.toHexString(symmAlgId));

        byte[] bitsBytes = ByteUtils.subArray(envelopedKeyBlob, 8, 12);
        int bits = ByteBuffer.wrap(bitsBytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
        System.out.println(Integer.toHexString(bits));

        byte[] encryptedPriKeyBytes = ByteUtils.subArray(envelopedKeyBlob, 12, 76);
        byte[] publicKeyXBytes = ByteUtils.subArray(envelopedKeyBlob, 112, 144);
        byte[] publicKeyYBytes = ByteUtils.subArray(envelopedKeyBlob, 176, 208);
        byte[] publicKeyBytes = ByteUtils.concatenate(publicKeyXBytes, publicKeyYBytes);

        ASN1EncodableVector sm2EnvelopedVector = new ASN1EncodableVector();
        sm2EnvelopedVector.add(new AlgorithmIdentifier(GMObjectIdentifiers.sm1_ecb, DERNull.INSTANCE));
        sm2EnvelopedVector.add(sm2Cipher);
        sm2EnvelopedVector.add(new DERBitString(publicKeyBytes));
        sm2EnvelopedVector.add(new DERBitString(encryptedPriKeyBytes));
        DERSequence sm2Enveloped = new DERSequence(sm2EnvelopedVector);
        String sm2EnvelopedB64 = Base64.encodeBase64String(sm2Enveloped.getEncoded(ASN1Encoding.DER));
        System.out.println("密文ASN1: " + sm2EnvelopedB64);
    }


}
