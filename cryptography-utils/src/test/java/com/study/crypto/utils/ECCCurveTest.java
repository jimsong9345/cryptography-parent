package com.study.crypto.utils;

import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.security.KeyPair;

/**
 * @author Songjin
 * @since 2024-07-17 13:44
 */
class ECCCurveTest {

    /**
     * # SM2 parameters
     * a  = 0xFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC
     * b  = 0x28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93
     * n  = 0xFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123
     *
     * Gx = 0x32c4ae2c1f1981195f9904466a39c9948fe30bbff2660be1715a4589334c74c7
     * Gy = 0xbc3736a2f4f6779c59bdcee36b692153d0a9877cc62a474002df32e52139f0a0
     * p  = 0xFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF
     * # Quick verify if(Gx, Gy) is on SM2 curve:
     * Gy**2 % p == (Gx**3 + Gx*a + b) % p
     */
    @Test
    void test02() throws Exception{
        BigInteger p = new BigInteger(1, Hex.decode("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF"));
        BigInteger a = new BigInteger(1, Hex.decode("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC"));
        BigInteger b = new BigInteger(1, Hex.decode("28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93"));
        KeyPair keyPair = KeyUtils.generateKeyPair(KeyUtils.ALGO_SM2);
        BCECPublicKey publicKey = (BCECPublicKey) keyPair.getPublic();
        BigInteger x = publicKey.getQ().getAffineXCoord().toBigInteger();
        BigInteger y = publicKey.getQ().getAffineYCoord().toBigInteger();
        BigInteger cubeX = x.multiply(x).multiply(x);
        BigInteger squreY = y.multiply(y);
        BigInteger left = squreY.mod(p);
        BigInteger right = (cubeX.add(a.multiply(x)).add(b)).mod(p);
        System.out.println(Hex.toHexString(left.toByteArray()));
        System.out.println(Hex.toHexString(right.toByteArray()));
        Assertions.assertEquals(left, right);
    }

    /**
     * # secp256k1 parameters
     * a  = 0
     * b  = 7
     * n  = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141
     *
     * Gx = 0x79BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798
     * Gy = 0x483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8
     * p  = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F
     * # Quick verify if(Gx, Gy) is on secp256k1 curve:
     * Gy**2 % p == (Gx**3 + Gx*a + b) % p
     */
    @Test
    void test01() throws Exception{
        BigInteger p = new BigInteger(1, Hex.decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F"));
        KeyPair keyPair = KeyUtils.generateKeyPair(KeyUtils.ALGO_ECC);
        BCECPublicKey publicKey = (BCECPublicKey) keyPair.getPublic();
        BigInteger x = publicKey.getQ().getAffineXCoord().toBigInteger();
        BigInteger y = publicKey.getQ().getAffineYCoord().toBigInteger();
        BigInteger cubeX = x.multiply(x).multiply(x);
        BigInteger squreY = y.multiply(y);
        BigInteger left = squreY.mod(p);
        BigInteger right = (cubeX.add(new BigInteger("7"))).mod(p);
        System.out.println(Hex.toHexString(left.toByteArray()));
        System.out.println(Hex.toHexString(right.toByteArray()));
        Assertions.assertEquals(left, right);
    }

}
