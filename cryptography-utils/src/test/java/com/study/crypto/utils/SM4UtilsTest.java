package com.study.crypto.utils;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Security;

/**
 * @author Songjin
 * @since 2022-08-15 16:56
 */
class SM4UtilsTest {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void testSM4Decrypt() throws Exception{
        byte[] encrypted = Base64.decodeBase64("Iso3l6glNVHoIj2qxv7b3A==");
        byte[] key = Base64.decodeBase64("Xxa+oFPldoq8XPHk9hktEw==");
        SecretKey secretKey = new SecretKeySpec(key, "SM4");
        Cipher cipher = Cipher.getInstance("SM4/ECB/PKCS5Padding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decrypted = cipher.doFinal(encrypted);
        System.out.println("对称密钥: Xxa+oFPldoq8XPHk9hktEw==");
        System.out.println("密文: Iso3l6glNVHoIj2qxv7b3A==");
        System.out.println("原文：" + new String(decrypted));
    }

    /**
     * SM4 加解密测试
     */
    @Test
    void testCrypto() throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("SM4", "BC");
        keyGenerator.init(128);
        SecretKey secretKey = keyGenerator.generateKey();
        Cipher cipher = Cipher.getInstance("SM4/ECB/PKCS5Padding", "BC");
        // 加密
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encrypted = cipher.doFinal("hello".getBytes());
        // 传入明文值为 hello
        System.out.println("SM4_ECB 加密:" + Base64.encodeBase64String(encrypted));
        // 解密
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decrypted = cipher.doFinal(encrypted);
        //encrypted 为加密后密文
        System.out.println("SM4_ECB 解密:" + new String(decrypted));
    }

}
