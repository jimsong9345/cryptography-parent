package com.study.crypto.signer.ofd;

import com.study.crypto.utils.KeyUtils;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.junit.jupiter.api.Test;
import org.ofdrw.reader.OFDReader;
import org.ofdrw.sign.OFDSigner;
import org.ofdrw.sign.SignMode;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;

/**
 * @author Songjin
 * @since 2021-03-10 19:19
 */
class OFDTest {
    
    @Test
    void testEncoding() throws Exception{
        byte[] ori = { (byte)145, 2, 3};
        String str = new String(ori, StandardCharsets.UTF_8);
        byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
        System.out.println(bytes);
    
        String str2 = "宋进";
        byte[] bytes1 = str2.getBytes(StandardCharsets.UTF_8);
        System.out.println(bytes1);
        String str3 = new String(bytes1, StandardCharsets.UTF_8);
        System.out.println(str3);
    }
    
    /**
     * 测试 ofd 消息签名
     */
    @Test
    void testOfdPkcsGmSign() throws Exception{
        byte[] keybytes = FileUtils.readFileToByteArray(new File("D:/11010800016981.keystore"));
        byte[] privateKeyBytes = ByteUtils.subArray(keybytes, 64);
        byte[] certbytes = FileUtils.readFileToByteArray(new File("D:/11010800016981.cer"));
        PrivateKey privateKey = KeyUtils.convertPrivateKey(privateKeyBytes);
        Certificate certificate = Certificate.getInstance(certbytes);

        Path src = Paths.get("src/test/resources", "helloworld.ofd");
        Path out = Paths.get("target/SM2DigitalSign.ofd");
        // 1. 构造签名引擎
        try (OFDReader reader = new OFDReader(src);
             OFDSigner signer = new OFDSigner(reader, out)) {
            // 最后参数 containContent，表示是否包含数据原文
            OfdPKCS7GmContainer signContainer = new OfdPKCS7GmContainer(certificate, privateKey, null, false);
            // 2. 设置签名模式
            signer.setSignMode(SignMode.ContinueSign);
            // 3. 设置签名使用的扩展签名容器
            signer.setSignContainer(signContainer);
            // 4. 执行签名
            signer.exeSign();
            // 5. 关闭签名引擎，生成文档。
        }
        System.out.println(">> 生成文件位置: " + out.toAbsolutePath().toAbsolutePath());
    }
    
}
