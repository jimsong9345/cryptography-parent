package com.study.crypto.dto.gb;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.dto.gb.base.ResponseGbDto;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

/**
 * 唯一赋码响应
 * @author Songjin
 * @since 2019/10/28 11:06
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class ResponseUniqueCodeDto extends ResponseGbDto {
	
	/**
	 * 国办生成的电子印章唯一赋码
	 */
	@JSONField(ordinal = 6)
	private ResponseUniqueCodeDtoData data;
	
	@Tolerate
	public ResponseUniqueCodeDto() {
		this.setVersion(GbConstants.VERSION);
		this.setTaskCode(GbConstants.APPLY_SEAL_CODE);
		this.setTaskTypeCode(GbConstants.TASK_TYPE_CODE_APPLY_SEAL_CODE);
	}
	
	@Builder
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ResponseUniqueCodeDtoData {
		/**
		 * 电子印章唯一赋码
		 */
        private String sealCode;
		
	}
}
