package com.study.crypto.dto.ca;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.dto.TaskIdDto;
import com.study.crypto.dto.ca.base.ResponseCaDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

/**
 * 证书申请响应
 * @author Songjin
 * @since 2019/10/28 20:14
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class ResponseSealCertDto extends ResponseCaDto {
	
	/**
	 * CA 系统返回的任务编号，用于查询任务结果时使用
	 */
	@JSONField(ordinal = 5)
	private TaskIdDto data;
	
	public ResponseSealCertDto() {
		this.setVersion(CaConstants.VERSION);
		this.setTaskCode(CaConstants.TASK_CODE_APPLY_SEAL_CERT);
		this.setData(new TaskIdDto());
	}
	
}
