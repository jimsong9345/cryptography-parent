package com.study.crypto.dto.ca;

import com.study.crypto.dto.SignInfoDto;
import com.study.crypto.dto.ca.base.RequestCaDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import java.util.ArrayList;
import java.util.List;

/**
 * 证书内容变更请求
 * @author Songjin
 * @since 2019/10/29 10:31
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class RequestChangeCertDto extends RequestCaDto {
	
	/**
	 * 电子印章证书申请信息列表
	 */
	private List<RequestChangeCertDtoData> data;
	
	public RequestChangeCertDto() {
		this.setData(new ArrayList<>());
		this.setVersion(CaConstants.VERSION);
		this.setTaskCode(CaConstants.TASK_CODE_CHANGE_SEAL_CERT);
		this.setSignInfo(new SignInfoDto());
	}
	
	@Data
	@Builder
	public static class RequestChangeCertDtoData {
		/**
		 * 用于区分请求列表的单个请求
		 */
		private String requestID;
		/**
		 * 证书类型：个人证书(0)、单位证书(1)、服务器证书(2)，大于 2 预留扩展
		 */
		private String certType;
		/**
		 * 需要变更的签名证书序列号
		 */
		private String serialNumber;
		/**
		 * 证书主体的国家项，2 个字节 CN
		 */
		private String countryName;
		/**
		 * 证书主体的组织项，不大于 150 个字节
		 */
		private String organizationName;
		/**
		 * 证书主体的名称项，不大于 200 个字节
		 */
		private String commonName;
		/**
		 * Base64 编码的签名公钥信息
		 */
		private String subjectPublicKeyInfo;
		/**
		 * SM2 椭圆曲线公钥密码算法 OID，1.2.156.10197.1.301
		 */
		private String algorithm;
		
		@Tolerate
		public RequestChangeCertDtoData() {
		}
	}
}
