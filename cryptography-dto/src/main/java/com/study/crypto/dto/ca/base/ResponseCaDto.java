package com.study.crypto.dto.ca.base;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.dto.SignInfoDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * ca请求响应超类
 *
 * @author Songjin
 * @since 2019/10/29 10:59
 */
@NoArgsConstructor
@SuperBuilder
@Data
public class ResponseCaDto {

    /**
     * 业务类型
     */
    @JSONField(ordinal = 1)
    private String taskCode;
    /**
     * 协议版本号，一般固定为：1.0
     */
    @JSONField(ordinal = 2)
    private String version;
    /**
     * 返回值，0 表示成功，其他表示错误
     */
    @JSONField(ordinal = 3)
    private String resultCode;
    /**
     * 返回信息描述
     */
    @JSONField(ordinal = 4)
    private String resultCodeMsg;
    /**
     * 签名信息
     */
    @JSONField(ordinal = 10)
    private SignInfoDto signInfo;
    
}
