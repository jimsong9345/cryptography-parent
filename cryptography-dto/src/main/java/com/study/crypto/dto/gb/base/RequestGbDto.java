package com.study.crypto.dto.gb.base;

import com.study.crypto.annotation.FixedValue;
import com.study.crypto.dto.SignInfoDto;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Songjin
 * @since 2019/10/25 19:11
 */
@SuperBuilder
@Data
public abstract class RequestGbDto {
    /**
     * 业务类型
     */
    @NotBlank(message = "taskCode参数为空")
    private String taskCode;
    
    /**
     * 协议版本号，一般固定为：1.0
     */
    @FixedValue(values = "1.0")
    private String version;
    
    /**
     * 客户端随机数(randomA，16字节)与服务端随机数(randomB，16字节)字符串拼接，中间无分割符号
     */
    @NotBlank(message = "tokenInfo参数为空")
    @Size(min = 32, max = 32, message = "tokenInfo参数固定长度32字节")
    private String tokenInfo;
    
    /**
     * 业务类型编码，0 表示备案业务，1 表示保护密钥更新业务，2 表示印章撤销，3 表示申请电子印章唯一赋码业务
     */
    @FixedValue(values = {"0", "1", "2", "3"})
    private String taskTypeCode;
    
    /**
     * 签名信息
     */
    @Valid
    @NotNull(message = "signInfo参数为空")
    private SignInfoDto signInfo;
    
    @Tolerate
    public RequestGbDto() {
    }
    
}
