package com.study.crypto.dto.ca;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.dto.ca.base.RequestCaDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

/**
 * Pkcs10 证书申请请求
 * @author Songjin
 * @since 2023-03-17 10:15
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class RequestPkcs10Dto extends RequestCaDto {

    /**
     * 证书申请信息列表
     */
    @JSONField(ordinal = 4)
    @NotNull(message = "data参数为空")
    @Valid
    private List<RequestPkcs10DtoData> data;

    @Tolerate
    public RequestPkcs10Dto() {
        this.setData(new ArrayList<>());
    }

    @Data
    @Builder
    public static class RequestPkcs10DtoData {
        /**
         * 用于区分请求列表的单个请求
         */
        @JSONField(ordinal = 1)
        @NotBlank(message = "requestId参数为空")
        private String requestId;
        /**
         * Base64 编码的 pkcs10 字符串
         */
        @JSONField(ordinal = 6)
        @NotBlank(message = "pkcs10参数为空")
        private String pkcs10;
        /**
         * 证书有效期起始时间，时间字符串格式为：yyyy-MM-dd HH:mm:ss
         */
        @JSONField(ordinal = 7)
        @NotBlank(message = "notBefore参数为空")
        @Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}", message = "日期格式不正确")
        private String notBefore;
        /**
         * 证书有效期结束时间，时间字符串格式为：yyyy-MM-dd HH:mm:ss
         */
        @JSONField(ordinal = 8)
        @NotBlank(message = "notAfter参数为空")
        @Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}", message = "日期格式不正确")
        private String notAfter;
        /**
         * SM2 椭圆曲线公钥密码算法 OID，1.2.156.10197.1.301
         */
        @JSONField(ordinal = 9)
        @NotBlank(message = "algorithm参数为空")
        private String algorithm;

        @Tolerate
        public RequestPkcs10DtoData() {
        }
    }
}
