package com.study.crypto.dto.gb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 电子印章状态信息推送：身份认证请求
 * @author Songjin
 * @since 2021-07-10 1:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestAuthenticateDto {
	
	/**
	 * 电子印章状态发布系统证书，即发起电子印章推送请求的系统的证书
	 */
	@NotBlank(message = "idA参数为空")
	private String idA;
	/**
	 * 电子印章状态发布系统生成的 16 位随机数，用于确定该次请求
	 */
	@Size(min = 16, max = 16, message = "randomA参数固定长度16字节")
	private String randomA;
	
}
