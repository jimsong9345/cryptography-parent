package com.study.crypto.dto.gb;

import com.study.crypto.dto.gb.base.RequestGbDto;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 上传备案信息请求
 * @author Songjin
 * @since 2019/10/25 19:01
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class RequestSealRecordDto extends RequestGbDto {
	
	/**
	 * 电子印章备案信息列表
	 */
	@NotNull(message = "data参数为空")
	@Valid
	private List<RequestSealRecordDtoData> data;
	
	@Tolerate
	public RequestSealRecordDto() {
		this.setVersion(GbConstants.VERSION);
		this.setTaskCode(GbConstants.UPLOAD_RECORD_INFOS);
		this.setTaskTypeCode(GbConstants.TASK_TYPE_CODE_UPLOAD_RECORD_INFOS);
		this.setData(new ArrayList<>());
	}
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RequestSealRecordDtoData {
		
		/** BASE64 编码的电子印章加密证书 */
		@NotBlank(message = "sealEncCert参数为空")
		private String sealEncCert;
		
		/** BASE64 编码的电子印章签名证书 */
		@NotBlank(message = "sealSignCert参数为空")
		private String sealSignCert;
		
		/** 电子印章的备案数据，备案的数据内容见附录 B.1 */
		@NotNull(message = "sealData参数为空")
		@Valid
		private SealDataDto sealData;
		
		/** 行政区划代码 */
		@NotBlank(message = "districtCode参数为空")
		private String districtCode;
		
	}

}
