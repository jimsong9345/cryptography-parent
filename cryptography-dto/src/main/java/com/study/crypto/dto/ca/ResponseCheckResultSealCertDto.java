package com.study.crypto.dto.ca;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.study.crypto.dto.ca.base.ResponseCaDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import java.util.ArrayList;
import java.util.List;

/**
 * 使用taskid查询接口调用结果：证书申请请求
 * @author Songjin
 * @since 2019/10/29 11:16
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class ResponseCheckResultSealCertDto extends ResponseCaDto {
	
	/**
	 * 返回的业务包列表
	 */
	@JSONField(ordinal = 5)
	private List<CheckResultSealCertDtoData> data;
	
	public ResponseCheckResultSealCertDto() {
		this.setVersion(CaConstants.VERSION);
		this.setData(new ArrayList<>());
		this.setTaskCode(CaConstants.CHECK_RESULT);
	}
	
	@Builder
	@Data
	public static class CheckResultSealCertDtoData {
		/**
		 * 用于区分请求列表单个请求的业务编码(与请求对应)
		 */
		@JSONField(ordinal = 1)
		private String requestID;
		/**
		 * 每个申请包处理的返回结果，0 是成功，其他表示错误
		 */
		@JSONField(ordinal = 2)
		private String errorCode;
		/**
		 * 数据包
		 */
		@JSONField(ordinal = 3, name = "package")
		@JsonProperty("package")
		private CheckResultSealCertDtoPackage package_;
		
		@Tolerate
		public CheckResultSealCertDtoData() {
		}
	}
	
	@Builder
	@Data
	public static class CheckResultSealCertDtoPackage {
		/**
		 * Base64 编码签名证书
		 */
		@JSONField(ordinal = 1)
		private String signCert;
		/**
		 * Base64 编码的加密证书
		 */
		@JSONField(ordinal = 2)
		private String encCert;
		/**
		 * Base64 编码的加密密钥对保护结构，被编码的结构 ENVELOPEDKEYBLOB，遵从 GM/T 0016—2012 标准
		 */
		@JSONField(ordinal = 3)
		private String encKeyProtection;
        /**
         * 管理员PIN码，UKEY印章的时候使用此字段更新智能密码钥匙的管理员PIN码值
         */
        private String envsnCode;
		
        @Tolerate
		public CheckResultSealCertDtoPackage() {
		}
	}
	
}
