package com.study.crypto.dto.gb;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 电子印章状态信息推送：身份认证响应
 * @author Songjin
 * @since 2019/10/28 14:00
 */
@Data
@NoArgsConstructor
public class ResponseAuthenticateDto {
	
	/**
	 * 获取随机数成功为"0"
	 */
	private String result;
	/**
	 * 返回的描述信息
	 */
	private String msg;
	/**
	 * 随机数，签名值，国家政务服务平台电子印章状态发布系统证书拼接的字符串
	 * <p>idB||randomB||randomA||signValue</p>
	 * <p>idB 是国家政务服务平台电子印章状态发布系统证书</p>
	 * <p>randomB 是国家政务服务平台电子印章状态发布系统生成的 16 位字符串</p>
	 * <p>randomA 是国家政务服务平台电子印章状态发布系统请求的 16 位随机数</p>
	 * <p>signValue 是国家政务服务平台电子印章状态发布系统的内部签名。</p>
	 * <p>签名明文为 idB||randomB||randomA</p>
	 */
	private String token;
	
	public ResponseAuthenticateDto(String result, String msg, String token) {
		this.result = result;
		this.msg = msg;
		this.token = token;
	}
}
