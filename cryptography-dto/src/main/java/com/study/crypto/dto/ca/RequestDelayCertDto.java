package com.study.crypto.dto.ca;

import com.study.crypto.dto.SignInfoDto;
import com.study.crypto.dto.ca.base.RequestCaDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * 证书延期请求
 * @author Songjin
 * @since 2019/10/29 10:33
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class RequestDelayCertDto extends RequestCaDto {
	
	/**
	 * 证书延期申请信息列表
	 */
	private List<RequestDelayCertDtoData> data;
	
	public RequestDelayCertDto() {
		this.setData(new ArrayList<RequestDelayCertDtoData>());
		this.setVersion(CaConstants.VERSION);
		this.setTaskCode(CaConstants.TASK_CODE_DELAY_SEAL_CERT);
		this.setSignInfo(new SignInfoDto());
	}
	
	@Data
	@Builder
	public static class RequestDelayCertDtoData {
		/**
		 * 用于区分请求列表单个请求的业务编码
		 */
		private String requestID;
		/**
		 * 签名证书序列号
		 */
		private String serialNumber;
		/**
		 * Base64 编码的签名公钥信息
		 */
		private String subjectPublicKeyInfo;
		
	}
}
