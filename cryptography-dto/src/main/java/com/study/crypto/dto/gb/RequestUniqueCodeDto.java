package com.study.crypto.dto.gb;

import com.study.crypto.dto.gb.base.RequestGbDto;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 唯一赋码请求
 * @author Songjin
 * @since 2019/10/28 10:58
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class RequestUniqueCodeDto extends RequestGbDto {
	
	/**
	 * 唯一赋码请求数据
	 */
	@Valid
	@NotNull(message = "data参数为空")
	private RequestUniqueCodeDtoData data;
	
	@Tolerate
	public RequestUniqueCodeDto() {
		this.setVersion(GbConstants.VERSION);
		this.setTaskCode(GbConstants.APPLY_SEAL_CODE);
		this.setTaskTypeCode(GbConstants.TASK_TYPE_CODE_APPLY_SEAL_CODE);
	}
	
	@Builder
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RequestUniqueCodeDtoData {
		/**
		 * 行政区划代码
		 */
		@NotBlank(message = "areaNumber参数为空")
		@Size(min = 3, max = 3, message = "areaNumber参数长度3字节")
		private String areaNumber;
		
	}
}
