package com.study.crypto.dto.ca;

import com.study.crypto.dto.ca.base.ResponseCaDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * 使用taskid查询接口调用结果：证书吊销请求
 * @author Songjin
 * @since 2019/10/29 11:25
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class ResponseCheckResultRevokeCertDto extends ResponseCaDto {
	
	/**
	 * 返回的业务包列表
	 */
	private List<CheckResultRevokeCertDtoData> data;
	
	public ResponseCheckResultRevokeCertDto() {
		this.setData(new ArrayList<CheckResultRevokeCertDtoData>());
		this.setVersion(CaConstants.VERSION);
		this.setTaskCode(CaConstants.CHECK_RESULT);
	}
	
	@Builder
	@Data
	public static class CheckResultRevokeCertDtoData {
		/**
		 * 用于区分请求列表单个请求的业务编码(与请求对应)
		 */
		private String requestID;
		/**
		 * 每个申请包处理的返回结果，0 是成功，其他表示错误
		 */
		private String errorCode;
		
	}
}
