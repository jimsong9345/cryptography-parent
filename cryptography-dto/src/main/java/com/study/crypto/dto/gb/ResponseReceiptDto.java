package com.study.crypto.dto.gb;

import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

/**
 * 确认回执响应
 * @author Songjin
 * @since 2019/10/28 13:25
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class ResponseReceiptDto extends ResponsePoliceDto {
	
	@Tolerate
	public ResponseReceiptDto() {
		this.setVersion(GbConstants.VERSION);
		this.setTaskCode(GbConstants.CONFIRM_RECEIPT);
	}
	
	@Tolerate
	public ResponseReceiptDto(String taskTypeCode) {
		this();
		this.setTaskTypeCode(taskTypeCode);
	}
}