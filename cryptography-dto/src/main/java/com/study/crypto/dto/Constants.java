package com.study.crypto.dto;

/**
 * @author Songjin
 * @since 2021-01-26 17:48
 */
public class Constants {
    
    /**
     * 验签失败
     */
    public static final String VERIFY_FAILED = "20301";
}
