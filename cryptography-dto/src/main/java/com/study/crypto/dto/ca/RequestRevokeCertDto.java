package com.study.crypto.dto.ca;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.dto.ca.base.RequestCaDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import java.util.ArrayList;
import java.util.List;

/**
 * 证书吊销请求
 * @author Songjin
 * @since 2019/10/29 10:30
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class RequestRevokeCertDto extends RequestCaDto {
	
	/**
	 * 证书吊销申请信息列表
	 */
	@JSONField(ordinal = 4)
	private List<RequestRevokeCertDtoData> data;
	
	@Tolerate
	public RequestRevokeCertDto() {
		this.setVersion(CaConstants.VERSION);
		this.setTaskCode(CaConstants.TASK_CODE_REVOKE_SEAL_CERT);
		this.setData(new ArrayList<RequestRevokeCertDtoData>());
	}
	
	@Builder
	@Data
	public static class RequestRevokeCertDtoData {
		/**
		 * 用于区分请求列表单个请求的业务编码
		 */
		@JSONField(ordinal = 1)
		private String requestID;
		/**
		 * 签名证书序列号
		 */
		@JSONField(ordinal = 2)
		private String serialNumber;
		
	}
}
