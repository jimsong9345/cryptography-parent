package com.study.crypto.dto.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author Songjin
 * @since 2021-05-30 10:46
 */
@Data
@AllArgsConstructor
@Builder
public class ReturnResult<T> {
    
    private static final long   serialVersionUID = 6948768594132295784L;
    /** 状态码 */
    private int status;
    /** 提示信息 */
    private String message;
    /** 请求流水号 */
    private String transId;
    /** 数据 */
    private T data;
    /** 错误信息 */
    private String moreInfo;
    
    public ReturnResult() {
    }
    
    public ReturnResult(int status) {
        this(status, null);
    }
    
    public ReturnResult(int status, String message) {
        this(status, message, null);
    }
    
    public ReturnResult(int status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
    
    /**
     * 构建一个成功结果的对象
     * @param message 消息
     * @return ReturnResult
     */
    public static ReturnResult<String> success(String message) {
        return build(200, message, null);
    }
    
    /**
     * 构建一个成功结果的对象
     * @param message 消息
     * @param data 数据
     * @return ReturnResult
     * @param <T> 泛型参数
     */
    public static <T> ReturnResult<T> success(String message, T data) {
        return build(200, message, data);
    }
    
    /**
     * 构建一个成功结果的对象
     * @param data 数据
     * @return ReturnResult
     * @param <T> 泛型参数
     */
    public static <T> ReturnResult<T> success(T data) {
        return build(200, "SUCCESS", data);
    }
    
    /**
     * 构建一个成功结果的对象
     *
     * @param transId 事务id
     * @param data 数据
     * @return ReturnResult
     * @param <T> 泛型参数
     */
    public static <T> ReturnResult<T> successWithTransId(String transId, T data) {
        ReturnResult<T> success = build(200, "SUCCESS", data);
        success.setTransId(transId);
        return success;
    }
    
    /**
     * 构建一个结果的对象
     * @param status 状态
     * @param message 消息
     * @param data 数据
     * @return ReturnResult
     * @param <T> 泛型参数
     */
    public static <T> ReturnResult<T> build(int status, String message, T data) {
        ReturnResult<T> vo = new ReturnResult<>(status);
        vo.setMessage(message);
        vo.setData(data);
        return vo;
    }
}
