package com.study.crypto.dto.ca;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.annotation.FixedValue;
import com.study.crypto.dto.ca.base.RequestCaDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

/**证书申请请求，支持传入多个 dn 子项
 * @author Songjin
 * @since 2022-10-20 18:06
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class RequestSealCertDistinctNameDto extends RequestCaDto {
	
	/**
	 * 证书申请信息列表
	 */
	@JSONField(ordinal = 4)
	@NotNull(message = "data参数为空")
	@Valid
	private List<RequestSealCertDistinctNameDtoData> data;
	
	@Tolerate
	public RequestSealCertDistinctNameDto() {
		this.setData(new ArrayList<>());
	}
	
	@Data
	@Builder
	public static class RequestSealCertDistinctNameDtoData {
		/**
		 * 用于区分请求列表的单个请求
		 */
		@JSONField(ordinal = 1)
		@NotBlank(message = "requestId参数为空")
		private String requestId;
		/**
		 * 证书类型：个人证书(0)、单位证书(1)、服务器证书(2)，大于 2 预留扩展
		 */
		@JSONField(ordinal = 2)
		@FixedValue(values = {"0", "1", "2"})
		private String certType;
		/**
		 * 证书 distinct name，包含多个 oid 键值对。如 C、CN、O、OU 等等
		 */
		@JSONField(ordinal = 3)
		@NotBlank(message = "dn参数为空")
		private String dn;
		/**
		 * Base64 编码的签名公钥信息
		 */
		@JSONField(ordinal = 6)
		@NotBlank(message = "subjectPublicKeyInfo参数为空")
		private String subjectPublicKeyInfo;
		/**
		 * 证书有效期起始时间，时间字符串格式为：yyyy-MM-dd HH:mm:ss
		 */
		@JSONField(ordinal = 7)
		@NotBlank(message = "notBefore参数为空")
		@Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}", message = "日期格式不正确")
		private String notBefore;
		/**
		 * 证书有效期结束时间，时间字符串格式为：yyyy-MM-dd HH:mm:ss
		 */
		@JSONField(ordinal = 8)
		@NotBlank(message = "notAfter参数为空")
		@Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}", message = "日期格式不正确")
		private String notAfter;
		/**
		 * 公钥密码算法 OID
		 */
		@JSONField(ordinal = 9)
		@NotBlank(message = "algorithm参数为空")
		@FixedValue(values = {"1.2.156.10197.1.301", "1.2.840.113549.1.1.1"})
		private String algorithm;
		
		@Tolerate
		public RequestSealCertDistinctNameDtoData() {
		}
		
	}
}
