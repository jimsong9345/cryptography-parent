package com.study.crypto.dto.gb;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 印模数据
 * @author Songjin
 * @since 2019/10/28 10:25
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class SealDataDto {
	
	/** 印章名称 */
	private String yzmc;
	
	/** 印章编码。省市区(6个字节)+印章赋码(8个字节)，如11010100000001，为 14 个字节 */
	private String yzbm;
	
	/** 印章制作单位编码。印章制作单位的统一社会信用代码，为 18 个字节*/
	private String yzzzdwbm;
	
	/** 印章类型代码 */
	private String yzlxdm;
	
	/** 经办人姓名 */
	private String jbr_xm;
	
	/** 经办人证件类型。默认身份证，111 */
	private String jbr_zjlx;
	
	/** 经办人证件号码 jbr_zjhm String 身份证号码，最大 20 个字节*/
	private String jbr_zjhm;
	
	/** 印章制作日期，格式如下：2018-11-07 10:09:07 最大长度20字节 */
	private String zzrq;
	
	/** 印模数据，最大为30K，参见标准 */
	private String ymsj;
	
	/** 印章使用单位_单位名称 */
	private String yzsydw_dwmc;
	
	/** 印章使用单位_单位少数民族文字名称 */
	private String yzsydw_dwssmzwzmc;
	
	/** 印章使用单位_单位英文名称 */
	private String yzsydw_dwywmc;
	
	/** 印章制作单位_单位名称 */
	private String yzzzdw_dwmc;
	
	/** 印章制作单位_单位少数民族文字名称 */
	private String yzzzdw_dwssmzwzmc;
	
	/** 印章制作单位_单位英文名称 */
	private String yzzzdw_dwywmc;
	
	/** 印章使用单位_统一社会信用代码 */
	private String yzsydw_tyshxydm;
	
}
