package com.study.crypto.dto.ca;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

/**
 * Pkcs10 证书申请请求响应
 * @author Songjin
 * @since 2023-03-17 10:26
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class ResponsePkcs10Dto extends ResponseCheckResultSealCertDto {

    @Tolerate
    public ResponsePkcs10Dto() {
    }
}
