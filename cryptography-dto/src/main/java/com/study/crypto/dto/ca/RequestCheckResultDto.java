package com.study.crypto.dto.ca;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.annotation.FixedValue;
import com.study.crypto.dto.SignInfoDto;
import com.study.crypto.dto.TaskIdDto;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 使用taskId查询接口执行结果请求
 * @author Songjin
 * @since 2019/10/28 20:50
 */
@Builder
@Data
public class RequestCheckResultDto {
	
	/**
	 * 业务类型
	 */
	@JSONField(ordinal = 1)
	@FixedValue(values = CaConstants.CHECK_RESULT)
	private String taskCode;
	/**
	 * 协议版本号，一般固定为：1.0
	 */
	@JSONField(ordinal = 2)
	@FixedValue(values = CaConstants.VERSION)
	private String version;
	/**
	 * CA 系统返回的任务编号，用于查询任务结果时使用
	 */
	@JSONField(ordinal = 3)
	@Valid
	private TaskIdDto data;
	/**
	 * 签名信息
	 */
	@JSONField(ordinal = 4)
	@NotNull(message = "signInfo参数为空")
	@Valid
	private SignInfoDto signInfo;
	
	@Tolerate
	public RequestCheckResultDto() {
		this.setData(new TaskIdDto());
	}
	
}
