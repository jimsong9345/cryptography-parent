package com.study.crypto.dto.gb;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.dto.TaskIdDto;
import com.study.crypto.dto.gb.base.ResponseGbDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

/**
 * 上传备案信息响应
 * @author Songjin
 * @since 2019/10/25 19:01
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class ResponseSealRecordDto extends ResponseGbDto {
	
	/**
	 * 任务编号
	 */
    @JSONField(ordinal = 6)
    private TaskIdDto data;
	
	public ResponseSealRecordDto() {
		this.setVersion(GbConstants.VERSION);
		this.setTaskCode(GbConstants.UPLOAD_RECORD_INFOS);
		this.setTaskTypeCode(GbConstants.TASK_TYPE_CODE_UPLOAD_RECORD_INFOS);
	}
	
	/**
	 * 制章请求返回值构造器
	 * @param taskId        任务标识/印章标识
	 * @param resultCode    结果代码
	 * @param resultCodeMsg 结果代码说明
	 */
	public ResponseSealRecordDto(String taskId, String resultCode, String resultCodeMsg) {
		this.setVersion(GbConstants.VERSION);
		this.setTaskCode(GbConstants.SEALMAKING);
		this.setTaskTypeCode(GbConstants.TASK_TYPE_CODE_SEAL_MAKING_CODE);
		this.setData(new TaskIdDto(taskId));
		this.setResultCode(resultCode);
		this.setResultCodeMsg(resultCodeMsg);
	}

}

