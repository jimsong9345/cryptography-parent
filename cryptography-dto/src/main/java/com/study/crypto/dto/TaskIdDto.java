package com.study.crypto.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Songjin
 * @since 2019/10/25 19:53
 */
@Data
public class TaskIdDto {

	public TaskIdDto() {
	}

	public TaskIdDto(String taskId) {
		this.taskId = taskId;
	}

	/**
	 * 国家政务服务平台电子印章信任支撑系统返回的任务编号
	 */
	@NotBlank(message = "taskId参数为空")
	private String taskId;
	
}
