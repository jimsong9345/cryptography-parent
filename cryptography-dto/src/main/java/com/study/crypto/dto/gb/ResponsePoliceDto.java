package com.study.crypto.dto.gb;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.study.crypto.dto.gb.base.ResponseGbDto;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import java.util.ArrayList;
import java.util.List;

/**
 * 下载公安下发数据响应
 * @author Songjin
 * @since 2019/10/28 10:37
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class ResponsePoliceDto extends ResponseGbDto {
	
	/**
	 * 针对每一个任务编号taskId的处理结果
	 */
	@JSONField(ordinal = 6)
	private List<ResponsePoliceDtoData> data;
	
	@Tolerate
	public ResponsePoliceDto() {
		this.setVersion(GbConstants.VERSION);
		this.setTaskCode(GbConstants.CHECK_RESULT);
		this.setData(new ArrayList<>());
	}
	
	@Tolerate
	public ResponsePoliceDto(String taskTypeCode) {
		this();
		this.setTaskTypeCode(taskTypeCode);
	}
	
	@Builder
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ResponsePoliceDtoData {
		/**
		 * 每个申请包处理的返回结果，0 是成功，其他表示错误
		 */
		@JSONField(ordinal = 1)
		private String errorCode;
		/**
		 * 印章编码
		 */
		@JSONField(ordinal = 3)
		private String yzbm;
		/**
		 * 公安下发的数据信息，具体数据内容见附录 B.2
		 */
		@JSONField(ordinal = 2,name = "package")
		@JsonProperty("package")
		private ResponsePoliceDtoDataPackage package_;
	}

	@Builder
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ResponsePoliceDtoDataPackage {
		/**
		 * 印章编码
		 */
		@JSONField(ordinal = 1)
		private String yzbm;
		/**
		 * BASE64 编码的应用维护对称密钥密文
		 */
		@JSONField(ordinal = 2)
		private String appSymKeyEnc;
		/**
		 * BASE64 编码的数据加密对称密钥密文
		 */
		@JSONField(ordinal = 3)
		private String dataSymKeyEnc;
		/**
		 * 公安下发的加密文件
		 */
		@JSONField(ordinal = 4)
		private String encFile;
		
	}
}
