package com.study.crypto.dto;

import com.study.crypto.dto.gb.GbConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * 获取随机数接口响应
 * @author Songjin
 * @since 2019/10/28 11:15
 */
@Builder
@Data
@AllArgsConstructor
public class ResponseRandomDto {
	
	/**
	 * 业务类型
	 */
	private String taskCode;
	/**
	 * 协议版本号，一般固定为：1.0
	 */
	private String version;
	/**
	 * 返回值，0 表示成功，其他表示错误
	 */
	private String resultCode;
	/**
	 * 返回信息描述
	 */
	private String resultCodeMsg;
	/**
	 * 国家政务服务平台电子印章信任支撑系统产生随机数，长度为 16 个字节
	 */
	private String randomB;
	
	@Tolerate
	public ResponseRandomDto() {
		this.version = GbConstants.VERSION;
		this.taskCode = GbConstants.APPLY_RANDOM_GB;
		this.resultCode = GbConstants.SUCCESS_INNER;
	}
}
