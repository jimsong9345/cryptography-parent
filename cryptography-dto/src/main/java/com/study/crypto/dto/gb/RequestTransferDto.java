package com.study.crypto.dto.gb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 电子印章状态信息推送：数据传输请求
 * @author Songjin
 * @since 2019/10/28 13:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestTransferDto {
	
	/**
	 * 由 idA、16 位返回的随机数、业务数据 text、签名值拼接而成，格式为：
	 * idA+||+randomB+||+text+||+signValue，其中 signValue 是对
	 * idA+||+randomB+||+text 做的 SM2 签名
	 */
	private String data;
	
}
