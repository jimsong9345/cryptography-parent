package com.study.crypto.dto.gb;

import com.study.crypto.annotation.FixedValue;
import com.study.crypto.dto.SignInfoDto;
import com.study.crypto.dto.TaskIdDto;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 查询并下载公安下发数据请求
 * @author Songjin
 * @since 2019/10/28 10:19
 */
@Builder
@Data
public class RequestPoliceDto {
	
	/**
	 * 业务类型
	 */
	@NotBlank(message = "taskCode参数为空")
	private String taskCode;
	
	/**
	 * 协议版本号，一般固定为：1.0
	 */
	@FixedValue(values = "1.0")
	private String version;
	
	/**
	 * 业务类型编码，0 表示备案业务，1 表示保护密钥更新业务，2 表示印章撤销，3 表示申请电子印章唯一赋码业务
	 */
	@FixedValue(values = {"0", "1", "2", "3"})
	private String taskTypeCode;
	
	/**
	 * 任务编号
	 */
	@NotNull(message = "data参数为空")
	@Valid
	private TaskIdDto data;
	
	/**
	 * 签名信息
	 */
	@Valid
	@NotNull(message = "signInfo参数为空")
	private SignInfoDto signInfo;
	
	@Tolerate
	public RequestPoliceDto() {
		this.setVersion(GbConstants.VERSION);
		this.setTaskCode(GbConstants.CHECK_RESULT);
		this.setTaskTypeCode(GbConstants.TASK_TYPE_CODE_UPLOAD_RECORD_INFOS);
	}
	
	@Tolerate
	public RequestPoliceDto(String taskTypeCode, TaskIdDto data) {
		this();
		this.setTaskTypeCode(taskTypeCode);
		this.setData(data);
	}
	
}
