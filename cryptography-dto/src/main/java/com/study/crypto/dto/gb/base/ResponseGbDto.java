package com.study.crypto.dto.gb.base;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.dto.SignInfoDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Songjin
 * @since 2019/10/25 19:47
 */
@SuperBuilder
@Data
@NoArgsConstructor
public class ResponseGbDto {
	/**
	 * 业务类型
	 */
	@JSONField(ordinal = 1)
	private String taskCode;
	/**
	 * 协议版本号，一般固定为：1.0
	 */
	@JSONField(ordinal = 2)
	private String version;
	/**
	 * 返回值，0 表示成功，其他表示错误
	 */
	@JSONField(ordinal = 3)
	private String resultCode;
	/**
	 * 返回信息描述
	 */
	@JSONField(ordinal = 4)
	private String resultCodeMsg;
	/**
	 * 业务类型编码，0 表示备案业务，1 表示保护密钥更新业务，2 表示印章撤销，3 表示申请电子印章唯一赋码业务
	 */
	@JSONField(ordinal = 5)
	private String taskTypeCode;
	/**
	 * 签名信息
	 */
	@JSONField(ordinal = 7)
	private SignInfoDto signInfo;
	
}
