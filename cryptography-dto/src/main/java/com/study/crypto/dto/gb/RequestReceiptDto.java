package com.study.crypto.dto.gb;

import com.study.crypto.dto.gb.base.RequestGbDto;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import java.util.ArrayList;
import java.util.List;

/**
 * 确认回执请求
 * @author Songjin
 * @since 2019/10/28 11:17
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class RequestReceiptDto extends RequestGbDto {
	
	/**
	 * 确认回执信息列表
	 */
	private List<RequestReceiptDtoData> data;
	
	@Tolerate
	public RequestReceiptDto() {
		this.setVersion(GbConstants.VERSION);
		this.setTaskCode(GbConstants.CONFIRM_RECEIPT);
		this.setData(new ArrayList<>());
		this.setTaskTypeCode(GbConstants.TASK_TYPE_CODE_UPLOAD_RECORD_INFOS);
	}
	
	@Tolerate
	public RequestReceiptDto(String taskTypeCode) {
		this();
		this.setTaskTypeCode(taskTypeCode);
	}
	
	@Builder
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RequestReceiptDtoData {
		/**
		 * 在一项业务处理中，国家政务服务平台电子印章信任支撑系统返回的业务编号，用于串联上下文
		 */
		private String taskId;
		/**
		 * 印章编码
		 */
		private String yzbm;
		/**
		 * 设备编码
		 */
		private String deviceCode;
		/**
		 * BASE64 编码的设备编码密文
		 */
		private String encDeviceCode;
		
	}
}
