package com.study.crypto.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.apache.commons.codec.binary.Base64;

import javax.validation.constraints.NotBlank;

/**
 * 签名数据
 * @author Songjin
 * @since 2019/10/25 19:13
 */
@Builder
@Data
public class SignInfoDto {
	
	/**
	 * 签名算法 oid 字符串，1.2.156.10197.1.501(默认) 或者 1.2.156.10197.1.301
	 */
	@JSONField(ordinal = 1)
	@NotBlank(message = "signAlgorithm参数为空")
	private String signAlgorithm;
	/**
	 * <ul>
	 *     <li>签名值的base64编码,编码前的签名值长度为64字节，其中r和s分别为32字节</li>
	 *     <li>该签名值是由制章系统的签名私钥进行签名生成的</li>
	 * </ul>
	 */
	@JSONField(ordinal = 2)
	@NotBlank(message = "signValue参数为空")
	private String signValue;
	
	@Tolerate
	public SignInfoDto() {
	}
	
	@Tolerate
	public SignInfoDto(String oid, byte[] signature) {
		this.signAlgorithm = oid;
		this.signValue = Base64.encodeBase64String(signature);
	}
}
