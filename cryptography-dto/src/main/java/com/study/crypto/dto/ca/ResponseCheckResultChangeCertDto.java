package com.study.crypto.dto.ca;

import com.study.crypto.dto.SignInfoDto;

import java.util.ArrayList;

/**
 * @author Songjin
 * @since 2019/10/29 11:40
 */
public class ResponseCheckResultChangeCertDto extends ResponseCheckResultSealCertDto {
	
	public ResponseCheckResultChangeCertDto() {
		this.setVersion(CaConstants.VERSION);
		this.setData(new ArrayList<CheckResultSealCertDtoData>());
		this.setTaskCode(CaConstants.CHECK_RESULT);
		this.setSignInfo(new SignInfoDto());
	}
}
