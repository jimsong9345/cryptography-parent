package com.study.crypto.dto.ca;

/**
 * CA相关常量
 * @author Songjin
 * @since 2019/10/28 20:11
 */
public class CaConstants {

	private CaConstants() {
	}

	/**
	 * 协议版本号 1.0
	 */
	public static final String VERSION = "1.0";
	/**
	 * 业务类型：获取随机数接口CA
	 */
	public static final String TASK_CODE_APPLY_RANDOM_CA = "applyServiceRandom";
	/**
	 * 业务类型：证书申请请求
	 */
	public static final String TASK_CODE_APPLY_SEAL_CERT = "applySealCert";
	/**
	 * 业务类型：证书吊销请求
	 */
	public static final String TASK_CODE_REVOKE_SEAL_CERT = "revokeSealCert";
	/**
	 * 业务类型：证书内容变更
	 */
	public static final String TASK_CODE_CHANGE_SEAL_CERT = "changeSealCert";
	/**
	 * 业务类型：证书延期请求
	 */
	public static final String TASK_CODE_DELAY_SEAL_CERT = "delaySealCert";
	/**
	 * 签名算法 oid 字符串
	 */
	public static final String SIGN_ALGORITHM = "1.2.156.10197.1.301";
	/**
	 * 异步请求获取请求结果命令编码
	 * <ul>
	 *     <li>使用taskId查询下载证书业务参数</li>
	 *     <li>使用taskId查询证书吊销业务参数</li>
	 * </ul>
	 */
	public static final String CHECK_RESULT = "checkResult";
	
	/**
	 * 证书类型：个人证书(说明，将证书类型参数设置为个人证书接口报错)
	 */
	public static final String CERT_TYPE_INDIVIDUAL = "0";
	/**
	 * 证书类型：单位证书
	 */
	public static final String CERT_TYPE_CORPORATE = "1";
	/**
	 * 证书类型：服务器证书
	 */
	public static final String CERT_TYPE_SERVER = "2";
	
	// --以下常量为证书申请相关状态码--------------------------------------------------------------------------------
	/**
	 * 获取随机数失败
	 */
	public static final String APPLY_RANDOM_FAILED = "20001";
	/**
	 * 调用申请证书接口，获取taskId失败
	 */
	public static final String APPLY_SEAL_CERT_FAILED = "20002";
	/**
	 * 调用吊销证书接口，获取taskId失败
	 */
	public static final String REVOKE_SEAL_CERT_FAILED = "20003";
	/**
	 * 离线ca: 上传文件数据不正确
	 */
	public static final String OFFLINE_CA_UPLOAD_ERROR = "20004";
	
	// --以下常量为C0122文档中证书申请相关状态码----------------------------------------------------------------------
	/**
	 * 返回值，0 表示成功，其他表示错误
	 */
	public static final String SUCCESS = "0";
	/**
	 * 返回值，错误
	 */
	public static final String ERROR = "-1";
}
