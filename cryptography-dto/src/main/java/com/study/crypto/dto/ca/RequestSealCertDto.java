package com.study.crypto.dto.ca;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.annotation.FixedValue;
import com.study.crypto.dto.ca.base.RequestCaDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * 证书申请请求
 * @author Songjin
 * @since 2019/10/28 20:36
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class RequestSealCertDto extends RequestCaDto {
	
	/**
	 * 证书申请信息列表
	 */
	@JSONField(ordinal = 4)
	@NotNull(message = "data参数为空")
	@Valid
	private List<RequestSealCertDtoData> data;
	
	@Tolerate
	public RequestSealCertDto() {
		this.setData(new ArrayList<>());
	}
	
	@Data
	@Builder
	public static class RequestSealCertDtoData {
		/**
		 * 用于区分请求列表的单个请求
		 */
		@JSONField(ordinal = 1)
		@NotBlank(message = "requestId参数为空")
		private String requestId;
		/**
		 * 证书类型：个人证书(0)、单位证书(1)、服务器证书(2)，大于 2 预留扩展
		 */
		@JSONField(ordinal = 2)
		@FixedValue(values = {"0", "1", "2"})
		private String certType;
		/**
		 * 证书主体的国家项，2 个字节 CN
		 */
		@JSONField(ordinal = 3)
		@NotBlank(message = "countryName参数为空")
		@Size(min = 2, max = 2, message = "证书主体国家项，2字节")
		private String countryName;
		/**
		 * 证书主体的组织项，不大于 150 个字节
		 */
		@JSONField(ordinal = 4)
		@NotBlank(message = "organizationName参数为空")
		private String organizationName;
		/**
		 * 证书主体的名称项，不大于 200 个字节
		 */
		@JSONField(ordinal = 5)
		@NotBlank(message = "commonName参数为空")
		private String commonName;
		/**
		 * Base64 编码的签名公钥信息
		 */
		@JSONField(ordinal = 6)
		@NotBlank(message = "subjectPublicKeyInfo参数为空")
		private String subjectPublicKeyInfo;
		/**
		 * 证书有效期起始时间，时间字符串格式为：yyyy-MM-dd HH:mm:ss
		 */
		@JSONField(ordinal = 7)
		@NotBlank(message = "notBefore参数为空")
		@Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}", message = "日期格式不正确")
		private String notBefore;
		/**
		 * 证书有效期结束时间，时间字符串格式为：yyyy-MM-dd HH:mm:ss
		 */
		@JSONField(ordinal = 8)
		@NotBlank(message = "notAfter参数为空")
		@Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}", message = "日期格式不正确")
		private String notAfter;
		/**
		 * SM2 椭圆曲线公钥密码算法 OID，1.2.156.10197.1.301
		 */
		@JSONField(ordinal = 9)
		@NotBlank(message = "algorithm参数为空")
		private String algorithm;
		
		@Tolerate
		public RequestSealCertDtoData() {
		}
		
	}
}
