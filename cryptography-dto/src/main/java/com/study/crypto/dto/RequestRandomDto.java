package com.study.crypto.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.annotation.FixedValue;
import com.study.crypto.dto.ca.CaConstants;
import com.study.crypto.dto.gb.GbConstants;
import com.study.crypto.dto.group.RandomForCA;
import com.study.crypto.dto.group.RandomForGb;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 获取随机数请求
 * @author Songjin
 * @since 2019/10/28 11:13
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestRandomDto {
	
	/**
	 * 业务类型
	 */
	@JSONField(ordinal = 1)
	@FixedValue(groups = RandomForCA.class, values = "applyServiceRandom")
	@FixedValue(groups = RandomForGb.class, values = "applyRandom")
	private String taskCode;
	/**
	 * 协议版本号，一般固定为：1.0
	 */
	@JSONField(ordinal = 2)
	@FixedValue(values = "1.0")
	private String version;
	/**
	 * BASE64 编码的申请者签名证书
	 */
	@JSONField(ordinal = 3)
	@NotBlank(message = "applicantCert参数为空")
	private String applicantCert;
	/**
	 * 电子印章制作系统产生的随机数，长度为 16 个字节
	 */
	@JSONField(ordinal = 4)
	@NotBlank(message = "randomA参数为空")
	@Size(min = 16, max = 16, message = "randomA参数固定长度16字节")
	private String randomA;
	
	/**
	 * 获取国办随机数请求
	 * @return 随机数请求对象
	 */
	public static RequestRandomDto getRequestRandomDtoGb() {
		RequestRandomDto request = new RequestRandomDto();
		request.setVersion(GbConstants.VERSION);
		request.setTaskCode(GbConstants.APPLY_RANDOM_GB);
		return request;
	}
	
	/**
	 * 获取ca随机数请求
	 * @return 随机数请求对象
	 */
	public static RequestRandomDto getRequestRandomDtoCa() {
		RequestRandomDto request = new RequestRandomDto();
		request.setVersion(CaConstants.VERSION);
		request.setTaskCode(CaConstants.TASK_CODE_APPLY_RANDOM_CA);
		return request;
	}
	
}
