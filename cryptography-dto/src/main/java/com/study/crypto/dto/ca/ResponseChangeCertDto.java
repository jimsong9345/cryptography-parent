package com.study.crypto.dto.ca;

import com.study.crypto.dto.SignInfoDto;
import com.study.crypto.dto.TaskIdDto;

/**
 * 证书内容变更响应
 * @author Songjin
 * @since 2019/10/29 10:31
 */
public class ResponseChangeCertDto extends ResponseSealCertDto {
	
	public ResponseChangeCertDto() {
		this.setVersion(CaConstants.VERSION);
		this.setTaskCode(CaConstants.TASK_CODE_APPLY_SEAL_CERT);
		this.setData(new TaskIdDto());
		this.setSignInfo(new SignInfoDto());
	}
}
