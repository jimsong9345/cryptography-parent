package com.study.crypto.dto.ca.base;

import com.alibaba.fastjson.annotation.JSONField;
import com.study.crypto.annotation.FixedValue;
import com.study.crypto.dto.SignInfoDto;
import com.study.crypto.dto.ca.CaConstants;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * ca请求超类
 *
 * @author Songjin
 * @since 2019/10/29 10:55
 */
@NoArgsConstructor
@SuperBuilder
@Data
public class RequestCaDto {
    
    public interface SealCert{ }
    public interface RevokeCert{ }

    /**
     * 业务类型，applySealCert
     */
    @JSONField(ordinal = 1)
    @FixedValue(groups = SealCert.class, values = "applySealCert")
    @FixedValue(groups = RevokeCert.class, values = "revokeSealCert")
    private String taskCode;

    /**
     * 协议版本号，一般固定为：1.0
     */
    @JSONField(ordinal = 2)
    @FixedValue(values = CaConstants.VERSION)
    private String version;

    /**
     * 客户端随机数(randomA)与服务端随机数(randomB)字符串拼接，中间无分割符号
     */
    @JSONField(ordinal = 3)
    @NotBlank(message = "tokenInfo参数为空")
    @Size(min = 32, max = 32, message = "tokenInfo参数固定长度32字节")
    private String tokenInfo;

    /**
     * 签名信息
     */
    @JSONField(ordinal = 10)
    @NotNull(message = "signInfo参数为空")
    @Valid
    private SignInfoDto signInfo;
    
}
