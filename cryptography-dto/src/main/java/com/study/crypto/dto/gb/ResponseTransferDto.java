package com.study.crypto.dto.gb;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 电子印章状态信息推送：数据传输响应
 * @author Songjin
 * @since 2019/10/28 13:53
 */
@Data
public class ResponseTransferDto {
	
	/**
	 * 存放返回信息的对象
	 */
	private MessageHeaderDto messageHeader;
	/**
	 * 存放返回信息的对象
	 */
	private MessageBodyDto messageBody;
	/**
	 * 状态码
	 */
	private String code;
	/**
	 * 描述信息
	 */
	private String message;
	/**
	 * 状态码
	 */
	private String resultCode;
	/**
	 * 描述信息
	 */
	private String resultMsg;
	/**
	 * 是否成功
	 */
	private String success;
	
	@Data
	@AllArgsConstructor
	public static class MessageHeaderDto {
		/**
		 * 返回的状态码，0 为成功
		 */
		private String code;
		/**
		 * 返回的描述信息
		 */
		private String msg;
		
	}
	
	@Data
	@AllArgsConstructor
	public static class MessageBodyDto {
		/**
		 * 调用内部接口是否成功
		 */
		private String success;
		/**
		 * 接口返回状态码
		 */
		private String resultCode;
		/**
		 * 返回结果信息
		 */
		private String resultMsg;
		
	}
}
