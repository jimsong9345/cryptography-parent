package com.study.crypto.dto.ca;

import com.study.crypto.dto.TaskIdDto;

/**
 * 证书吊销响应
 * @author Songjin
 * @since 2019/10/29 10:29
 */
public class ResponseRevokeCertDto extends ResponseSealCertDto {
	
	public ResponseRevokeCertDto() {
		this.setVersion(CaConstants.VERSION);
		this.setTaskCode(CaConstants.TASK_CODE_REVOKE_SEAL_CERT);
		this.setData(new TaskIdDto());
	}
}
