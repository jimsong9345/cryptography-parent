/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : cryptography-client

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 17/02/2021 23:07:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bo_certificate_apply
-- ----------------------------
DROP TABLE IF EXISTS `bo_certificate_apply`;
CREATE TABLE `bo_certificate_apply`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `ext1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `ext2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `ext3` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户id',
  `task_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '业务类型',
  `version` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '协议版本号1.0',
  `token_info` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户端随机数与服务端随机数字符串拼接，中间无分隔符',
  `request_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用于区分请求列表的单个请求',
  `cert_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '证书类型: 0 个人证书，单位证书 1，服务器证书 2，大于2预留扩展',
  `country_name` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '证书主体的国家项，2字节CN',
  `organization_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '证书主体的组织项，不大于 150 个字节',
  `common_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '证书主体的组织项，不大于200个字节',
  `subject_public_key_info` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'base64编码的签名公钥信息',
  `not_before` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '证书有效期起始时间，格式: yyyy-MM-dd HH:mm:ss',
  `not_after` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '证书有效期结束时间，格式: yyyy-MM-dd HH:mm:ss',
  `algorithm` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'SM2椭圆曲线公钥密码算法OID，1.2.156.10197.1.301',
  `keystore_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密钥外键',
  `task_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'CA系统返回的任务编号，用于查询任务结果时使用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '证书申请' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for bo_key_storage
-- ----------------------------
DROP TABLE IF EXISTS `bo_key_storage`;
CREATE TABLE `bo_key_storage`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `ext1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `ext2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `ext3` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户id',
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '别名',
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
  `port` mediumint(6) NULL DEFAULT NULL COMMENT '端口号',
  `key_alg` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '算法名称',
  `key_size` smallint(6) NULL DEFAULT NULL COMMENT '密钥字节大小',
  `key_type` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密钥类型: 1 签名密钥，2 加密密钥，3 对称密钥',
  `key_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密钥名称',
  `pwd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '私钥密码',
  `status` tinyint(11) NULL DEFAULT NULL COMMENT '状态',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `key_bytes` blob NULL COMMENT '密钥数据',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_key_name`(`key_name`) USING BTREE COMMENT '密钥名称索引'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '密钥表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bo_seal
-- ----------------------------
DROP TABLE IF EXISTS `bo_seal`;
CREATE TABLE `bo_seal`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `ext1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `ext2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `ext3` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户id',
  `seal_height` float NULL DEFAULT NULL COMMENT '印章高度',
  `seal_width` float NULL DEFAULT NULL COMMENT '印章宽度',
  `seal_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '印章名称',
  `seal_image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '印章图片',
  `not_before` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '印章有效起始时间',
  `not_after` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '印章有效终止时间',
  `sign_cert` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '签名证书',
  `enc_cert` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '加密证书',
  `enc_cert_key` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '加密密钥对保护结构',
  `enc_file` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '公安下发加密文件',
  `app_sym_key_enc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'base64编码的应用维护对称密钥密文',
  `data_sym_key_enc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'base64编码的数据加密对称密钥密文',
  `pkcs10` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备案签名证书P10请求',
  `unit_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '印章使用主体单位id，关联bo_unit',
  `sign_key_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签名密钥id关联bo_signkey',
  `sign_policy_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签名策略id关联bo_signpolicy',
  `seal_file` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '印章结构文件',
  `seal_hash` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '印章结构hash',
  `status` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '印章状态：0 有效，1 废弃，2 注销，3 过期，4 停用。默认为禁用状态1',
  `state_time` datetime(0) NULL DEFAULT NULL COMMENT '印章状态变更时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '印章表' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
