package com.study.crypto.annotation;

import com.study.crypto.annotation.validation.FixedValueValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 参数为固定值校验
 *
 * @author Songjin
 * @since 2021-01-10 19:00
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(FixedValue.List.class)
@Documented
@Constraint(validatedBy = FixedValueValidator.class)
public @interface FixedValue {
    
    /** 是否是必选项 */
    boolean required() default true;
    
    /** 提示信息 */
    String message() default "参数必须为指定的值！";
    
    /** 分组检验 */
    Class<?>[] groups() default {};
    
    Class<? extends Payload>[] payload() default {};
    
    /** 字符类型的指定值时 */
    String[] values() default {};
    
    /** 数值类型的指定值时 */
    int[] intValues() default {};
    
    /**
     * Defines several {@link FixedValue} annotations on the same element.
     *
     * @see FixedValue
     */
    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        FixedValue[] value();
    }
}
