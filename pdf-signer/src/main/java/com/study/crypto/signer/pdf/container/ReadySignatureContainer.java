package com.study.crypto.signer.pdf.container;

import com.itextpdf.kernel.pdf.PdfDictionary;
import com.itextpdf.signatures.IExternalSignatureContainer;

import java.io.InputStream;

/**
 * 分步签章: 接收签名值，用于合章
 * @author Songjin
 * @since 2022-03-30 17:20
 */
public class ReadySignatureContainer implements IExternalSignatureContainer {
    
    /**
     * 签名值: 可以是rsa-pkcs7签名值，或者是GBT-35275 sm2签名值，或者是GMT-0031、GBT-38540签名值
     */
    private final byte[] signatureContents;
    
    public ReadySignatureContainer(byte[] signatureContents) {
        this.signatureContents = signatureContents;
    }
    
    public byte[] sign(InputStream docBytes) {
        return signatureContents;
    }
    
    public void modifySigningDictionary(PdfDictionary signDic) {
    }
}