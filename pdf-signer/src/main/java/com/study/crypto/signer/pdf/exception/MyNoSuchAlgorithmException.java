package com.study.crypto.signer.pdf.exception;

/**
 * 未找到对应算法
 * @author Songjin
 * @since 2022-04-24 17:58
 */
public class MyNoSuchAlgorithmException extends RuntimeException {
    
    public MyNoSuchAlgorithmException(String message, Throwable cause) {
        super(message, cause);
    }
}
