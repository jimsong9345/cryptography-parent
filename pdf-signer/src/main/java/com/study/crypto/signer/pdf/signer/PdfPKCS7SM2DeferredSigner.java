package com.study.crypto.signer.pdf.signer;

import com.itextpdf.signatures.DigestAlgorithms;
import org.bouncycastle.asn1.x509.Certificate;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;

/**
 * <p>使用外传摘要方式，进行 pdf sm2 签名</p>
 * <p>符合《GB-T 35275-2017_信息安全技术_SM2密码算法加密签名信息消息语法规范》标准</p>
 * @author Songjin
 * @since 2022-05-06 10:45
 */
public class PdfPKCS7SM2DeferredSigner extends PdfPKCS7DeferredSigner {
    
    public PdfPKCS7SM2DeferredSigner(PrivateKey privateKey, Certificate certificate, byte[] digest) {
        super(privateKey, certificate, digest);
    }
    
    public byte[] signDeferred(String sigFieldName, byte[] toSignedBytes) throws GeneralSecurityException, IOException {
        setDigestAlgo(DigestAlgorithms.SM3);
        return super.signDeferred(sigFieldName, toSignedBytes);
    }
}
