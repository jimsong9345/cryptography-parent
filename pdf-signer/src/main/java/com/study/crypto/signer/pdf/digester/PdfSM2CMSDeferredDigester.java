package com.study.crypto.signer.pdf.digester;

import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.signatures.DigestAlgorithms;

/**
 * @author Songjin
 * @since 2022-05-06 14:12
 */
public class PdfSM2CMSDeferredDigester extends PdfDeferredDigester{
    
    /** 显示的图章字节数据 */
    private final byte[] imageBytes;
    
    public PdfSM2CMSDeferredDigester(Rectangle rectangle, int pageNumber, byte[] imageBytes) {
        super(rectangle, pageNumber);
        this.imageBytes = imageBytes;
    }
    
    @Override
    public byte[] getImageBytes() {
        return imageBytes;
    }
    
    @Override
    public String getDigestAlgorithm() {
        return DigestAlgorithms.SM3;
    }
    
    @Override
    public PdfName getFilter() {
        return new PdfName("BJCA.GMPkiLite");
    }
    
    @Override
    public PdfName getSubFilter() {
        return new PdfName("GM.sm2cms.detached");
    }
}
