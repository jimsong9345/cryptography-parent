package com.study.crypto.signer.pdf;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.StampingProperties;
import com.itextpdf.signatures.*;
import com.study.crypto.signer.pdf.container.DigestBlankContainer;
import com.study.crypto.signer.pdf.container.ReadySignatureContainer;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;

/**
 * @author Songjin
 * @since 2022-03-29 15:08
 */
class PdfSignTest {
    
    private static final String HASH_ALGORITHM = DigestAlgorithms.SHA256;
    
    @BeforeAll
    public static void before() {
        Security.addProvider(new BouncyCastleProvider());
    }
    
    /**
     * pdf RSA 签名
     */
    @Test
    void testPdfRSASign() throws Exception{
        String input = "./src/test/resources/test.pdf";
        String outFileName = "./target/test-classes/test-signed.pdf";
    
        // pre-calculate hash on creating pre-signed PDF
        String sigFieldName = "DeferredSignature1";
        PdfName filter = PdfName.Adobe_PPKLite;
        PdfName subFilter = PdfName.Adbe_pkcs7_detached;
        int estimatedSize = 8192;
    
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfReader reader = new PdfReader(input);
        PdfSigner signer = new PdfSigner(reader, baos, new StampingProperties());
        signer.setCertificationLevel(PdfSigner.CERTIFIED_NO_CHANGES_ALLOWED);
        signer.setFieldName(sigFieldName);
        PdfSignatureAppearance appearance = signer.getSignatureAppearance();
        String imagePath = "C:/Users/Administrator/Desktop/temp/演示专用章.png";
        byte[] imageBytes = FileUtils.readFileToByteArray(new File(imagePath));
        float w = (float)(42.0 * 72 / 25.4);
        appearance.setImage(ImageDataFactory.create(imageBytes))
                  // .setLayer2Text("     BJCA   \nESS-PDF!")
                  .setLayer2Text("")
                  .setLayer2FontSize(12)
                  .setPageRect(new Rectangle(36, 600, w, w))
                  .setPageNumber(1);

        DigestBlankContainer external = new DigestBlankContainer(filter, subFilter, "");
        signer.signExternalContainer(external, estimatedSize);
        byte[] docBytesHash = external.getDigest();
        byte[] preSignedBytes = baos.toByteArray();
    
        // sign the hash
        char[] password = "testpass".toCharArray();
        String signCertFileName = "./src/test/resources/signCertRsa01.p12";
        Certificate[] signChain = Pkcs12FileHelper.readFirstChain(signCertFileName, password);
        PrivateKey signPrivateKey = Pkcs12FileHelper.readFirstKey(signCertFileName, password, password);
        byte[] cmsSignature = signDocBytesHash(docBytesHash, signPrivateKey, signChain);
    
        // fill the signature to the presigned document
        ReadySignatureContainer extSigContainer = new ReadySignatureContainer(cmsSignature);
        PdfDocument docToSign = new PdfDocument(new PdfReader(new ByteArrayInputStream(preSignedBytes)));
        FileOutputStream outStream = new FileOutputStream(outFileName);
        PdfSigner.signDeferred(docToSign, sigFieldName, outStream, extSigContainer);
        docToSign.close();
        outStream.close();
    }
    
    static byte[] signDocBytesHash(byte[] docBytesHash, PrivateKey pk, Certificate[] chain) {
        if (pk == null || chain == null) {
            return null;
        }
        
        byte[] signatureContent = null;
        try {
            PdfPKCS7 pkcs7 = new PdfPKCS7(null, chain, HASH_ALGORITHM, null, new BouncyCastleDigest(), false);
            
            byte[] attributes = pkcs7.getAuthenticatedAttributeBytes(docBytesHash, PdfSigner.CryptoStandard.CMS, null, null);
            
            PrivateKeySignature signature = new PrivateKeySignature(pk, HASH_ALGORITHM, BouncyCastleProvider.PROVIDER_NAME);
            byte[] attrSign = signature.sign(attributes);
            
            pkcs7.setExternalDigest(attrSign, null, signature.getEncryptionAlgorithm());
            signatureContent = pkcs7.getEncodedPKCS7(docBytesHash);
        } catch (GeneralSecurityException e) {
            // dummy catch clause
            e.printStackTrace();
        }
        return signatureContent;
    }
    
}
