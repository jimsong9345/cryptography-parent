package com.study.crypto.signer.pdf;

import com.study.crypto.utils.CertUtils;
import com.study.crypto.utils.KeyUtils;
import com.study.crypto.utils.SM2Utils;
import com.study.crypto.utils.SM4Utils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.*;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.gm.CMSEnvelopedDataGenerator;
import org.bouncycastle.cms.gm.GMObjectIdentifiers;
import org.bouncycastle.cms.gm.GmCMSContentEncryptorBuilder;
import org.bouncycastle.cms.gm.GmKeyTransRecipientInfoGenerator;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OutputEncryptor;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Songjin
 * @since 2021-09-21 23:12
 */
class SimpleTest {
    
    @BeforeEach
    void beforeAll() {
        // 添加BouncyCastle作为安全提供
        Security.addProvider(new BouncyCastleProvider());
    }
    
    @Test
    void testCharacter() throws Exception{
        String text = "系统管理员";
        String s = new String(text.getBytes(), StandardCharsets.UTF_8);
        System.out.println(s);
        
        StringBuilder sf = new StringBuilder();
        for (int i = 1; i <= Character.MAX_VALUE; i++) {
            char ch = (char) i;
            sf.append(ch).append('\t');
            if (i % 30 == 0) {
                sf.append('\n');
            }
        }
        FileUtils.writeStringToFile(new File("D:/ch.txt"), sf.toString(), StandardCharsets.UTF_8);
    }
    
    /**
     * 使用 BouncyCastle 解密 pkcs7 数字信封
     */
    @Test
    void testPkcs7EnvelopedDataOpen() throws Exception{
        byte[] envelop = FileUtils.readFileToByteArray(new File("D:/123.dat"));
        ASN1InputStream aIn = new ASN1InputStream(new ByteArrayInputStream(envelop));
        ContentInfo info = ContentInfo.getInstance(aIn.readObject());
        EnvelopedData envData = EnvelopedData.getInstance(info.getContent());
        ASN1Set recipientInfoSet = envData.getRecipientInfos();
        RecipientInfo recipientInfo = RecipientInfo.getInstance(recipientInfoSet.getObjects().nextElement());
        KeyTransRecipientInfo keyTransRecipientInfo = (KeyTransRecipientInfo) recipientInfo.getInfo();

        byte[]     keystore   = FileUtils.readFileToByteArray(new File("D:/production.keystore"));
        byte[]     privateKey = ByteUtils.subArray(keystore, 64);
        PrivateKey prikey     = KeyUtils.convertPrivateKey(privateKey);
        
        byte[] kek = keyTransRecipientInfo.getEncryptedKey().getOctets();
        byte[] key = SM2Utils.decrypt(prikey, kek, SM2Engine.Mode.C1C3C2);
        
        EncryptedContentInfo encryptedContentInfo = envData.getEncryptedContentInfo();
    
        AlgorithmIdentifier algorithmIdentifier = encryptedContentInfo.getContentEncryptionAlgorithm();
        byte[] encContent = encryptedContentInfo.getEncryptedContent().getOctets();
        byte[] bytes = SM4Utils.decrypt_ecb_padding(key, encContent);
        String text = new String(bytes, StandardCharsets.UTF_8);
        System.out.println(text);
    }
    
    /**
     * 使用 BouncyCastle 生成 pkcs7 数字信封
     */
    @Test
    void testPkcs7EnvelopedData() throws Exception {
        byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        X509Certificate cert = CertUtils.rebuildX509Certificate(certBytes);
    
        byte[] contentBytes = "宋进".getBytes(StandardCharsets.UTF_8);
        CMSTypedData content = new CMSProcessableByteArray(GMObjectIdentifiers.sm2_cms_data, contentBytes);
        
        CMSEnvelopedDataGenerator edGen = new CMSEnvelopedDataGenerator();
        edGen.addRecipientInfoGenerator(new GmKeyTransRecipientInfoGenerator(cert));
    
        ASN1ObjectIdentifier oid = GMObjectIdentifiers.sms4_ecb;
        OutputEncryptor encryptor = new GmCMSContentEncryptorBuilder(oid).build();
        CMSEnvelopedData ed = edGen.generate(content, encryptor);
        byte[] encoded = ed.toASN1Structure().getEncoded(ASN1Encoding.DER);
        String text = Base64.encodeBase64String(encoded);
        System.out.println(text);
    }
    
    /**
     * 使用 bouncycastle 生成 pkcs7 签名结构
     */
    @Test
    void testPkcs7SignData() throws Exception {
        // 获取私钥
        byte[]     keystore   = FileUtils.readFileToByteArray(new File("D:/production.keystore"));
        byte[]     privateKey = ByteUtils.subArray(keystore, 64);
        PrivateKey prikey     = KeyUtils.convertPrivateKey(privateKey);
        
        byte[]          certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        X509Certificate cerx509   = CertUtils.rebuildX509Certificate(certBytes);
        
        List<Certificate> certList = new ArrayList<>();
        certList.add(cerx509);
        
        CMSTypedData msg = new CMSProcessableByteArray("123".getBytes());
        
        Store<Certificate> certs = new JcaCertStore(certList);
    
        CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
        ContentSigner sha1Signer = new JcaContentSignerBuilder("SM3withSM2").setProvider("BC").build(prikey);
        
        gen.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(
                new JcaDigestCalculatorProviderBuilder().setProvider("BC")
                                                        .build()).build(sha1Signer, cerx509));
        
        gen.addCertificates(certs);
        
        CMSSignedData sigData = gen.generate(msg);
        String text = Base64.encodeBase64String(sigData.getEncoded(ASN1Encoding.DER));
        System.out.println(text);
    }
    
    @Test
    void test2() throws Exception {
        Certificate certificate;
    
        X509Certificate certificate1;
        X509CertificateHolder x509CertificateHolder;
        
        CertificateFactory sd;
    
        byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        org.bouncycastle.asn1.x509.Certificate certificate2 = org.bouncycastle.asn1.x509.Certificate.getInstance(certBytes);
        System.out.println(Hex.toHexString(certBytes));
        System.out.println(Hex.toHexString(certificate2.getEncoded()));
    }
    
}
