package com.study.crypto.signer.pdf;

import com.itextpdf.kernel.geom.Rectangle;
import com.study.crypto.signer.pdf.digester.PdfSM2CMSDeferredDigester;
import com.study.crypto.signer.pdf.signer.PdfPKCS7SM2DeferredSigner;
import com.study.crypto.utils.KeyUtils;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.security.PrivateKey;
import java.security.Security;

/**
 * @author Songjin
 * @since 2022-04-25 15:54
 */
class PdfSM2SignTest {
    
    String keyStorageHex = "e2a0eae784ee407d6f56a0bd3267d373eac550e7ace7588d1e95ddb28fa64d0ef78f7721589d64b53d4dfaa66c3c97851c3c1f2a11aeebe491d10102fc081defe921c7baf041fe4c44b0fd0bb0b7e4a22a7a41d92e08c10dfa73aefcb313b00d";
    
    @BeforeAll
    public static void before() {
        Security.addProvider(new BouncyCastleProvider());
    }
    
    /**
     * pdf sm2 摘要签名
     */
    @Test
    void testSM2DeferredSign() throws Exception{
        String input = "./src/test/resources/test.pdf";
        String outFileName = "./target/test-classes/signed-sm2-Deferred.pdf";
        byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        Certificate certificate = Certificate.getInstance(certBytes);
        
        String sigFieldName = "DeferredSignature1";
        String imagePath = "C:/Users/Administrator/Desktop/temp/演示专用章.png";
        byte[] imageBytes = FileUtils.readFileToByteArray(new File(imagePath));
        float w = (float)(42.0 * 72 / 25.4);
        Rectangle rectangle = new Rectangle(36, 600, w, w);
        
        // 1.计算 pdf 签名摘要，同时生成预签名的 pdf 文件字节流
        PdfSM2CMSDeferredDigester deferredDigester = new PdfSM2CMSDeferredDigester(rectangle, 1, imageBytes);
        FileInputStream inputStream = FileUtils.openInputStream(new File(input));
        byte[] digest = deferredDigester.digest(sigFieldName, certificate, inputStream);
        byte[] toSignedBytes = deferredDigester.toSignedBytes();
    
        byte[] keyStorage = Hex.decode(keyStorageHex);
        PrivateKey privateKey = KeyUtils.convertPrivateKey(ByteUtils.subArray(keyStorage, 64));
        
        // 2.完成签章
        PdfPKCS7SM2DeferredSigner deferredContainer = new PdfPKCS7SM2DeferredSigner(privateKey, certificate, digest);
        byte[] signedBytes = deferredContainer.signDeferred(sigFieldName, toSignedBytes);
        FileUtils.writeByteArrayToFile(new File(outFileName), signedBytes);
    }
    
    /**
     * pdf sm2 原文签名
     */
    @Test
    void testSM2DetachedSign() throws Exception{
        // String input = "./src/test/resources/test.pdf";
        // String outFileName = "./target/test-classes/signed-sm2-detached.pdf";
        //
        // int estimatedSize = 8192;
        // PdfReader reader = new PdfReader(input);
        // StampingProperties properties = new StampingProperties();
        // if (isAppendMode) {
        //     properties.useAppendMode();
        // }
        // PdfSigner signer = new PdfSigner(reader, new FileOutputStream(dest), properties);
        //
        // signer.setCertificationLevel(certificationLevel);
        //
        // // Creating the appearance
        // PdfSignatureAppearance appearance = signer.getSignatureAppearance()
        //                                           .setReason(reason)
        //                                           .setLocation(location)
        //                                           .setReuseAppearance(setReuseAppearance);
        //
        // if (rectangleForNewField != null) {
        //     appearance.setPageRect(rectangleForNewField);
        // }
        //
        // signer.setFieldName(name);
        // // Creating the signature
        // IExternalSignatureContainer external =
        //         new ExternalSm2SignatureContainer(new PdfName("BJCA.GMPkiLite"), new PdfName("GM.sm2cms.detached"),chain,pk,digestAlgorithm, subfilter);
        // signer.signExternalContainer(external,estimatedSize);
        
        // String input = "./src/test/resources/test.pdf";
        // String outFileName = "./target/test-classes/test-signed-sm2.pdf";
        //
        // String sigFieldName = "DeferredSignature1";
        // PdfName filter = new PdfName("BJCA.GMPkiLite");
        // PdfName subFilter = new PdfName("GM.sm2cms.detached");
        // int estimatedSize = 8192;
        //
        // ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // PdfReader reader = new PdfReader(input);
        // PdfSigner signer = new PdfSigner(reader, baos, new StampingProperties());
        // signer.setCertificationLevel(PdfSigner.CERTIFIED_NO_CHANGES_ALLOWED);
        // signer.setFieldName(sigFieldName);
        // PdfSignatureAppearance appearance = signer.getSignatureAppearance();
        // String imagePath = "C:/Users/Administrator/Desktop/temp/演示专用章.png";
        // byte[] imageBytes = FileUtils.readFileToByteArray(new File(imagePath));
        // float w = (float)(42.0 * 72 / 25.4);
        // appearance.setImage(ImageDataFactory.create(imageBytes))
        //           .setLayer2Text("")
        //           .setPageRect(new Rectangle(36, 600, w, w))
        //           .setPageNumber(1);
        //
        // DigestBlankContainer external = new DigestBlankContainer(filter, subFilter, "");
        // signer.signExternalContainer(external, estimatedSize);
        // byte[] digest = external.getDigest();
        // byte[] preSignedBytes = baos.toByteArray();
        //
        // byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        // Certificate certificate = CertUtils.rebuildCertificate(certBytes);
        // byte[] keyStorage = Hex.decode(keyStorageHex);
        // PrivateKey privateKey = KeyUtils.convertPrivateKey(ByteUtils.subArray(keyStorage, 64));
        //
        // Pkcs7GmDeferredSigner deferredSigner = new Pkcs7GmDeferredSigner(certificate, null, digest);
        // byte[] cmsSignature = deferredSigner.sign(digest, privateKey);
        //
        // ReadySignatureContainer extSigContainer = new ReadySignatureContainer(cmsSignature);
        // PdfDocument docToSign = new PdfDocument(new PdfReader(new ByteArrayInputStream(preSignedBytes)));
        // FileOutputStream outStream = new FileOutputStream(outFileName);
        // PdfSigner.signDeferred(docToSign, sigFieldName, outStream, extSigContainer);
        // docToSign.close();
        // outStream.close();
    }
}
