package com.study.crypto.signer.pdf;

import com.study.crypto.signer.PKCS7GmDeferredSigner;
import com.study.crypto.utils.CertUtils;
import com.study.crypto.utils.KeyUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.pqc.legacy.math.linearalgebra.ByteUtils;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.security.PrivateKey;

/**
 * @author Songjin
 * @since 2022-04-28 17:43
 */
class PKCS7GmDeferredSignerTest {
    
    PrivateKey privateKey;
    String keyStorageHex = "e2a0eae784ee407d6f56a0bd3267d373eac550e7ace7588d1e95ddb28fa64d0ef78f7721589d64b53d4dfaa66c3c97851c3c1f2a11aeebe491d10102fc081defe921c7baf041fe4c44b0fd0bb0b7e4a22a7a41d92e08c10dfa73aefcb313b00d";
    
    @BeforeEach
    void beforeEach() throws Exception{
        byte[] keyStorage = Hex.decode(keyStorageHex);
        privateKey = KeyUtils.convertPrivateKey(ByteUtils.subArray(keyStorage, 64));
    }
    
    @Test
    void test() throws Exception{
        String hashB64 = "lnStZA7RVqRAeiDb/19fPRtJfHcZsboQYZpebyKYs2A=";
        byte[] certBytes = FileUtils.readFileToByteArray(new File("D:/production.cer"));
        Certificate certificate = CertUtils.convertCertificate(certBytes);
        PKCS7GmDeferredSigner signer = new PKCS7GmDeferredSigner(certificate, null, null);
        byte[] sign = signer.sign(Base64.decodeBase64(hashB64), privateKey);
        File file = new File("D:/java/JetBrains/workspace01/cryptography-parent/pdf-signer/target/test-classes/sm2-sign-2.dat");
        FileUtils.writeByteArrayToFile(file, sign);
    }
    
}
