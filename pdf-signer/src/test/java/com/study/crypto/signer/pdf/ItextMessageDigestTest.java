package com.study.crypto.signer.pdf;

import com.itextpdf.signatures.DigestAlgorithms;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.Security;

/**
 * @author Songjin
 * @since 2022-05-06 9:11
 */
@Slf4j
class ItextMessageDigestTest {
    
    @BeforeAll
    public static void before() {
        Security.addProvider(new BouncyCastleProvider());
    }
    
    @Test
    void testItextMessageDigest() throws Exception{
        MessageDigest messageDigest = MessageDigest.getInstance("SM3", "BC");
        File file = new File("D:/transcode.mp4");
        FileInputStream input = FileUtils.openInputStream(file);
        byte[] digest = DigestAlgorithms.digest(input, messageDigest);
        log.info("MessageDigest文件摘要: {}", Base64.encodeBase64String(digest));
    
        byte[] plaintext = FileUtils.readFileToByteArray(file);
        SM3Digest sm3Digest = new SM3Digest();
        byte[] hash = new byte[sm3Digest.getDigestSize()];
        sm3Digest.update(plaintext, 0, plaintext.length);
        sm3Digest.doFinal(hash, 0);
        log.info("SM3Digest    文件摘要: {}", Base64.encodeBase64String(digest));
    }
    
}
