package com.study.crypto.signer.pdf;

import com.itextpdf.kernel.pdf.PdfArray;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.signatures.PdfSignature;
import com.itextpdf.signatures.SignatureUtil;
import org.junit.jupiter.api.Test;

/**
 * @author Songjin
 * @since 2022-04-28 9:24
 */
class PdfSignatureTest {
    
    @Test
    void test() throws Exception{
        String outFileName = "./target/test-classes/test-signed-sm2.pdf";
        try (PdfDocument doc = new PdfDocument(new PdfReader(outFileName))) {
            SignatureUtil sigUtil = new SignatureUtil(doc);
            PdfSignature signature = sigUtil.getSignature("DeferredSignature1");
            PdfArray byteRange = signature.getByteRange();
            System.out.println(byteRange);
        }
    }
    
}
