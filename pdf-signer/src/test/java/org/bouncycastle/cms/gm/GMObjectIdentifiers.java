package org.bouncycastle.cms.gm;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

/**
 * @author Songjin
 * @since 2021-10-03 11:16
 */
public interface GMObjectIdentifiers extends org.bouncycastle.asn1.gm.GMObjectIdentifiers {
    
    ASN1ObjectIdentifier sm4 = sm_scheme.branch("104");
    
    /** gm cms: sm2密码算法加密去签名消息语法规范 */
    ASN1ObjectIdentifier sm2_cms                        = new ASN1ObjectIdentifier("1.2.156.10197.6.1.4.2");
    /** gm cms: 1.2.156.10197.6.1.4.2.1 */
    ASN1ObjectIdentifier sm2_cms_data                   = sm2_cms.branch("1");
    /** gm cms: 1.2.156.10197.6.1.4.2.2 */
    ASN1ObjectIdentifier sm2_cms_signedData             = sm2_cms.branch("2");
    /** gm cms: 1.2.156.10197.6.1.4.2.3 */
    ASN1ObjectIdentifier sm2_cms_envelopedData          = sm2_cms.branch("3");
    /** gm cms: 1.2.156.10197.6.1.4.2.4 */
    ASN1ObjectIdentifier sm2_cms_signedAndEnvelopedData = sm2_cms.branch("4");
    /** gm cms: 1.2.156.10197.6.1.4.2.5 */
    ASN1ObjectIdentifier sm2_cms_encryptedData          = sm2_cms.branch("5");
}
