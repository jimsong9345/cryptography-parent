package org.bouncycastle.cms.gm;

import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.KeyTransRecipientInfoGenerator;
import org.bouncycastle.operator.AsymmetricKeyWrapper;

import java.security.Provider;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

/**
 * @author Songjin
 * @since 2021-10-03 16:59
 */
public class GmKeyTransRecipientInfoGenerator extends KeyTransRecipientInfoGenerator {
    
    public GmKeyTransRecipientInfoGenerator(X509Certificate recipientCert) throws CertificateEncodingException, CMSException {
        super(new IssuerAndSerialNumber(new JcaX509CertificateHolder(recipientCert).toASN1Structure()), new GmAsymmetricKeyWrapper(recipientCert));
    }
    
    protected GmKeyTransRecipientInfoGenerator(IssuerAndSerialNumber issuerAndSerial, AsymmetricKeyWrapper wrapper) {
        super(issuerAndSerial, wrapper);
    }
    
    protected GmKeyTransRecipientInfoGenerator(byte[] subjectKeyIdentifier, AsymmetricKeyWrapper wrapper) {
        super(subjectKeyIdentifier, wrapper);
    }
    
    public GmKeyTransRecipientInfoGenerator setProvider(String providerName) {
        ((GmAsymmetricKeyWrapper) this.wrapper).setProvider(providerName);
        return this;
    }
    
    public GmKeyTransRecipientInfoGenerator setProvider(Provider provider) {
        ((GmAsymmetricKeyWrapper) this.wrapper).setProvider(provider);
        return this;
    }
    
}
